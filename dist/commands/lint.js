"use strict";

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var __awaiter = undefined && undefined.__awaiter || function (thisArg, _arguments, Promise, generator) {
    return new Promise(function (resolve, reject) {
        generator = generator.call(thisArg, _arguments);
        function cast(value) {
            return value instanceof Promise && value.constructor === Promise ? value : new Promise(function (resolve) {
                resolve(value);
            });
        }
        function onfulfill(value) {
            try {
                step("next", value);
            } catch (e) {
                reject(e);
            }
        }
        function onreject(value) {
            try {
                step("throw", value);
            } catch (e) {
                reject(e);
            }
        }
        function step(verb, value) {
            var result = generator[verb](value);
            result.done ? resolve(result.value) : cast(result.value).then(onfulfill, onreject);
        }
        step("next", void 0);
    });
};
require('./ICommandLine');
var Logger_1 = require('../lib/Logger');
var fsp = require('fs-promise');
var fs = require('fs');
var exec_1 = require('../lib/exec');

var LintCLI = function () {
    function LintCLI(linters) {
        _classCallCheck(this, LintCLI);

        this.linters = linters;
        this.commands = {
            lint: {
                description: 'run linters on all source files'
            }
        };
        this.linterPackages = {
            tslint: {
                code: 'TypeScript',
                package: 'project-cli/dist/lib/LinterTS'
            },
            jscs: {
                code: 'JavaScript ES201x'
            },
            sasslint: {
                code: 'SASS/SCSS',
                package: 'sass-lint'
            },
            htmlhint: {
                code: 'HTML',
                package: 'htmlhint'
            }
        };
    }

    _createClass(LintCLI, [{
        key: "exec",
        value: function exec(argv) {
            return __awaiter(this, void 0, Promise, regeneratorRuntime.mark(function _callee() {
                var cmd, result;
                return regeneratorRuntime.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                cmd = argv._[0];

                                if (this.commands[cmd]) {
                                    _context.next = 3;
                                    break;
                                }

                                return _context.abrupt("return", false);

                            case 3:
                                _context.t0 = cmd;
                                _context.next = _context.t0 === 'lint' ? 6 : 15;
                                break;

                            case 6:
                                _context.next = 8;
                                return this.lint();

                            case 8:
                                result = _context.sent;

                                if (!result.errorCount) {
                                    _context.next = 13;
                                    break;
                                }

                                return _context.abrupt("return", true);

                            case 13:
                                Logger_1.logger.success('looking good !');

                            case 14:
                                return _context.abrupt("return", true);

                            case 15:
                                return _context.abrupt("return", false);

                            case 16:
                            case "end":
                                return _context.stop();
                        }
                    }
                }, _callee, this);
            }));
        }
    }, {
        key: "lint",
        value: function lint() {
            return __awaiter(this, void 0, Promise, regeneratorRuntime.mark(function _callee2() {
                var results, errorCount, _iteratorNormalCompletion, _didIteratorError, _iteratorError, _iterator, _step, _step$value, packageName, packageObj, _result, pkg;

                return regeneratorRuntime.wrap(function _callee2$(_context2) {
                    while (1) {
                        switch (_context2.prev = _context2.next) {
                            case 0:
                                results = [];
                                errorCount = 0;

                                Logger_1.logger.info("Linting all code");
                                _iteratorNormalCompletion = true;
                                _didIteratorError = false;
                                _iteratorError = undefined;
                                _context2.prev = 6;
                                _iterator = Object.entries(this.linters)[Symbol.iterator]();

                            case 8:
                                if (_iteratorNormalCompletion = (_step = _iterator.next()).done) {
                                    _context2.next = 43;
                                    break;
                                }

                                _step$value = _slicedToArray(_step.value, 2);
                                packageName = _step$value[0];
                                packageObj = _step$value[1];
                                _result = undefined;
                                pkg = this.linterPackages[packageName];
                                _context2.t0 = packageName;
                                _context2.next = _context2.t0 === 'tslint' ? 17 : _context2.t0 === 'jscs' ? 22 : _context2.t0 === 'sasslint' ? 27 : _context2.t0 === 'htmlhint' ? 33 : 38;
                                break;

                            case 17:
                                _context2.next = 19;
                                return this.lintTS(packageObj.config, packageObj.paths, pkg.package);

                            case 19:
                                _result = _context2.sent;

                                errorCount += _result.errorCount;
                                return _context2.abrupt("break", 39);

                            case 22:
                                _context2.next = 24;
                                return this.lintJS(packageObj.config, packageObj.paths);

                            case 24:
                                _result = _context2.sent;

                                errorCount += _result.errorCount;
                                return _context2.abrupt("break", 39);

                            case 27:
                                _context2.next = 29;
                                return this.lintSass(packageObj.config, packageObj.paths);

                            case 29:
                                _result = _context2.sent;

                                errorCount += _result.errorCount;
                                errorCount += _result.warningCount;
                                return _context2.abrupt("break", 39);

                            case 33:
                                _context2.next = 35;
                                return this.lintHtml(packageObj.config, packageObj.paths);

                            case 35:
                                _result = _context2.sent;

                                errorCount += _result.errorCount;
                                return _context2.abrupt("break", 39);

                            case 38:
                                throw new Error("linter '" + packageName + "' not supported");

                            case 39:
                                results[packageName] = _result;

                            case 40:
                                _iteratorNormalCompletion = true;
                                _context2.next = 8;
                                break;

                            case 43:
                                _context2.next = 49;
                                break;

                            case 45:
                                _context2.prev = 45;
                                _context2.t1 = _context2["catch"](6);
                                _didIteratorError = true;
                                _iteratorError = _context2.t1;

                            case 49:
                                _context2.prev = 49;
                                _context2.prev = 50;

                                if (!_iteratorNormalCompletion && _iterator.return) {
                                    _iterator.return();
                                }

                            case 52:
                                _context2.prev = 52;

                                if (!_didIteratorError) {
                                    _context2.next = 55;
                                    break;
                                }

                                throw _iteratorError;

                            case 55:
                                return _context2.finish(52);

                            case 56:
                                return _context2.finish(49);

                            case 57:
                                return _context2.abrupt("return", { errorCount: errorCount, results: results });

                            case 58:
                            case "end":
                                return _context2.stop();
                        }
                    }
                }, _callee2, this, [[6, 45, 49, 57], [50,, 52, 56]]);
            }));
        }
    }, {
        key: "lintTS",
        value: function lintTS(configFile, globs, modulePath) {
            return __awaiter(this, void 0, Promise, regeneratorRuntime.mark(function _callee3() {
                var cfg, TSLinter, linter, result;
                return regeneratorRuntime.wrap(function _callee3$(_context3) {
                    while (1) {
                        switch (_context3.prev = _context3.next) {
                            case 0:
                                cfg = {};

                                if (!fs.existsSync(configFile)) {
                                    _context3.next = 7;
                                    break;
                                }

                                _context3.t0 = JSON;
                                _context3.next = 5;
                                return fsp.readFile(configFile, 'utf8');

                            case 5:
                                _context3.t1 = _context3.sent;
                                cfg = _context3.t0.parse.call(_context3.t0, _context3.t1);

                            case 7:
                                TSLinter = require(modulePath).default;
                                linter = new TSLinter(cfg);
                                _context3.next = 11;
                                return linter.lint(globs);

                            case 11:
                                result = _context3.sent;

                                if (result.failureCount) {
                                    Logger_1.logger.error("✗ TypeScript: " + result.failureCount + " errors");
                                    Logger_1.logger.warn(result.errors.join('\n'));
                                } else {
                                    Logger_1.logger.success("✓ TypeScript");
                                }
                                result.errorCount = result.failureCount;
                                return _context3.abrupt("return", result);

                            case 15:
                            case "end":
                                return _context3.stop();
                        }
                    }
                }, _callee3, this);
            }));
        }
    }, {
        key: "lintJS",
        value: function lintJS(configFile, globs) {
            return __awaiter(this, void 0, Promise, regeneratorRuntime.mark(function _callee4() {
                var results, result, errorCount, _iteratorNormalCompletion2, _didIteratorError2, _iteratorError2, _iterator2, _step2, globPattern, lintOutputs, _iteratorNormalCompletion3, _didIteratorError3, _iteratorError3, _iterator3, _step3, _step3$value, key, errors, _iteratorNormalCompletion4, _didIteratorError4, _iteratorError4, _iterator4, _step4, err;

                return regeneratorRuntime.wrap(function _callee4$(_context4) {
                    while (1) {
                        switch (_context4.prev = _context4.next) {
                            case 0:
                                results = undefined;
                                result = undefined;
                                errorCount = 0;
                                _iteratorNormalCompletion2 = true;
                                _didIteratorError2 = false;
                                _iteratorError2 = undefined;
                                _context4.prev = 6;
                                _iterator2 = globs[Symbol.iterator]();

                            case 8:
                                if (_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done) {
                                    _context4.next = 70;
                                    break;
                                }

                                globPattern = _step2.value;
                                lintOutputs = [];
                                _context4.prev = 11;
                                _context4.next = 14;
                                return exec_1.default("jscs -c " + configFile + " " + globPattern + " --reporter json", {
                                    noErrors: true,
                                    silent: true
                                });

                            case 14:
                                result = _context4.sent;
                                _context4.next = 66;
                                break;

                            case 17:
                                _context4.prev = 17;
                                _context4.t0 = _context4["catch"](11);

                                result = JSON.parse(_context4.t0.output);
                                _iteratorNormalCompletion3 = true;
                                _didIteratorError3 = false;
                                _iteratorError3 = undefined;
                                _context4.prev = 23;
                                _iterator3 = Object.entries(result)[Symbol.iterator]();

                            case 25:
                                if (_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done) {
                                    _context4.next = 52;
                                    break;
                                }

                                _step3$value = _slicedToArray(_step3.value, 2);
                                key = _step3$value[0];
                                errors = _step3$value[1];
                                _iteratorNormalCompletion4 = true;
                                _didIteratorError4 = false;
                                _iteratorError4 = undefined;
                                _context4.prev = 32;

                                for (_iterator4 = errors[Symbol.iterator](); !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
                                    err = _step4.value;

                                    lintOutputs.push(key + " (" + err.line + ":" + err.column + "): " + err.message);
                                }
                                _context4.next = 40;
                                break;

                            case 36:
                                _context4.prev = 36;
                                _context4.t1 = _context4["catch"](32);
                                _didIteratorError4 = true;
                                _iteratorError4 = _context4.t1;

                            case 40:
                                _context4.prev = 40;
                                _context4.prev = 41;

                                if (!_iteratorNormalCompletion4 && _iterator4.return) {
                                    _iterator4.return();
                                }

                            case 43:
                                _context4.prev = 43;

                                if (!_didIteratorError4) {
                                    _context4.next = 46;
                                    break;
                                }

                                throw _iteratorError4;

                            case 46:
                                return _context4.finish(43);

                            case 47:
                                return _context4.finish(40);

                            case 48:
                                if (errors.length) {
                                    errorCount += errors.length;
                                }

                            case 49:
                                _iteratorNormalCompletion3 = true;
                                _context4.next = 25;
                                break;

                            case 52:
                                _context4.next = 58;
                                break;

                            case 54:
                                _context4.prev = 54;
                                _context4.t2 = _context4["catch"](23);
                                _didIteratorError3 = true;
                                _iteratorError3 = _context4.t2;

                            case 58:
                                _context4.prev = 58;
                                _context4.prev = 59;

                                if (!_iteratorNormalCompletion3 && _iterator3.return) {
                                    _iterator3.return();
                                }

                            case 61:
                                _context4.prev = 61;

                                if (!_didIteratorError3) {
                                    _context4.next = 64;
                                    break;
                                }

                                throw _iteratorError3;

                            case 64:
                                return _context4.finish(61);

                            case 65:
                                return _context4.finish(58);

                            case 66:
                                if (errorCount) {
                                    Logger_1.logger.error("✗ JavaScript: " + errorCount + " errors");
                                    Logger_1.logger.warn(lintOutputs.join('\n'));
                                } else {
                                    Logger_1.logger.success("✓ JavaScript");
                                }

                            case 67:
                                _iteratorNormalCompletion2 = true;
                                _context4.next = 8;
                                break;

                            case 70:
                                _context4.next = 76;
                                break;

                            case 72:
                                _context4.prev = 72;
                                _context4.t3 = _context4["catch"](6);
                                _didIteratorError2 = true;
                                _iteratorError2 = _context4.t3;

                            case 76:
                                _context4.prev = 76;
                                _context4.prev = 77;

                                if (!_iteratorNormalCompletion2 && _iterator2.return) {
                                    _iterator2.return();
                                }

                            case 79:
                                _context4.prev = 79;

                                if (!_didIteratorError2) {
                                    _context4.next = 82;
                                    break;
                                }

                                throw _iteratorError2;

                            case 82:
                                return _context4.finish(79);

                            case 83:
                                return _context4.finish(76);

                            case 84:
                                return _context4.abrupt("return", { results: results, errorCount: errorCount });

                            case 85:
                            case "end":
                                return _context4.stop();
                        }
                    }
                }, _callee4, this, [[6, 72, 76, 84], [11, 17], [23, 54, 58, 66], [32, 36, 40, 48], [41,, 43, 47], [59,, 61, 65], [77,, 79, 83]]);
            }));
        }
    }, {
        key: "lintSass",
        value: function lintSass(configFile, globs) {
            return __awaiter(this, void 0, Promise, regeneratorRuntime.mark(function _callee5() {
                var result, errorCount, warningCount, lintResult, lintOutputs, _iteratorNormalCompletion5, _didIteratorError5, _iteratorError5, _iterator5, _step5, _iteratorNormalCompletion6, _didIteratorError6, _iteratorError6, _iterator6, _step6, _err;

                return regeneratorRuntime.wrap(function _callee5$(_context5) {
                    while (1) {
                        switch (_context5.prev = _context5.next) {
                            case 0:
                                result = undefined;
                                errorCount = 0;
                                warningCount = 0;
                                _context5.prev = 3;
                                _context5.next = 6;
                                return exec_1.default("./node_modules/.bin/sass-lint -c " + configFile + " --format json --verbose", {
                                    noErrors: true,
                                    silent: true
                                });

                            case 6:
                                result = _context5.sent;
                                _context5.next = 14;
                                break;

                            case 9:
                                _context5.prev = 9;
                                _context5.t0 = _context5["catch"](3);

                                if (!_context5.t0.error) {
                                    _context5.next = 14;
                                    break;
                                }

                                Logger_1.logger.error("✗ Sass");
                                throw new Error(_context5.t0.error);

                            case 14:
                                lintResult = undefined;
                                lintOutputs = [];

                                if (!result.output) {
                                    _context5.next = 63;
                                    break;
                                }

                                lintResult = JSON.parse(result.output);
                                _iteratorNormalCompletion5 = true;
                                _didIteratorError5 = false;
                                _iteratorError5 = undefined;
                                _context5.prev = 21;
                                _iterator5 = lintResult[Symbol.iterator]();

                            case 23:
                                if (_iteratorNormalCompletion5 = (_step5 = _iterator5.next()).done) {
                                    _context5.next = 49;
                                    break;
                                }

                                result = _step5.value;

                                if (result.warningCount) warningCount += result.warningCount;
                                if (result.errorCount) errorCount += result.errorCount;
                                _iteratorNormalCompletion6 = true;
                                _didIteratorError6 = false;
                                _iteratorError6 = undefined;
                                _context5.prev = 30;
                                for (_iterator6 = result.messages[Symbol.iterator](); !(_iteratorNormalCompletion6 = (_step6 = _iterator6.next()).done); _iteratorNormalCompletion6 = true) {
                                    _err = _step6.value;

                                    lintOutputs.push(result.filePath + " (" + _err.line + ":" + _err.column + "): [severity: " + _err.severity + "] " + _err.ruleId + " (" + _err.message + ")");
                                }
                                _context5.next = 38;
                                break;

                            case 34:
                                _context5.prev = 34;
                                _context5.t1 = _context5["catch"](30);
                                _didIteratorError6 = true;
                                _iteratorError6 = _context5.t1;

                            case 38:
                                _context5.prev = 38;
                                _context5.prev = 39;

                                if (!_iteratorNormalCompletion6 && _iterator6.return) {
                                    _iterator6.return();
                                }

                            case 41:
                                _context5.prev = 41;

                                if (!_didIteratorError6) {
                                    _context5.next = 44;
                                    break;
                                }

                                throw _iteratorError6;

                            case 44:
                                return _context5.finish(41);

                            case 45:
                                return _context5.finish(38);

                            case 46:
                                _iteratorNormalCompletion5 = true;
                                _context5.next = 23;
                                break;

                            case 49:
                                _context5.next = 55;
                                break;

                            case 51:
                                _context5.prev = 51;
                                _context5.t2 = _context5["catch"](21);
                                _didIteratorError5 = true;
                                _iteratorError5 = _context5.t2;

                            case 55:
                                _context5.prev = 55;
                                _context5.prev = 56;

                                if (!_iteratorNormalCompletion5 && _iterator5.return) {
                                    _iterator5.return();
                                }

                            case 58:
                                _context5.prev = 58;

                                if (!_didIteratorError5) {
                                    _context5.next = 61;
                                    break;
                                }

                                throw _iteratorError5;

                            case 61:
                                return _context5.finish(58);

                            case 62:
                                return _context5.finish(55);

                            case 63:
                                if (warningCount || errorCount) {
                                    Logger_1.logger.error("✗ Sass: " + errorCount + " errors, " + warningCount + " warnings ");
                                    Logger_1.logger.warn(lintOutputs.join('\n'));
                                } else {
                                    Logger_1.logger.success("✓ Sass");
                                }
                                return _context5.abrupt("return", { result: lintResult, errorCount: errorCount, warningCount: warningCount });

                            case 65:
                            case "end":
                                return _context5.stop();
                        }
                    }
                }, _callee5, this, [[3, 9], [21, 51, 55, 63], [30, 34, 38, 46], [39,, 41, 45], [56,, 58, 62]]);
            }));
        }
    }, {
        key: "lintHtml",
        value: function lintHtml(configFile, globs) {
            return __awaiter(this, void 0, Promise, regeneratorRuntime.mark(function _callee6() {
                var results, errorResults, errorCount, lintOutputs, _iteratorNormalCompletion7, _didIteratorError7, _iteratorError7, _iterator7, _step7, _globPattern, _iteratorNormalCompletion8, _didIteratorError8, _iteratorError8, _iterator8, _step8, _result2, _iteratorNormalCompletion9, _didIteratorError9, _iteratorError9, _iterator9, _step9, _err2;

                return regeneratorRuntime.wrap(function _callee6$(_context6) {
                    while (1) {
                        switch (_context6.prev = _context6.next) {
                            case 0:
                                results = [];
                                errorResults = [];
                                errorCount = 0;
                                lintOutputs = [];
                                _iteratorNormalCompletion7 = true;
                                _didIteratorError7 = false;
                                _iteratorError7 = undefined;
                                _context6.prev = 7;
                                _iterator7 = globs[Symbol.iterator]();

                            case 9:
                                if (_iteratorNormalCompletion7 = (_step7 = _iterator7.next()).done) {
                                    _context6.next = 75;
                                    break;
                                }

                                _globPattern = _step7.value;
                                _context6.prev = 11;
                                _context6.t0 = results;
                                _context6.next = 15;
                                return exec_1.default("./node_modules/.bin/htmlhint -c " + configFile + " " + _globPattern + " --format json ", {
                                    noErrors: true,
                                    silent: true
                                });

                            case 15:
                                _context6.t1 = _context6.sent;

                                _context6.t0.push.call(_context6.t0, _context6.t1);

                                _context6.next = 27;
                                break;

                            case 19:
                                _context6.prev = 19;
                                _context6.t2 = _context6["catch"](11);

                                if (!_context6.t2.output) {
                                    _context6.next = 25;
                                    break;
                                }

                                errorResults = errorResults.concat(JSON.parse(_context6.t2.output));
                                _context6.next = 27;
                                break;

                            case 25:
                                Logger_1.logger.error("✗ HTML");
                                throw new Error(_context6.t2.error);

                            case 27:
                                if (!errorResults.length) {
                                    _context6.next = 72;
                                    break;
                                }

                                _iteratorNormalCompletion8 = true;
                                _didIteratorError8 = false;
                                _iteratorError8 = undefined;
                                _context6.prev = 31;
                                _iterator8 = errorResults[Symbol.iterator]();

                            case 33:
                                if (_iteratorNormalCompletion8 = (_step8 = _iterator8.next()).done) {
                                    _context6.next = 58;
                                    break;
                                }

                                _result2 = _step8.value;

                                if (!(_result2.messages && _result2.messages.length)) {
                                    _context6.next = 55;
                                    break;
                                }

                                _iteratorNormalCompletion9 = true;
                                _didIteratorError9 = false;
                                _iteratorError9 = undefined;
                                _context6.prev = 39;

                                for (_iterator9 = _result2.messages[Symbol.iterator](); !(_iteratorNormalCompletion9 = (_step9 = _iterator9.next()).done); _iteratorNormalCompletion9 = true) {
                                    _err2 = _step9.value;

                                    if (_err2.type === 'error') {
                                        lintOutputs.push(_result2.file + " (" + _err2.line + ":" + _err2.col + "): " + _err2.rule.description + " (" + _err2.rule.id + ")");
                                        errorCount++;
                                    }
                                }
                                _context6.next = 47;
                                break;

                            case 43:
                                _context6.prev = 43;
                                _context6.t3 = _context6["catch"](39);
                                _didIteratorError9 = true;
                                _iteratorError9 = _context6.t3;

                            case 47:
                                _context6.prev = 47;
                                _context6.prev = 48;

                                if (!_iteratorNormalCompletion9 && _iterator9.return) {
                                    _iterator9.return();
                                }

                            case 50:
                                _context6.prev = 50;

                                if (!_didIteratorError9) {
                                    _context6.next = 53;
                                    break;
                                }

                                throw _iteratorError9;

                            case 53:
                                return _context6.finish(50);

                            case 54:
                                return _context6.finish(47);

                            case 55:
                                _iteratorNormalCompletion8 = true;
                                _context6.next = 33;
                                break;

                            case 58:
                                _context6.next = 64;
                                break;

                            case 60:
                                _context6.prev = 60;
                                _context6.t4 = _context6["catch"](31);
                                _didIteratorError8 = true;
                                _iteratorError8 = _context6.t4;

                            case 64:
                                _context6.prev = 64;
                                _context6.prev = 65;

                                if (!_iteratorNormalCompletion8 && _iterator8.return) {
                                    _iterator8.return();
                                }

                            case 67:
                                _context6.prev = 67;

                                if (!_didIteratorError8) {
                                    _context6.next = 70;
                                    break;
                                }

                                throw _iteratorError8;

                            case 70:
                                return _context6.finish(67);

                            case 71:
                                return _context6.finish(64);

                            case 72:
                                _iteratorNormalCompletion7 = true;
                                _context6.next = 9;
                                break;

                            case 75:
                                _context6.next = 81;
                                break;

                            case 77:
                                _context6.prev = 77;
                                _context6.t5 = _context6["catch"](7);
                                _didIteratorError7 = true;
                                _iteratorError7 = _context6.t5;

                            case 81:
                                _context6.prev = 81;
                                _context6.prev = 82;

                                if (!_iteratorNormalCompletion7 && _iterator7.return) {
                                    _iterator7.return();
                                }

                            case 84:
                                _context6.prev = 84;

                                if (!_didIteratorError7) {
                                    _context6.next = 87;
                                    break;
                                }

                                throw _iteratorError7;

                            case 87:
                                return _context6.finish(84);

                            case 88:
                                return _context6.finish(81);

                            case 89:
                                if (errorCount) {
                                    Logger_1.logger.error("✗ HTML5: " + errorCount + " errors");
                                    Logger_1.logger.warn(lintOutputs.join('\n'));
                                } else {
                                    Logger_1.logger.success("✓ HTML5");
                                }
                                return _context6.abrupt("return", { results: results, errorCount: errorCount });

                            case 91:
                            case "end":
                                return _context6.stop();
                        }
                    }
                }, _callee6, this, [[7, 77, 81, 89], [11, 19], [31, 60, 64, 72], [39, 43, 47, 55], [48,, 50, 54], [65,, 67, 71], [82,, 84, 88]]);
            }));
        }
    }]);

    return LintCLI;
}();

exports.default = LintCLI;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbW1hbmRzL2xpbnQuanMiLCJjb21tYW5kcy9saW50LnRzIl0sIm5hbWVzIjpbIkxpbnRDTEkiLCJMaW50Q0xJLmNvbnN0cnVjdG9yIiwiTGludENMSS5leGVjIiwiTGludENMSS5saW50IiwiTGludENMSS5saW50VFMiLCJMaW50Q0xJLmxpbnRKUyIsIkxpbnRDTEkubGludFNhc3MiLCJMaW50Q0xJLmxpbnRIdG1sIl0sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUFBLElBQUksWUFBWSxTQUFDLElBQVEsVUFBSyxTQUFMLElBQW1CLFVBQVUsT0FBVixFQUFtQixVQUFuQixFQUErQixPQUEvQixFQUF3QyxTQUF4QyxFQUFtRDtBQUMzRixXQUFPLElBQUksT0FBSixDQUFZLFVBQVUsT0FBVixFQUFtQixNQUFuQixFQUEyQjtBQUMxQyxvQkFBWSxVQUFVLElBQVYsQ0FBZSxPQUFmLEVBQXdCLFVBQXhCLENBQVosQ0FEMEM7QUFFMUMsaUJBQVMsSUFBVCxDQUFjLEtBQWQsRUFBcUI7QUFBRSxtQkFBTyxpQkFBaUIsT0FBakIsSUFBNEIsTUFBTSxXQUFOLEtBQXNCLE9BQXRCLEdBQWdDLEtBQTVELEdBQW9FLElBQUksT0FBSixDQUFZLFVBQVUsT0FBVixFQUFtQjtBQUFFLHdCQUFRLEtBQVIsRUFBRjthQUFuQixDQUFoRixDQUFUO1NBQXJCO0FBQ0EsaUJBQVMsU0FBVCxDQUFtQixLQUFuQixFQUEwQjtBQUFFLGdCQUFJO0FBQUUscUJBQUssTUFBTCxFQUFhLEtBQWIsRUFBRjthQUFKLENBQTZCLE9BQU8sQ0FBUCxFQUFVO0FBQUUsdUJBQU8sQ0FBUCxFQUFGO2FBQVY7U0FBekQ7QUFDQSxpQkFBUyxRQUFULENBQWtCLEtBQWxCLEVBQXlCO0FBQUUsZ0JBQUk7QUFBRSxxQkFBSyxPQUFMLEVBQWMsS0FBZCxFQUFGO2FBQUosQ0FBOEIsT0FBTyxDQUFQLEVBQVU7QUFBRSx1QkFBTyxDQUFQLEVBQUY7YUFBVjtTQUF6RDtBQUNBLGlCQUFTLElBQVQsQ0FBYyxJQUFkLEVBQW9CLEtBQXBCLEVBQTJCO0FBQ3ZCLGdCQUFJLFNBQVMsVUFBVSxJQUFWLEVBQWdCLEtBQWhCLENBQVQsQ0FEbUI7QUFFdkIsbUJBQU8sSUFBUCxHQUFjLFFBQVEsT0FBTyxLQUFQLENBQXRCLEdBQXNDLEtBQUssT0FBTyxLQUFQLENBQUwsQ0FBbUIsSUFBbkIsQ0FBd0IsU0FBeEIsRUFBbUMsUUFBbkMsQ0FBdEMsQ0FGdUI7U0FBM0I7QUFJQSxhQUFLLE1BQUwsRUFBYSxLQUFLLENBQUwsQ0FBYixDQVQwQztLQUEzQixDQUFuQixDQUQyRjtDQUFuRDtBQ0E1QyxRQUFPLGdCQUFQO0FBQ0EsSUFBQSxXQUFBLFFBQXFCLGVBQXJCLENBQUE7QUFFQSxJQUFZLE1BQUcsUUFBTSxZQUFOLENBQUg7QUFDWixJQUFZLEtBQUUsUUFBTSxJQUFOLENBQUY7QUFDWixJQUFBLFNBQUEsUUFBNkMsYUFBN0MsQ0FBQTs7SUFFQTtBQStCRUEsYUEvQkYsT0ErQkVBLENBQW9CQSxPQUFwQkEsRUFBK0JBOzhCQS9CakMsU0ErQmlDQTs7QUFBWEMsYUFBQUEsT0FBQUEsR0FBQUEsT0FBQUEsQ0FBV0Q7QUE3QnhCQyxhQUFBQSxRQUFBQSxHQUFXQTtBQUNoQkEsa0JBQU1BO0FBQ0pBLDZCQUFhQSxpQ0FBYkE7YUFERkE7U0FES0EsQ0E2QndCRDtBQWxCdkJDLGFBQUFBLGNBQUFBLEdBQWlCQTtBQUN2QkEsb0JBQVFBO0FBQ05BLHNCQUFNQSxZQUFOQTtBQUNBQSx5QkFBU0EsK0JBQVRBO2FBRkZBO0FBSUFBLGtCQUFNQTtBQUNKQSxzQkFBTUEsbUJBQU5BO2FBREZBO0FBR0FBLHNCQUFVQTtBQUNSQSxzQkFBTUEsV0FBTkE7QUFDQUEseUJBQVNBLFdBQVRBO2FBRkZBO0FBSUFBLHNCQUFVQTtBQUNSQSxzQkFBTUEsTUFBTkE7QUFDQUEseUJBQVNBLFVBQVRBO2FBRkZBO1NBWk1BLENBa0J1QkQ7S0FBL0JBOztpQkEvQkY7OzZCQW1Db0JBLE1BQVFBO0FER3BCLG1CQUFPLFVBQVUsSUFBVixFQUFnQixLQUFLLENBQUwsRUFBUSxPQUF4QiwwQkFBaUM7b0JDRnhDRSxLQU1JQTs7Ozs7QUFOSkEsc0NBQU1BLEtBQUtBLENBQUxBLENBQU9BLENBQVBBOztvQ0FFTEEsS0FBS0EsUUFBTEEsQ0FBY0EsR0FBZEE7Ozs7O2lFQUEyQkE7Ozs4Q0FFeEJBO2dFQUNEQTs7Ozs7dUNBQ2dCQSxLQUFLQSxJQUFMQTs7O0FBQWZBOztxQ0FDQUEsT0FBT0EsVUFBUEE7Ozs7O2lFQUNLQTs7O0FBRVBBLHlDQUFBQSxNQUFBQSxDQUFPQSxPQUFQQSxDQUFlQSxnQkFBZkE7OztpRUFHS0E7OztpRUFFQUE7Ozs7Ozs7O2FEYmlDLENBQWpDLENBQVAsQ0NIb0JGOzs7OytCQW9CVEE7QURHWCxtQkFBTyxVQUFVLElBQVYsRUFBZ0IsS0FBSyxDQUFMLEVBQVEsT0FBeEIsMEJBQWlDO29CQ0Z4Q0csU0FFQUEseUdBSU9BLGFBQWFBLFlBQ2xCQSxTQUNBQTs7Ozs7O0FBUkZBLDBDQUFVQTtBQUVWQSw2Q0FBYUE7O0FBRWpCQSx5Q0FBQUEsTUFBQUEsQ0FBT0EsSUFBUEE7Ozs7OzRDQUV1Q0EsT0FBT0EsT0FBUEEsQ0FBZUEsS0FBS0EsT0FBTEE7Ozs7Ozs7OztBQUEzQ0E7QUFBYUE7QUFDbEJBO0FBQ0FBLHNDQUFNQSxLQUFLQSxjQUFMQSxDQUFvQkEsV0FBcEJBOytDQUVGQTtrRUFDREEsaUNBSUFBLCtCQUlBQSxtQ0FLQUE7Ozs7O3VDQVpZQSxLQUFLQSxNQUFMQSxDQUFZQSxXQUFXQSxNQUFYQSxFQUFtQkEsV0FBV0EsS0FBWEEsRUFBa0JBLElBQUlBLE9BQUpBOzs7QUFBaEVBOztBQUNBQSw4Q0FBY0EsUUFBT0EsVUFBUEE7Ozs7O3VDQUdDQSxLQUFLQSxNQUFMQSxDQUFZQSxXQUFXQSxNQUFYQSxFQUFtQkEsV0FBV0EsS0FBWEE7OztBQUE5Q0E7O0FBQ0FBLDhDQUFjQSxRQUFPQSxVQUFQQTs7Ozs7dUNBR0NBLEtBQUtBLFFBQUxBLENBQWNBLFdBQVdBLE1BQVhBLEVBQW1CQSxXQUFXQSxLQUFYQTs7O0FBQWhEQTs7QUFDQUEsOENBQWNBLFFBQU9BLFVBQVBBO0FBQ2RBLDhDQUFjQSxRQUFPQSxZQUFQQTs7Ozs7dUNBR0NBLEtBQUtBLFFBQUxBLENBQWNBLFdBQVdBLE1BQVhBLEVBQW1CQSxXQUFXQSxLQUFYQTs7O0FBQWhEQTs7QUFDQUEsOENBQWNBLFFBQU9BLFVBQVBBOzs7O3NDQUdSQSxJQUFJQSxLQUFKQSxjQUFxQkEsK0JBQXJCQTs7O0FBRVZBLHdDQUFRQSxXQUFSQSxJQUF1QkEsT0FBdkJBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7a0VBR0tBLEVBQUNBLHNCQUFEQSxFQUFhQSxnQkFBYkE7Ozs7Ozs7O2FEaENxQyxDQUFqQyxDQUFQLENDSFdIOzs7OytCQXNDR0EsWUFBbUJBLE9BQXFCQSxZQUFpQkE7QUREdkUsbUJBQU8sVUFBVSxJQUFWLEVBQWdCLEtBQUssQ0FBTCxFQUFRLE9BQXhCLDBCQUFpQztvQkNHeENJLEtBS0FBLFVBQ0FBLFFBQ0FBOzs7OztBQVBBQSxzQ0FBTUE7O3FDQUNOQSxHQUFHQSxVQUFIQSxDQUFjQSxVQUFkQTs7Ozs7K0NBQ0lBOzt1Q0FBaUJBLElBQUlBLFFBQUpBLENBQWFBLFVBQWJBLEVBQXlCQSxNQUF6QkE7Ozs7QUFBdkJBLG1EQUFXQTs7O0FBR1RBLDJDQUFXQSxRQUFRQSxVQUFSQSxFQUFvQkEsT0FBcEJBO0FBQ1hBLHlDQUFTQSxJQUFJQSxRQUFKQSxDQUFhQSxHQUFiQTs7dUNBQ01BLE9BQU9BLElBQVBBLENBQVlBLEtBQVpBOzs7QUFBZkE7O0FBQ0pBLG9DQUFJQSxPQUFPQSxZQUFQQSxFQUFxQkE7QUFDdkJBLDZDQUFBQSxNQUFBQSxDQUFPQSxLQUFQQSxvQkFBOEJBLE9BQU9BLFlBQVBBLFlBQTlCQSxFQUR1QkE7QUFFdkJBLDZDQUFBQSxNQUFBQSxDQUFPQSxJQUFQQSxDQUFZQSxPQUFPQSxNQUFQQSxDQUFjQSxJQUFkQSxDQUFtQkEsSUFBbkJBLENBQVpBLEVBRnVCQTtpQ0FBekJBLE1BR01BO0FBQ0pBLDZDQUFBQSxNQUFBQSxDQUFPQSxPQUFQQSxpQkFESUE7aUNBSE5BO0FBTUFBLHVDQUFPQSxVQUFQQSxHQUFvQkEsT0FBT0EsWUFBUEE7a0VBRWJBOzs7Ozs7OzthRG5CcUMsQ0FBakMsQ0FBUCxDQ0N1RUo7Ozs7K0JBcUJ6REEsWUFBbUJBLE9BQW1CQTtBREZwRCxtQkFBTyxVQUFVLElBQVYsRUFBZ0IsS0FBSyxDQUFMLEVBQVEsT0FBeEIsMEJBQWlDO29CQ0d4Q0ssU0FDQUEsUUFDQUEsaUdBQ0tBLGFBQ0hBLGdIQVVRQSxLQUFLQSw2RkFHSEE7Ozs7OztBQWpCWkE7QUFDQUE7QUFDQUEsNkNBQWFBOzs7Ozs2Q0FDT0E7Ozs7Ozs7O0FBQWZBO0FBQ0hBLDhDQUFjQTs7O3VDQUdEQSxPQUFBQSxPQUFBQSxjQUFxQkEsbUJBQWNBLGdDQUFuQ0EsRUFBa0VBO0FBQy9FQSw4Q0FBVUEsSUFBVkE7QUFDQUEsNENBQVFBLElBQVJBO2lDQUZhQTs7O0FBQWZBOzs7Ozs7OztBQUtBQSx5Q0FBU0EsS0FBS0EsS0FBTEEsQ0FBV0EsYUFBWUEsTUFBWkEsQ0FBcEJBOzs7Ozs2Q0FFMEJBLE9BQU9BLE9BQVBBLENBQWVBLE1BQWZBOzs7Ozs7Ozs7QUFBaEJBO0FBQUtBOzs7Ozs7QUFHYkEsa0RBQWlCQSx5QkFBakJBLHdHQUF5QkE7QUFBZkEsdURBQWVBOztBQUN2QkEsZ0RBQVlBLElBQVpBLENBQW9CQSxhQUFRQSxJQUFJQSxJQUFKQSxTQUFZQSxJQUFJQSxNQUFKQSxXQUFnQkEsSUFBSUEsT0FBSkEsQ0FBeERBLENBRHVCQTtpQ0FBekJBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUdBQSxvQ0FBSUEsT0FBT0EsTUFBUEEsRUFBZUE7QUFFakJBLGtEQUFjQSxPQUFPQSxNQUFQQSxDQUZHQTtpQ0FBbkJBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFPSkEsb0NBQUlBLFVBQUpBLEVBQWdCQTtBQUNkQSw2Q0FBQUEsTUFBQUEsQ0FBT0EsS0FBUEEsb0JBQThCQSxzQkFBOUJBLEVBRGNBO0FBRWRBLDZDQUFBQSxNQUFBQSxDQUFPQSxJQUFQQSxDQUFZQSxZQUFZQSxJQUFaQSxDQUFpQkEsSUFBakJBLENBQVpBLEVBRmNBO2lDQUFoQkEsTUFHTUE7QUFDSkEsNkNBQUFBLE1BQUFBLENBQU9BLE9BQVBBLGlCQURJQTtpQ0FITkE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztrRUFRS0EsRUFBQ0EsZ0JBQURBLEVBQVVBLHNCQUFWQTs7Ozs7Ozs7YUR0Q3FDLENBQWpDLENBQVAsQ0NFb0RMOzs7O2lDQXVDcENBLFlBQW1CQSxPQUFtQkE7QUROdEQsbUJBQU8sVUFBVSxJQUFWLEVBQWdCLEtBQUssQ0FBTCxFQUFRLE9BQXhCLDBCQUFpQztvQkNPeENNLFFBQ0FBLFlBQ0FBLGNBaUJBQSxZQUNBQSx1TEFPVUE7Ozs7OztBQTNCVkE7QUFDQUEsNkNBQWFBO0FBQ2JBLCtDQUFlQTs7O3VDQUdGQSxPQUFBQSxPQUFBQSx1Q0FBOENBLHVDQUE5Q0EsRUFBb0ZBO0FBQ2pHQSw4Q0FBVUEsSUFBVkE7QUFDQUEsNENBQVFBLElBQVJBO2lDQUZhQTs7O0FBQWZBOzs7Ozs7OztxQ0FLSUEsYUFBWUEsS0FBWkE7Ozs7O0FBQ0ZBLHlDQUFBQSxNQUFBQSxDQUFPQSxLQUFQQTtzQ0FFTUEsSUFBSUEsS0FBSkEsQ0FBVUEsYUFBWUEsS0FBWkE7OztBQU1oQkE7QUFDQUEsOENBQWNBOztxQ0FDZEEsT0FBT0EsTUFBUEE7Ozs7O0FBQ0ZBLDZDQUFhQSxLQUFLQSxLQUFMQSxDQUFXQSxPQUFPQSxNQUFQQSxDQUF4QkE7Ozs7OzZDQUNlQTs7Ozs7Ozs7QUFBVkE7O0FBQ0hBLG9DQUFJQSxPQUFPQSxZQUFQQSxFQUFxQkEsZ0JBQWdCQSxPQUFPQSxZQUFQQSxDQUF6Q0E7QUFDQUEsb0NBQUlBLE9BQU9BLFVBQVBBLEVBQW1CQSxjQUFjQSxPQUFPQSxVQUFQQSxDQUFyQ0E7Ozs7O0FBRUFBLGtEQUFpQkEsT0FBT0EsUUFBUEEsbUJBQWpCQSx3R0FBa0NBO0FBQXhCQSx3REFBd0JBOztBQUNoQ0EsZ0RBQVlBLElBQVpBLENBQW9CQSxPQUFPQSxRQUFQQSxVQUFvQkEsS0FBSUEsSUFBSkEsU0FBWUEsS0FBSUEsTUFBSkEsc0JBQTJCQSxLQUFJQSxRQUFKQSxVQUFpQkEsS0FBSUEsTUFBSkEsVUFBZUEsS0FBSUEsT0FBSkEsTUFBL0dBLEVBRGdDQTtpQ0FBbENBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQU1KQSxvQ0FBSUEsZ0JBQWdCQSxVQUFoQkEsRUFBNEJBO0FBQzlCQSw2Q0FBQUEsTUFBQUEsQ0FBT0EsS0FBUEEsY0FBd0JBLDJCQUFzQkEsMkJBQTlDQSxFQUQ4QkE7QUFFOUJBLDZDQUFBQSxNQUFBQSxDQUFPQSxJQUFQQSxDQUFZQSxZQUFZQSxJQUFaQSxDQUFpQkEsSUFBakJBLENBQVpBLEVBRjhCQTtpQ0FBaENBLE1BR01BO0FBQ0pBLDZDQUFBQSxNQUFBQSxDQUFPQSxPQUFQQSxXQURJQTtpQ0FITkE7a0VBT09BLEVBQUNBLFFBQVFBLFVBQVJBLEVBQW9CQSxzQkFBckJBLEVBQWlDQSwwQkFBakNBOzs7Ozs7OzthRC9DcUMsQ0FBakMsQ0FBUCxDQ01zRE47Ozs7aUNBNEN0Q0EsWUFBbUJBLE9BQW1CQTtBRFR0RCxtQkFBTyxVQUFVLElBQVYsRUFBZ0IsS0FBSyxDQUFMLEVBQVEsT0FBeEIsMEJBQWlDO29CQ1V4Q08sU0FDQUEsY0FDQUEsWUFDQUEsa0dBRUtBLG1HQWdCRUEsK0ZBRUtBOzs7Ozs7QUF2QlpBLDBDQUFVQTtBQUNWQSwrQ0FBZUE7QUFDZkEsNkNBQWFBO0FBQ2JBLDhDQUFjQTs7Ozs7NkNBRU1BOzs7Ozs7OztBQUFmQTs7K0NBRUxBOzt1Q0FBbUJBLE9BQUFBLE9BQUFBLHNDQUE2Q0EsbUJBQWNBLGdDQUEzREEsRUFBeUZBO0FBQzFHQSw4Q0FBVUEsSUFBVkE7QUFDQUEsNENBQVFBLElBQVJBO2lDQUZpQkE7Ozs7OzZDQUFYQTs7Ozs7Ozs7O3FDQUtKQSxhQUFZQSxNQUFaQTs7Ozs7QUFDRkEsK0NBQWVBLGFBQWFBLE1BQWJBLENBQW9CQSxLQUFLQSxLQUFMQSxDQUFXQSxhQUFZQSxNQUFaQSxDQUEvQkEsQ0FBZkE7Ozs7O0FBRUFBLHlDQUFBQSxNQUFBQSxDQUFPQSxLQUFQQTtzQ0FDTUEsSUFBSUEsS0FBSkEsQ0FBVUEsYUFBWUEsS0FBWkE7OztxQ0FJaEJBLGFBQWFBLE1BQWJBOzs7Ozs7Ozs7NkNBQ2VBOzs7Ozs7OztBQUFWQTs7c0NBQ0hBLFNBQU9BLFFBQVBBLElBQW1CQSxTQUFPQSxRQUFQQSxDQUFnQkEsTUFBaEJBOzs7Ozs7Ozs7O0FBQ3JCQSxrREFBaUJBLFNBQU9BLFFBQVBBLG1CQUFqQkEsd0dBQWtDQTtBQUF4QkEseURBQXdCQTs7QUFDaENBLHdDQUFJQSxNQUFJQSxJQUFKQSxLQUFhQSxPQUFiQSxFQUFzQkE7QUFDeEJBLG9EQUFZQSxJQUFaQSxDQUFvQkEsU0FBT0EsSUFBUEEsVUFBZ0JBLE1BQUlBLElBQUpBLFNBQVlBLE1BQUlBLEdBQUpBLFdBQWFBLE1BQUlBLElBQUpBLENBQVNBLFdBQVRBLFVBQXlCQSxNQUFJQSxJQUFKQSxDQUFTQSxFQUFUQSxNQUF0RkEsRUFEd0JBO0FBRXhCQSxxREFGd0JBO3FDQUExQkE7aUNBREZBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQVVOQSxvQ0FBSUEsVUFBSkEsRUFBZ0JBO0FBQ2RBLDZDQUFBQSxNQUFBQSxDQUFPQSxLQUFQQSxlQUF5QkEsc0JBQXpCQSxFQURjQTtBQUVkQSw2Q0FBQUEsTUFBQUEsQ0FBT0EsSUFBUEEsQ0FBWUEsWUFBWUEsSUFBWkEsQ0FBaUJBLElBQWpCQSxDQUFaQSxFQUZjQTtpQ0FBaEJBLE1BR01BO0FBQ0pBLDZDQUFBQSxNQUFBQSxDQUFPQSxPQUFQQSxZQURJQTtpQ0FITkE7a0VBT09BLEVBQUNBLFNBQVNBLE9BQVRBLEVBQWtCQSxzQkFBbkJBOzs7Ozs7OzthRGxEcUMsQ0FBakMsQ0FBUCxDQ1NzRFA7Ozs7V0FyTTlEOzs7QUFrUEEsUUFBQSxPQUFBLEdBQWUsT0FBZiIsImZpbGUiOiJjb21tYW5kcy9saW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsidmFyIF9fYXdhaXRlciA9ICh0aGlzICYmIHRoaXMuX19hd2FpdGVyKSB8fCBmdW5jdGlvbiAodGhpc0FyZywgX2FyZ3VtZW50cywgUHJvbWlzZSwgZ2VuZXJhdG9yKSB7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKGZ1bmN0aW9uIChyZXNvbHZlLCByZWplY3QpIHtcbiAgICAgICAgZ2VuZXJhdG9yID0gZ2VuZXJhdG9yLmNhbGwodGhpc0FyZywgX2FyZ3VtZW50cyk7XG4gICAgICAgIGZ1bmN0aW9uIGNhc3QodmFsdWUpIHsgcmV0dXJuIHZhbHVlIGluc3RhbmNlb2YgUHJvbWlzZSAmJiB2YWx1ZS5jb25zdHJ1Y3RvciA9PT0gUHJvbWlzZSA/IHZhbHVlIDogbmV3IFByb21pc2UoZnVuY3Rpb24gKHJlc29sdmUpIHsgcmVzb2x2ZSh2YWx1ZSk7IH0pOyB9XG4gICAgICAgIGZ1bmN0aW9uIG9uZnVsZmlsbCh2YWx1ZSkgeyB0cnkgeyBzdGVwKFwibmV4dFwiLCB2YWx1ZSk7IH0gY2F0Y2ggKGUpIHsgcmVqZWN0KGUpOyB9IH1cbiAgICAgICAgZnVuY3Rpb24gb25yZWplY3QodmFsdWUpIHsgdHJ5IHsgc3RlcChcInRocm93XCIsIHZhbHVlKTsgfSBjYXRjaCAoZSkgeyByZWplY3QoZSk7IH0gfVxuICAgICAgICBmdW5jdGlvbiBzdGVwKHZlcmIsIHZhbHVlKSB7XG4gICAgICAgICAgICB2YXIgcmVzdWx0ID0gZ2VuZXJhdG9yW3ZlcmJdKHZhbHVlKTtcbiAgICAgICAgICAgIHJlc3VsdC5kb25lID8gcmVzb2x2ZShyZXN1bHQudmFsdWUpIDogY2FzdChyZXN1bHQudmFsdWUpLnRoZW4ob25mdWxmaWxsLCBvbnJlamVjdCk7XG4gICAgICAgIH1cbiAgICAgICAgc3RlcChcIm5leHRcIiwgdm9pZCAwKTtcbiAgICB9KTtcbn07XG5yZXF1aXJlKCcuL0lDb21tYW5kTGluZScpO1xudmFyIExvZ2dlcl8xID0gcmVxdWlyZSgnLi4vbGliL0xvZ2dlcicpO1xudmFyIGZzcCA9IHJlcXVpcmUoJ2ZzLXByb21pc2UnKTtcbnZhciBmcyA9IHJlcXVpcmUoJ2ZzJyk7XG52YXIgZXhlY18xID0gcmVxdWlyZSgnLi4vbGliL2V4ZWMnKTtcbmNsYXNzIExpbnRDTEkge1xuICAgIGNvbnN0cnVjdG9yKGxpbnRlcnMpIHtcbiAgICAgICAgdGhpcy5saW50ZXJzID0gbGludGVycztcbiAgICAgICAgdGhpcy5jb21tYW5kcyA9IHtcbiAgICAgICAgICAgIGxpbnQ6IHtcbiAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogJ3J1biBsaW50ZXJzIG9uIGFsbCBzb3VyY2UgZmlsZXMnXG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMubGludGVyUGFja2FnZXMgPSB7XG4gICAgICAgICAgICB0c2xpbnQ6IHtcbiAgICAgICAgICAgICAgICBjb2RlOiAnVHlwZVNjcmlwdCcsXG4gICAgICAgICAgICAgICAgcGFja2FnZTogJ3Byb2plY3QtY2xpL2Rpc3QvbGliL0xpbnRlclRTJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGpzY3M6IHtcbiAgICAgICAgICAgICAgICBjb2RlOiAnSmF2YVNjcmlwdCBFUzIwMXgnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgc2Fzc2xpbnQ6IHtcbiAgICAgICAgICAgICAgICBjb2RlOiAnU0FTUy9TQ1NTJyxcbiAgICAgICAgICAgICAgICBwYWNrYWdlOiAnc2Fzcy1saW50J1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGh0bWxoaW50OiB7XG4gICAgICAgICAgICAgICAgY29kZTogJ0hUTUwnLFxuICAgICAgICAgICAgICAgIHBhY2thZ2U6ICdodG1saGludCdcbiAgICAgICAgICAgIH1cbiAgICAgICAgfTtcbiAgICB9XG4gICAgZXhlYyhhcmd2KSB7XG4gICAgICAgIHJldHVybiBfX2F3YWl0ZXIodGhpcywgdm9pZCAwLCBQcm9taXNlLCBmdW5jdGlvbiogKCkge1xuICAgICAgICAgICAgbGV0IGNtZCA9IGFyZ3YuX1swXTtcbiAgICAgICAgICAgIGlmICghdGhpcy5jb21tYW5kc1tjbWRdKVxuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgIHN3aXRjaCAoY21kKSB7XG4gICAgICAgICAgICAgICAgY2FzZSAnbGludCc6XG4gICAgICAgICAgICAgICAgICAgIGxldCByZXN1bHQgPSB5aWVsZCB0aGlzLmxpbnQoKTtcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3VsdC5lcnJvckNvdW50KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIExvZ2dlcl8xLmxvZ2dlci5zdWNjZXNzKCdsb29raW5nIGdvb2QgIScpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgfVxuICAgIGxpbnQoKSB7XG4gICAgICAgIHJldHVybiBfX2F3YWl0ZXIodGhpcywgdm9pZCAwLCBQcm9taXNlLCBmdW5jdGlvbiogKCkge1xuICAgICAgICAgICAgbGV0IHJlc3VsdHMgPSBbXTtcbiAgICAgICAgICAgIGxldCBlcnJvckNvdW50ID0gMDtcbiAgICAgICAgICAgIExvZ2dlcl8xLmxvZ2dlci5pbmZvKGBMaW50aW5nIGFsbCBjb2RlYCk7XG4gICAgICAgICAgICBmb3IgKGxldCBbcGFja2FnZU5hbWUsIHBhY2thZ2VPYmpdIG9mIE9iamVjdC5lbnRyaWVzKHRoaXMubGludGVycykpIHtcbiAgICAgICAgICAgICAgICBsZXQgcmVzdWx0O1xuICAgICAgICAgICAgICAgIGxldCBwa2cgPSB0aGlzLmxpbnRlclBhY2thZ2VzW3BhY2thZ2VOYW1lXTtcbiAgICAgICAgICAgICAgICBzd2l0Y2ggKHBhY2thZ2VOYW1lKSB7XG4gICAgICAgICAgICAgICAgICAgIGNhc2UgJ3RzbGludCc6XG4gICAgICAgICAgICAgICAgICAgICAgICByZXN1bHQgPSB5aWVsZCB0aGlzLmxpbnRUUyhwYWNrYWdlT2JqLmNvbmZpZywgcGFja2FnZU9iai5wYXRocywgcGtnLnBhY2thZ2UpO1xuICAgICAgICAgICAgICAgICAgICAgICAgZXJyb3JDb3VudCArPSByZXN1bHQuZXJyb3JDb3VudDtcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgICAgICBjYXNlICdqc2NzJzpcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3VsdCA9IHlpZWxkIHRoaXMubGludEpTKHBhY2thZ2VPYmouY29uZmlnLCBwYWNrYWdlT2JqLnBhdGhzKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGVycm9yQ291bnQgKz0gcmVzdWx0LmVycm9yQ291bnQ7XG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICAgICAgY2FzZSAnc2Fzc2xpbnQnOlxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdWx0ID0geWllbGQgdGhpcy5saW50U2FzcyhwYWNrYWdlT2JqLmNvbmZpZywgcGFja2FnZU9iai5wYXRocyk7XG4gICAgICAgICAgICAgICAgICAgICAgICBlcnJvckNvdW50ICs9IHJlc3VsdC5lcnJvckNvdW50O1xuICAgICAgICAgICAgICAgICAgICAgICAgZXJyb3JDb3VudCArPSByZXN1bHQud2FybmluZ0NvdW50O1xuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgICAgIGNhc2UgJ2h0bWxoaW50JzpcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3VsdCA9IHlpZWxkIHRoaXMubGludEh0bWwocGFja2FnZU9iai5jb25maWcsIHBhY2thZ2VPYmoucGF0aHMpO1xuICAgICAgICAgICAgICAgICAgICAgICAgZXJyb3JDb3VudCArPSByZXN1bHQuZXJyb3JDb3VudDtcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKGBsaW50ZXIgJyR7cGFja2FnZU5hbWV9JyBub3Qgc3VwcG9ydGVkYCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHJlc3VsdHNbcGFja2FnZU5hbWVdID0gcmVzdWx0O1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIHsgZXJyb3JDb3VudCwgcmVzdWx0cyB9O1xuICAgICAgICB9KTtcbiAgICB9XG4gICAgbGludFRTKGNvbmZpZ0ZpbGUsIGdsb2JzLCBtb2R1bGVQYXRoKSB7XG4gICAgICAgIHJldHVybiBfX2F3YWl0ZXIodGhpcywgdm9pZCAwLCBQcm9taXNlLCBmdW5jdGlvbiogKCkge1xuICAgICAgICAgICAgbGV0IGNmZyA9IHt9O1xuICAgICAgICAgICAgaWYgKGZzLmV4aXN0c1N5bmMoY29uZmlnRmlsZSkpIHtcbiAgICAgICAgICAgICAgICBjZmcgPSBKU09OLnBhcnNlKHlpZWxkIGZzcC5yZWFkRmlsZShjb25maWdGaWxlLCAndXRmOCcpKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGxldCBUU0xpbnRlciA9IHJlcXVpcmUobW9kdWxlUGF0aCkuZGVmYXVsdDtcbiAgICAgICAgICAgIGxldCBsaW50ZXIgPSBuZXcgVFNMaW50ZXIoY2ZnKTtcbiAgICAgICAgICAgIGxldCByZXN1bHQgPSB5aWVsZCBsaW50ZXIubGludChnbG9icyk7XG4gICAgICAgICAgICBpZiAocmVzdWx0LmZhaWx1cmVDb3VudCkge1xuICAgICAgICAgICAgICAgIExvZ2dlcl8xLmxvZ2dlci5lcnJvcihg4pyXIFR5cGVTY3JpcHQ6ICR7cmVzdWx0LmZhaWx1cmVDb3VudH0gZXJyb3JzYCk7XG4gICAgICAgICAgICAgICAgTG9nZ2VyXzEubG9nZ2VyLndhcm4ocmVzdWx0LmVycm9ycy5qb2luKCdcXG4nKSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICBMb2dnZXJfMS5sb2dnZXIuc3VjY2Vzcyhg4pyTIFR5cGVTY3JpcHRgKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJlc3VsdC5lcnJvckNvdW50ID0gcmVzdWx0LmZhaWx1cmVDb3VudDtcbiAgICAgICAgICAgIHJldHVybiByZXN1bHQ7XG4gICAgICAgIH0pO1xuICAgIH1cbiAgICBsaW50SlMoY29uZmlnRmlsZSwgZ2xvYnMpIHtcbiAgICAgICAgcmV0dXJuIF9fYXdhaXRlcih0aGlzLCB2b2lkIDAsIFByb21pc2UsIGZ1bmN0aW9uKiAoKSB7XG4gICAgICAgICAgICBsZXQgcmVzdWx0cztcbiAgICAgICAgICAgIGxldCByZXN1bHQ7XG4gICAgICAgICAgICBsZXQgZXJyb3JDb3VudCA9IDA7XG4gICAgICAgICAgICBmb3IgKGxldCBnbG9iUGF0dGVybiBvZiBnbG9icykge1xuICAgICAgICAgICAgICAgIGxldCBsaW50T3V0cHV0cyA9IFtdO1xuICAgICAgICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAgICAgICAgIHJlc3VsdCA9IHlpZWxkIGV4ZWNfMS5kZWZhdWx0KGBqc2NzIC1jICR7Y29uZmlnRmlsZX0gJHtnbG9iUGF0dGVybn0gLS1yZXBvcnRlciBqc29uYCwge1xuICAgICAgICAgICAgICAgICAgICAgICAgbm9FcnJvcnM6IHRydWUsXG4gICAgICAgICAgICAgICAgICAgICAgICBzaWxlbnQ6IHRydWVcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGNhdGNoIChlcnJvclJlc3VsdCkge1xuICAgICAgICAgICAgICAgICAgICByZXN1bHQgPSBKU09OLnBhcnNlKGVycm9yUmVzdWx0Lm91dHB1dCk7XG4gICAgICAgICAgICAgICAgICAgIGZvciAobGV0IFtrZXksIGVycm9yc10gb2YgT2JqZWN0LmVudHJpZXMocmVzdWx0KSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgZm9yIChsZXQgZXJyIG9mIGVycm9ycykge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxpbnRPdXRwdXRzLnB1c2goYCR7a2V5fSAoJHtlcnIubGluZX06JHtlcnIuY29sdW1ufSk6ICR7ZXJyLm1lc3NhZ2V9YCk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoZXJyb3JzLmxlbmd0aCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGVycm9yQ291bnQgKz0gZXJyb3JzLmxlbmd0aDtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBpZiAoZXJyb3JDb3VudCkge1xuICAgICAgICAgICAgICAgICAgICBMb2dnZXJfMS5sb2dnZXIuZXJyb3IoYOKclyBKYXZhU2NyaXB0OiAke2Vycm9yQ291bnR9IGVycm9yc2ApO1xuICAgICAgICAgICAgICAgICAgICBMb2dnZXJfMS5sb2dnZXIud2FybihsaW50T3V0cHV0cy5qb2luKCdcXG4nKSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBMb2dnZXJfMS5sb2dnZXIuc3VjY2Vzcyhg4pyTIEphdmFTY3JpcHRgKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4geyByZXN1bHRzLCBlcnJvckNvdW50IH07XG4gICAgICAgIH0pO1xuICAgIH1cbiAgICBsaW50U2Fzcyhjb25maWdGaWxlLCBnbG9icykge1xuICAgICAgICByZXR1cm4gX19hd2FpdGVyKHRoaXMsIHZvaWQgMCwgUHJvbWlzZSwgZnVuY3Rpb24qICgpIHtcbiAgICAgICAgICAgIGxldCByZXN1bHQ7XG4gICAgICAgICAgICBsZXQgZXJyb3JDb3VudCA9IDA7XG4gICAgICAgICAgICBsZXQgd2FybmluZ0NvdW50ID0gMDtcbiAgICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAgICAgcmVzdWx0ID0geWllbGQgZXhlY18xLmRlZmF1bHQoYC4vbm9kZV9tb2R1bGVzLy5iaW4vc2Fzcy1saW50IC1jICR7Y29uZmlnRmlsZX0gLS1mb3JtYXQganNvbiAtLXZlcmJvc2VgLCB7XG4gICAgICAgICAgICAgICAgICAgIG5vRXJyb3JzOiB0cnVlLFxuICAgICAgICAgICAgICAgICAgICBzaWxlbnQ6IHRydWVcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGNhdGNoIChlcnJvclJlc3VsdCkge1xuICAgICAgICAgICAgICAgIGlmIChlcnJvclJlc3VsdC5lcnJvcikge1xuICAgICAgICAgICAgICAgICAgICBMb2dnZXJfMS5sb2dnZXIuZXJyb3IoYOKclyBTYXNzYCk7XG4gICAgICAgICAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihlcnJvclJlc3VsdC5lcnJvcik7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgbGV0IGxpbnRSZXN1bHQ7XG4gICAgICAgICAgICBsZXQgbGludE91dHB1dHMgPSBbXTtcbiAgICAgICAgICAgIGlmIChyZXN1bHQub3V0cHV0KSB7XG4gICAgICAgICAgICAgICAgbGludFJlc3VsdCA9IEpTT04ucGFyc2UocmVzdWx0Lm91dHB1dCk7XG4gICAgICAgICAgICAgICAgZm9yIChyZXN1bHQgb2YgbGludFJlc3VsdCkge1xuICAgICAgICAgICAgICAgICAgICBpZiAocmVzdWx0Lndhcm5pbmdDb3VudClcbiAgICAgICAgICAgICAgICAgICAgICAgIHdhcm5pbmdDb3VudCArPSByZXN1bHQud2FybmluZ0NvdW50O1xuICAgICAgICAgICAgICAgICAgICBpZiAocmVzdWx0LmVycm9yQ291bnQpXG4gICAgICAgICAgICAgICAgICAgICAgICBlcnJvckNvdW50ICs9IHJlc3VsdC5lcnJvckNvdW50O1xuICAgICAgICAgICAgICAgICAgICBmb3IgKGxldCBlcnIgb2YgcmVzdWx0Lm1lc3NhZ2VzKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBsaW50T3V0cHV0cy5wdXNoKGAke3Jlc3VsdC5maWxlUGF0aH0gKCR7ZXJyLmxpbmV9OiR7ZXJyLmNvbHVtbn0pOiBbc2V2ZXJpdHk6ICR7ZXJyLnNldmVyaXR5fV0gJHtlcnIucnVsZUlkfSAoJHtlcnIubWVzc2FnZX0pYCk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAod2FybmluZ0NvdW50IHx8IGVycm9yQ291bnQpIHtcbiAgICAgICAgICAgICAgICBMb2dnZXJfMS5sb2dnZXIuZXJyb3IoYOKclyBTYXNzOiAke2Vycm9yQ291bnR9IGVycm9ycywgJHt3YXJuaW5nQ291bnR9IHdhcm5pbmdzIGApO1xuICAgICAgICAgICAgICAgIExvZ2dlcl8xLmxvZ2dlci53YXJuKGxpbnRPdXRwdXRzLmpvaW4oJ1xcbicpKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIExvZ2dlcl8xLmxvZ2dlci5zdWNjZXNzKGDinJMgU2Fzc2ApO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIHsgcmVzdWx0OiBsaW50UmVzdWx0LCBlcnJvckNvdW50LCB3YXJuaW5nQ291bnQgfTtcbiAgICAgICAgfSk7XG4gICAgfVxuICAgIGxpbnRIdG1sKGNvbmZpZ0ZpbGUsIGdsb2JzKSB7XG4gICAgICAgIHJldHVybiBfX2F3YWl0ZXIodGhpcywgdm9pZCAwLCBQcm9taXNlLCBmdW5jdGlvbiogKCkge1xuICAgICAgICAgICAgbGV0IHJlc3VsdHMgPSBbXTtcbiAgICAgICAgICAgIGxldCBlcnJvclJlc3VsdHMgPSBbXTtcbiAgICAgICAgICAgIGxldCBlcnJvckNvdW50ID0gMDtcbiAgICAgICAgICAgIGxldCBsaW50T3V0cHV0cyA9IFtdO1xuICAgICAgICAgICAgZm9yIChsZXQgZ2xvYlBhdHRlcm4gb2YgZ2xvYnMpIHtcbiAgICAgICAgICAgICAgICB0cnkge1xuICAgICAgICAgICAgICAgICAgICByZXN1bHRzLnB1c2goeWllbGQgZXhlY18xLmRlZmF1bHQoYC4vbm9kZV9tb2R1bGVzLy5iaW4vaHRtbGhpbnQgLWMgJHtjb25maWdGaWxlfSAke2dsb2JQYXR0ZXJufSAtLWZvcm1hdCBqc29uIGAsIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIG5vRXJyb3JzOiB0cnVlLFxuICAgICAgICAgICAgICAgICAgICAgICAgc2lsZW50OiB0cnVlXG4gICAgICAgICAgICAgICAgICAgIH0pKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgY2F0Y2ggKGVycm9yUmVzdWx0KSB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChlcnJvclJlc3VsdC5vdXRwdXQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGVycm9yUmVzdWx0cyA9IGVycm9yUmVzdWx0cy5jb25jYXQoSlNPTi5wYXJzZShlcnJvclJlc3VsdC5vdXRwdXQpKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIExvZ2dlcl8xLmxvZ2dlci5lcnJvcihg4pyXIEhUTUxgKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihlcnJvclJlc3VsdC5lcnJvcik7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgaWYgKGVycm9yUmVzdWx0cy5sZW5ndGgpXG4gICAgICAgICAgICAgICAgICAgIGZvciAobGV0IHJlc3VsdCBvZiBlcnJvclJlc3VsdHMpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChyZXN1bHQubWVzc2FnZXMgJiYgcmVzdWx0Lm1lc3NhZ2VzLmxlbmd0aCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZvciAobGV0IGVyciBvZiByZXN1bHQubWVzc2FnZXMpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGVyci50eXBlID09PSAnZXJyb3InKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBsaW50T3V0cHV0cy5wdXNoKGAke3Jlc3VsdC5maWxlfSAoJHtlcnIubGluZX06JHtlcnIuY29sfSk6ICR7ZXJyLnJ1bGUuZGVzY3JpcHRpb259ICgke2Vyci5ydWxlLmlkfSlgKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGVycm9yQ291bnQrKztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKGVycm9yQ291bnQpIHtcbiAgICAgICAgICAgICAgICBMb2dnZXJfMS5sb2dnZXIuZXJyb3IoYOKclyBIVE1MNTogJHtlcnJvckNvdW50fSBlcnJvcnNgKTtcbiAgICAgICAgICAgICAgICBMb2dnZXJfMS5sb2dnZXIud2FybihsaW50T3V0cHV0cy5qb2luKCdcXG4nKSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICBMb2dnZXJfMS5sb2dnZXIuc3VjY2Vzcyhg4pyTIEhUTUw1YCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4geyByZXN1bHRzOiByZXN1bHRzLCBlcnJvckNvdW50IH07XG4gICAgICAgIH0pO1xuICAgIH1cbn1cbmV4cG9ydHMuZGVmYXVsdCA9IExpbnRDTEk7XG4iLCJpbXBvcnQgJy4vSUNvbW1hbmRMaW5lJztcbmltcG9ydCB7bG9nZ2VyfSBmcm9tICcuLi9saWIvTG9nZ2VyJztcbi8vaW1wb3J0ICogYXMgcGF0aCBmcm9tICdwYXRoJztcbmltcG9ydCAqIGFzIGZzcCBmcm9tICdmcy1wcm9taXNlJztcbmltcG9ydCAqIGFzIGZzIGZyb20gJ2ZzJztcbmltcG9ydCBzaGVsbEV4ZWMsIHtleGVjMiBhcyBzaGVsbEV4ZWMyfSBmcm9tICcuLi9saWIvZXhlYyc7XG5cbmNsYXNzIExpbnRDTEkgaW1wbGVtZW50cyBJQ29tbWFuZExpbmUge1xuXG4gIHB1YmxpYyBjb21tYW5kcyA9IHtcbiAgICBsaW50OiB7XG4gICAgICBkZXNjcmlwdGlvbjogJ3J1biBsaW50ZXJzIG9uIGFsbCBzb3VyY2UgZmlsZXMnXG4gICAgfVxuICB9O1xuXG4gIC8qKlxuICAgKiBNZXRhZGF0YSBmb3Igc3VwcG9ydGVkIGxpbnRlcnNcbiAgICpcbiAgICogQHR5cGUge09iamVjdH1cbiAgICovXG4gIHByaXZhdGUgbGludGVyUGFja2FnZXMgPSB7XG4gICAgdHNsaW50OiB7XG4gICAgICBjb2RlOiAnVHlwZVNjcmlwdCcsXG4gICAgICBwYWNrYWdlOiAncHJvamVjdC1jbGkvZGlzdC9saWIvTGludGVyVFMnXG4gICAgfSxcbiAgICBqc2NzOiB7XG4gICAgICBjb2RlOiAnSmF2YVNjcmlwdCBFUzIwMXgnXG4gICAgfSxcbiAgICBzYXNzbGludDoge1xuICAgICAgY29kZTogJ1NBU1MvU0NTUycsXG4gICAgICBwYWNrYWdlOiAnc2Fzcy1saW50J1xuICAgIH0sXG4gICAgaHRtbGhpbnQ6IHtcbiAgICAgIGNvZGU6ICdIVE1MJyxcbiAgICAgIHBhY2thZ2U6ICdodG1saGludCdcbiAgICB9XG4gIH07XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBsaW50ZXJzOmFueSkge1xuXG4gIH1cblxuICBwdWJsaWMgYXN5bmMgZXhlYyhhcmd2OmFueSk6UHJvbWlzZTxhbnk+IHtcbiAgICBsZXQgY21kID0gYXJndi5fWzBdO1xuXG4gICAgaWYgKCF0aGlzLmNvbW1hbmRzW2NtZF0pIHJldHVybiBmYWxzZTtcblxuICAgIHN3aXRjaCAoY21kKSB7XG4gICAgICBjYXNlICdsaW50JzpcbiAgICAgICAgbGV0IHJlc3VsdCA9IGF3YWl0IHRoaXMubGludCgpO1xuICAgICAgICBpZiAocmVzdWx0LmVycm9yQ291bnQpIHtcbiAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgfWVsc2Uge1xuICAgICAgICAgIGxvZ2dlci5zdWNjZXNzKCdsb29raW5nIGdvb2QgIScpO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICBkZWZhdWx0OlxuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuICB9XG5cbiAgcHVibGljIGFzeW5jIGxpbnQoKSB7XG4gICAgbGV0IHJlc3VsdHMgPSBbXTtcblxuICAgIGxldCBlcnJvckNvdW50ID0gMDtcblxuICAgIGxvZ2dlci5pbmZvKGBMaW50aW5nIGFsbCBjb2RlYCk7XG5cbiAgICBmb3IgKCBsZXQgW3BhY2thZ2VOYW1lLCBwYWNrYWdlT2JqXSBvZiBPYmplY3QuZW50cmllcyh0aGlzLmxpbnRlcnMpKSB7XG4gICAgICBsZXQgcmVzdWx0O1xuICAgICAgbGV0IHBrZyA9IHRoaXMubGludGVyUGFja2FnZXNbcGFja2FnZU5hbWVdO1xuXG4gICAgICBzd2l0Y2ggKHBhY2thZ2VOYW1lKSB7XG4gICAgICAgIGNhc2UgJ3RzbGludCc6XG4gICAgICAgICAgcmVzdWx0ID0gYXdhaXQgdGhpcy5saW50VFMocGFja2FnZU9iai5jb25maWcsIHBhY2thZ2VPYmoucGF0aHMsIHBrZy5wYWNrYWdlKTtcbiAgICAgICAgICBlcnJvckNvdW50ICs9IHJlc3VsdC5lcnJvckNvdW50O1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICBjYXNlICdqc2NzJzpcbiAgICAgICAgICByZXN1bHQgPSBhd2FpdCB0aGlzLmxpbnRKUyhwYWNrYWdlT2JqLmNvbmZpZywgcGFja2FnZU9iai5wYXRocyk7XG4gICAgICAgICAgZXJyb3JDb3VudCArPSByZXN1bHQuZXJyb3JDb3VudDtcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgY2FzZSAnc2Fzc2xpbnQnOlxuICAgICAgICAgIHJlc3VsdCA9IGF3YWl0IHRoaXMubGludFNhc3MocGFja2FnZU9iai5jb25maWcsIHBhY2thZ2VPYmoucGF0aHMpO1xuICAgICAgICAgIGVycm9yQ291bnQgKz0gcmVzdWx0LmVycm9yQ291bnQ7XG4gICAgICAgICAgZXJyb3JDb3VudCArPSByZXN1bHQud2FybmluZ0NvdW50O1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICBjYXNlICdodG1saGludCc6XG4gICAgICAgICAgcmVzdWx0ID0gYXdhaXQgdGhpcy5saW50SHRtbChwYWNrYWdlT2JqLmNvbmZpZywgcGFja2FnZU9iai5wYXRocyk7XG4gICAgICAgICAgZXJyb3JDb3VudCArPSByZXN1bHQuZXJyb3JDb3VudDtcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoYGxpbnRlciAnJHtwYWNrYWdlTmFtZX0nIG5vdCBzdXBwb3J0ZWRgKTtcbiAgICAgIH1cbiAgICAgIHJlc3VsdHNbcGFja2FnZU5hbWVdID0gcmVzdWx0O1xuICAgIH1cblxuICAgIHJldHVybiB7ZXJyb3JDb3VudCwgcmVzdWx0c307XG4gIH1cblxuICBwdWJsaWMgYXN5bmMgbGludFRTKGNvbmZpZ0ZpbGU6c3RyaW5nLCBnbG9iczpBcnJheTxzdHJpbmc+LCBtb2R1bGVQYXRoOnN0cmluZyk6UHJvbWlzZTxhbnk+IHtcblxuICAgIGxldCBjZmcgPSB7fTtcbiAgICBpZiAoZnMuZXhpc3RzU3luYyhjb25maWdGaWxlKSkge1xuICAgICAgY2ZnID0gSlNPTi5wYXJzZShhd2FpdCBmc3AucmVhZEZpbGUoY29uZmlnRmlsZSwgJ3V0ZjgnKSk7XG4gICAgfVxuXG4gICAgbGV0IFRTTGludGVyID0gcmVxdWlyZShtb2R1bGVQYXRoKS5kZWZhdWx0O1xuICAgIGxldCBsaW50ZXIgPSBuZXcgVFNMaW50ZXIoY2ZnKTtcbiAgICBsZXQgcmVzdWx0ID0gYXdhaXQgbGludGVyLmxpbnQoZ2xvYnMpO1xuICAgIGlmIChyZXN1bHQuZmFpbHVyZUNvdW50KSB7XG4gICAgICBsb2dnZXIuZXJyb3IoYOKclyBUeXBlU2NyaXB0OiAke3Jlc3VsdC5mYWlsdXJlQ291bnR9IGVycm9yc2ApO1xuICAgICAgbG9nZ2VyLndhcm4ocmVzdWx0LmVycm9ycy5qb2luKCdcXG4nKSk7XG4gICAgfWVsc2Uge1xuICAgICAgbG9nZ2VyLnN1Y2Nlc3MoYOKckyBUeXBlU2NyaXB0YCk7XG4gICAgfVxuICAgIHJlc3VsdC5lcnJvckNvdW50ID0gcmVzdWx0LmZhaWx1cmVDb3VudDtcblxuICAgIHJldHVybiByZXN1bHQ7XG4gIH1cblxuICBwdWJsaWMgYXN5bmMgbGludEpTKGNvbmZpZ0ZpbGU6U3RyaW5nLCBnbG9iczpBcnJheTxzdHJpbmc+KSB7XG4gICAgbGV0IHJlc3VsdHM7XG4gICAgbGV0IHJlc3VsdDtcbiAgICBsZXQgZXJyb3JDb3VudCA9IDA7XG4gICAgZm9yIChsZXQgZ2xvYlBhdHRlcm4gb2YgZ2xvYnMpIHtcbiAgICAgIGxldCBsaW50T3V0cHV0cyA9IFtdO1xuXG4gICAgICB0cnkge1xuICAgICAgICByZXN1bHQgPSBhd2FpdCBzaGVsbEV4ZWMoYGpzY3MgLWMgJHtjb25maWdGaWxlfSAke2dsb2JQYXR0ZXJufSAtLXJlcG9ydGVyIGpzb25gLCB7XG4gICAgICAgICAgbm9FcnJvcnM6IHRydWUsXG4gICAgICAgICAgc2lsZW50OiB0cnVlXG4gICAgICAgIH0pO1xuICAgICAgfWNhdGNoIChlcnJvclJlc3VsdCkge1xuICAgICAgICByZXN1bHQgPSBKU09OLnBhcnNlKGVycm9yUmVzdWx0Lm91dHB1dCk7XG5cbiAgICAgICAgZm9yIChsZXQgW2tleSwgZXJyb3JzXSBvZiBPYmplY3QuZW50cmllcyhyZXN1bHQpKSB7XG4gICAgICAgIC8vIE9iamVjdC5rZXlzKHJlc3VsdCkubWFwKGVycm9ycyA9PiB7XG4gICAgICAgICAgLy9jb25zb2xlLmxvZygnZXJyb3JzJywga2V5LCBlcnJvcnMpO1xuICAgICAgICAgIGZvciAoIGxldCBlcnIgb2YgZXJyb3JzKSB7XG4gICAgICAgICAgICBsaW50T3V0cHV0cy5wdXNoKGAke2tleX0gKCR7ZXJyLmxpbmV9OiR7ZXJyLmNvbHVtbn0pOiAke2Vyci5tZXNzYWdlfWApO1xuICAgICAgICAgIH1cbiAgICAgICAgICBpZiAoZXJyb3JzLmxlbmd0aCkge1xuICAgICAgICAgICAgLy9saW50aW5nIGVycm9ycyBmb3VuZFxuICAgICAgICAgICAgZXJyb3JDb3VudCArPSBlcnJvcnMubGVuZ3RoO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICBpZiAoZXJyb3JDb3VudCkge1xuICAgICAgICBsb2dnZXIuZXJyb3IoYOKclyBKYXZhU2NyaXB0OiAke2Vycm9yQ291bnR9IGVycm9yc2ApO1xuICAgICAgICBsb2dnZXIud2FybihsaW50T3V0cHV0cy5qb2luKCdcXG4nKSk7XG4gICAgICB9ZWxzZSB7XG4gICAgICAgIGxvZ2dlci5zdWNjZXNzKGDinJMgSmF2YVNjcmlwdGApO1xuICAgICAgfVxuXG4gICAgfVxuICAgIHJldHVybiB7cmVzdWx0cywgZXJyb3JDb3VudH07XG4gIH1cblxuICBwdWJsaWMgYXN5bmMgbGludFNhc3MoY29uZmlnRmlsZTpTdHJpbmcsIGdsb2JzOkFycmF5PHN0cmluZz4pOlByb21pc2U8YW55PiB7XG4gICAgbGV0IHJlc3VsdDtcbiAgICBsZXQgZXJyb3JDb3VudCA9IDA7XG4gICAgbGV0IHdhcm5pbmdDb3VudCA9IDA7XG5cbiAgICB0cnkge1xuICAgICAgcmVzdWx0ID0gYXdhaXQgc2hlbGxFeGVjKGAuL25vZGVfbW9kdWxlcy8uYmluL3Nhc3MtbGludCAtYyAke2NvbmZpZ0ZpbGV9IC0tZm9ybWF0IGpzb24gLS12ZXJib3NlYCwge1xuICAgICAgICBub0Vycm9yczogdHJ1ZSxcbiAgICAgICAgc2lsZW50OiB0cnVlXG4gICAgICB9KTtcbiAgICB9Y2F0Y2ggKGVycm9yUmVzdWx0KSB7XG4gICAgICBpZiAoZXJyb3JSZXN1bHQuZXJyb3IpIHtcbiAgICAgICAgbG9nZ2VyLmVycm9yKGDinJcgU2Fzc2ApO1xuICAgICAgICAvL3Jlc3VsdCA9IEpTT04ucGFyc2UoZXJyb3JSZXN1bHQuZXJyb3IpO1xuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoZXJyb3JSZXN1bHQuZXJyb3IpO1xuICAgICAgfVxuICAgIH1cblxuICAgIC8vY29uc29sZS5sb2coJ3Jlc3VsdCcsIHJlc3VsdCk7XG5cbiAgICBsZXQgbGludFJlc3VsdDtcbiAgICBsZXQgbGludE91dHB1dHMgPSBbXTtcbiAgICBpZiAocmVzdWx0Lm91dHB1dCkge1xuICAgICAgbGludFJlc3VsdCA9IEpTT04ucGFyc2UocmVzdWx0Lm91dHB1dCk7XG4gICAgICBmb3IgKHJlc3VsdCBvZiBsaW50UmVzdWx0KSB7XG4gICAgICAgIGlmIChyZXN1bHQud2FybmluZ0NvdW50KSB3YXJuaW5nQ291bnQgKz0gcmVzdWx0Lndhcm5pbmdDb3VudDtcbiAgICAgICAgaWYgKHJlc3VsdC5lcnJvckNvdW50KSBlcnJvckNvdW50ICs9IHJlc3VsdC5lcnJvckNvdW50O1xuXG4gICAgICAgIGZvciAoIGxldCBlcnIgb2YgcmVzdWx0Lm1lc3NhZ2VzKSB7XG4gICAgICAgICAgbGludE91dHB1dHMucHVzaChgJHtyZXN1bHQuZmlsZVBhdGh9ICgke2Vyci5saW5lfToke2Vyci5jb2x1bW59KTogW3NldmVyaXR5OiAke2Vyci5zZXZlcml0eX1dICR7ZXJyLnJ1bGVJZH0gKCR7ZXJyLm1lc3NhZ2V9KWApO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG4gICAgaWYgKHdhcm5pbmdDb3VudCB8fCBlcnJvckNvdW50KSB7XG4gICAgICBsb2dnZXIuZXJyb3IoYOKclyBTYXNzOiAke2Vycm9yQ291bnR9IGVycm9ycywgJHt3YXJuaW5nQ291bnR9IHdhcm5pbmdzIGApO1xuICAgICAgbG9nZ2VyLndhcm4obGludE91dHB1dHMuam9pbignXFxuJykpO1xuICAgIH1lbHNlIHtcbiAgICAgIGxvZ2dlci5zdWNjZXNzKGDinJMgU2Fzc2ApO1xuICAgIH1cblxuICAgIHJldHVybiB7cmVzdWx0OiBsaW50UmVzdWx0LCBlcnJvckNvdW50LCB3YXJuaW5nQ291bnR9O1xuICB9XG5cbiAgcHVibGljIGFzeW5jIGxpbnRIdG1sKGNvbmZpZ0ZpbGU6U3RyaW5nLCBnbG9iczpBcnJheTxzdHJpbmc+KTpQcm9taXNlPGFueT4ge1xuICAgIGxldCByZXN1bHRzID0gW107XG4gICAgbGV0IGVycm9yUmVzdWx0cyA9IFtdO1xuICAgIGxldCBlcnJvckNvdW50ID0gMDtcbiAgICBsZXQgbGludE91dHB1dHMgPSBbXTtcblxuICAgIGZvciAobGV0IGdsb2JQYXR0ZXJuIG9mIGdsb2JzKSB7XG4gICAgICB0cnkge1xuICAgICAgICByZXN1bHRzLnB1c2goYXdhaXQgc2hlbGxFeGVjKGAuL25vZGVfbW9kdWxlcy8uYmluL2h0bWxoaW50IC1jICR7Y29uZmlnRmlsZX0gJHtnbG9iUGF0dGVybn0gLS1mb3JtYXQganNvbiBgLCB7XG4gICAgICAgICAgbm9FcnJvcnM6IHRydWUsXG4gICAgICAgICAgc2lsZW50OiB0cnVlXG4gICAgICAgIH0pKTtcbiAgICAgIH1jYXRjaCAoZXJyb3JSZXN1bHQpIHtcbiAgICAgICAgaWYgKGVycm9yUmVzdWx0Lm91dHB1dCkge1xuICAgICAgICAgIGVycm9yUmVzdWx0cyA9IGVycm9yUmVzdWx0cy5jb25jYXQoSlNPTi5wYXJzZShlcnJvclJlc3VsdC5vdXRwdXQpKTtcbiAgICAgICAgfWVsc2Uge1xuICAgICAgICAgIGxvZ2dlci5lcnJvcihg4pyXIEhUTUxgKTtcbiAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoZXJyb3JSZXN1bHQuZXJyb3IpO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIGlmIChlcnJvclJlc3VsdHMubGVuZ3RoKVxuICAgICAgZm9yIChsZXQgcmVzdWx0IG9mIGVycm9yUmVzdWx0cykge1xuICAgICAgICBpZiAocmVzdWx0Lm1lc3NhZ2VzICYmIHJlc3VsdC5tZXNzYWdlcy5sZW5ndGgpIHtcbiAgICAgICAgICBmb3IgKCBsZXQgZXJyIG9mIHJlc3VsdC5tZXNzYWdlcykge1xuICAgICAgICAgICAgaWYgKGVyci50eXBlID09PSAnZXJyb3InKSB7XG4gICAgICAgICAgICAgIGxpbnRPdXRwdXRzLnB1c2goYCR7cmVzdWx0LmZpbGV9ICgke2Vyci5saW5lfToke2Vyci5jb2x9KTogJHtlcnIucnVsZS5kZXNjcmlwdGlvbn0gKCR7ZXJyLnJ1bGUuaWR9KWApO1xuICAgICAgICAgICAgICBlcnJvckNvdW50Kys7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG4gICAgaWYgKGVycm9yQ291bnQpIHtcbiAgICAgIGxvZ2dlci5lcnJvcihg4pyXIEhUTUw1OiAke2Vycm9yQ291bnR9IGVycm9yc2ApO1xuICAgICAgbG9nZ2VyLndhcm4obGludE91dHB1dHMuam9pbignXFxuJykpO1xuICAgIH1lbHNlIHtcbiAgICAgIGxvZ2dlci5zdWNjZXNzKGDinJMgSFRNTDVgKTtcbiAgICB9XG5cbiAgICByZXR1cm4ge3Jlc3VsdHM6IHJlc3VsdHMsIGVycm9yQ291bnR9O1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IExpbnRDTEk7XG4iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
