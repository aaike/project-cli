"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var __awaiter = undefined && undefined.__awaiter || function (thisArg, _arguments, Promise, generator) {
    return new Promise(function (resolve, reject) {
        generator = generator.call(thisArg, _arguments);
        function cast(value) {
            return value instanceof Promise && value.constructor === Promise ? value : new Promise(function (resolve) {
                resolve(value);
            });
        }
        function onfulfill(value) {
            try {
                step("next", value);
            } catch (e) {
                reject(e);
            }
        }
        function onreject(value) {
            try {
                step("throw", value);
            } catch (e) {
                reject(e);
            }
        }
        function step(verb, value) {
            var result = generator[verb](value);
            result.done ? resolve(result.value) : cast(result.value).then(onfulfill, onreject);
        }
        step("next", void 0);
    });
};
require('./ICommandLine');
var exec_1 = require('../lib/exec');
var PM2Helper_1 = require('../lib/PM2Helper');

var PM2CLI = function () {
    function PM2CLI(config) {
        _classCallCheck(this, PM2CLI);

        this.config = config;
        this.commands = {
            start: {
                description: 'start all processes'
            },
            stop: {
                description: 'stop all processes'
            },
            restart: {
                description: 'restart all processes'
            },
            list: {
                description: 'list all processes'
            },
            log: {
                description: 'start streaming logs from pm2 processes'
            }
        };
    }

    _createClass(PM2CLI, [{
        key: "exec",
        value: function exec(argv) {
            return __awaiter(this, void 0, Promise, regeneratorRuntime.mark(function _callee() {
                return regeneratorRuntime.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                _context.t0 = argv._[0];
                                _context.next = _context.t0 === 'start' ? 3 : _context.t0 === 'stop' ? 6 : _context.t0 === 'restart' ? 9 : _context.t0 === 'list' ? 12 : _context.t0 === 'log' ? 15 : 18;
                                break;

                            case 3:
                                _context.next = 5;
                                return this.start(undefined, argv.log);

                            case 5:
                                return _context.abrupt("return", true);

                            case 6:
                                _context.next = 8;
                                return this.stop();

                            case 8:
                                return _context.abrupt("return", true);

                            case 9:
                                _context.next = 11;
                                return this.restart();

                            case 11:
                                return _context.abrupt("return", true);

                            case 12:
                                _context.next = 14;
                                return this.list();

                            case 14:
                                return _context.abrupt("return", true);

                            case 15:
                                _context.next = 17;
                                return this.log();

                            case 17:
                                return _context.abrupt("return", true);

                            case 18:
                                return _context.abrupt("return", false);

                            case 19:
                            case "end":
                                return _context.stop();
                        }
                    }
                }, _callee, this);
            }));
        }
    }, {
        key: "log",
        value: function log() {
            return __awaiter(this, void 0, Promise, regeneratorRuntime.mark(function _callee2() {
                return regeneratorRuntime.wrap(function _callee2$(_context2) {
                    while (1) {
                        switch (_context2.prev = _context2.next) {
                            case 0:
                                _context2.next = 2;
                                return exec_1.exec2('pm2', ['logs']);

                            case 2:
                            case "end":
                                return _context2.stop();
                        }
                    }
                }, _callee2, this);
            }));
        }
    }, {
        key: "list",
        value: function list() {
            return __awaiter(this, void 0, Promise, regeneratorRuntime.mark(function _callee3() {
                return regeneratorRuntime.wrap(function _callee3$(_context3) {
                    while (1) {
                        switch (_context3.prev = _context3.next) {
                            case 0:
                                _context3.next = 2;
                                return exec_1.exec2('pm2', ['list']);

                            case 2:
                            case "end":
                                return _context3.stop();
                        }
                    }
                }, _callee3, this);
            }));
        }
    }, {
        key: "start",
        value: function start() {
            var config = arguments.length <= 0 || arguments[0] === undefined ? undefined : arguments[0];
            var log = arguments.length <= 1 || arguments[1] === undefined ? false : arguments[1];

            return __awaiter(this, void 0, Promise, regeneratorRuntime.mark(function _callee4() {
                var err;
                return regeneratorRuntime.wrap(function _callee4$(_context4) {
                    while (1) {
                        switch (_context4.prev = _context4.next) {
                            case 0:
                                if (!config) config = this.config;
                                _context4.next = 3;
                                return PM2Helper_1.default.connect();

                            case 3:
                                err = _context4.sent;

                                if (!err) {
                                    _context4.next = 6;
                                    break;
                                }

                                throw new Error(err);

                            case 6:
                                _context4.next = 8;
                                return PM2Helper_1.default.startAll(config);

                            case 8:
                                _context4.next = 10;
                                return PM2Helper_1.default.disconnect();

                            case 10:
                            case "end":
                                return _context4.stop();
                        }
                    }
                }, _callee4, this);
            }));
        }
    }, {
        key: "stop",
        value: function stop() {
            var config = arguments.length <= 0 || arguments[0] === undefined ? undefined : arguments[0];

            return __awaiter(this, void 0, Promise, regeneratorRuntime.mark(function _callee5() {
                var err;
                return regeneratorRuntime.wrap(function _callee5$(_context5) {
                    while (1) {
                        switch (_context5.prev = _context5.next) {
                            case 0:
                                if (!config) config = this.config;
                                _context5.next = 3;
                                return PM2Helper_1.default.connect();

                            case 3:
                                err = _context5.sent;

                                if (!err) {
                                    _context5.next = 6;
                                    break;
                                }

                                throw new Error(err);

                            case 6:
                                _context5.next = 8;
                                return PM2Helper_1.default.stopAll(config);

                            case 8:
                                _context5.next = 10;
                                return PM2Helper_1.default.disconnect();

                            case 10:
                            case "end":
                                return _context5.stop();
                        }
                    }
                }, _callee5, this);
            }));
        }
    }, {
        key: "restart",
        value: function restart() {
            var config = arguments.length <= 0 || arguments[0] === undefined ? undefined : arguments[0];

            return __awaiter(this, void 0, Promise, regeneratorRuntime.mark(function _callee6() {
                var err;
                return regeneratorRuntime.wrap(function _callee6$(_context6) {
                    while (1) {
                        switch (_context6.prev = _context6.next) {
                            case 0:
                                if (!config) config = this.config;
                                _context6.next = 3;
                                return PM2Helper_1.default.connect();

                            case 3:
                                err = _context6.sent;

                                if (!err) {
                                    _context6.next = 6;
                                    break;
                                }

                                throw new Error(err);

                            case 6:
                                _context6.next = 8;
                                return PM2Helper_1.default.stopAll(config);

                            case 8:
                                _context6.next = 10;
                                return PM2Helper_1.default.startAll(config);

                            case 10:
                                _context6.next = 12;
                                return PM2Helper_1.default.disconnect();

                            case 12:
                            case "end":
                                return _context6.stop();
                        }
                    }
                }, _callee6, this);
            }));
        }
    }]);

    return PM2CLI;
}();

exports.default = PM2CLI;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbW1hbmRzL3BtMi5qcyIsImNvbW1hbmRzL3BtMi50cyJdLCJuYW1lcyI6WyJQTTJDTEkiLCJQTTJDTEkuY29uc3RydWN0b3IiLCJQTTJDTEkuZXhlYyIsIlBNMkNMSS5sb2ciLCJQTTJDTEkubGlzdCIsIlBNMkNMSS5zdGFydCIsIlBNMkNMSS5zdG9wIiwiUE0yQ0xJLnJlc3RhcnQiXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLElBQUksWUFBWSxTQUFDLElBQVEsVUFBSyxTQUFMLElBQW1CLFVBQVUsT0FBVixFQUFtQixVQUFuQixFQUErQixPQUEvQixFQUF3QyxTQUF4QyxFQUFtRDtBQUMzRixXQUFPLElBQUksT0FBSixDQUFZLFVBQVUsT0FBVixFQUFtQixNQUFuQixFQUEyQjtBQUMxQyxvQkFBWSxVQUFVLElBQVYsQ0FBZSxPQUFmLEVBQXdCLFVBQXhCLENBQVosQ0FEMEM7QUFFMUMsaUJBQVMsSUFBVCxDQUFjLEtBQWQsRUFBcUI7QUFBRSxtQkFBTyxpQkFBaUIsT0FBakIsSUFBNEIsTUFBTSxXQUFOLEtBQXNCLE9BQXRCLEdBQWdDLEtBQTVELEdBQW9FLElBQUksT0FBSixDQUFZLFVBQVUsT0FBVixFQUFtQjtBQUFFLHdCQUFRLEtBQVIsRUFBRjthQUFuQixDQUFoRixDQUFUO1NBQXJCO0FBQ0EsaUJBQVMsU0FBVCxDQUFtQixLQUFuQixFQUEwQjtBQUFFLGdCQUFJO0FBQUUscUJBQUssTUFBTCxFQUFhLEtBQWIsRUFBRjthQUFKLENBQTZCLE9BQU8sQ0FBUCxFQUFVO0FBQUUsdUJBQU8sQ0FBUCxFQUFGO2FBQVY7U0FBekQ7QUFDQSxpQkFBUyxRQUFULENBQWtCLEtBQWxCLEVBQXlCO0FBQUUsZ0JBQUk7QUFBRSxxQkFBSyxPQUFMLEVBQWMsS0FBZCxFQUFGO2FBQUosQ0FBOEIsT0FBTyxDQUFQLEVBQVU7QUFBRSx1QkFBTyxDQUFQLEVBQUY7YUFBVjtTQUF6RDtBQUNBLGlCQUFTLElBQVQsQ0FBYyxJQUFkLEVBQW9CLEtBQXBCLEVBQTJCO0FBQ3ZCLGdCQUFJLFNBQVMsVUFBVSxJQUFWLEVBQWdCLEtBQWhCLENBQVQsQ0FEbUI7QUFFdkIsbUJBQU8sSUFBUCxHQUFjLFFBQVEsT0FBTyxLQUFQLENBQXRCLEdBQXNDLEtBQUssT0FBTyxLQUFQLENBQUwsQ0FBbUIsSUFBbkIsQ0FBd0IsU0FBeEIsRUFBbUMsUUFBbkMsQ0FBdEMsQ0FGdUI7U0FBM0I7QUFJQSxhQUFLLE1BQUwsRUFBYSxLQUFLLENBQUwsQ0FBYixDQVQwQztLQUEzQixDQUFuQixDQUQyRjtDQUFuRDtBQ0E1QyxRQUFPLGdCQUFQO0FBQ0EsSUFBQSxTQUFBLFFBQWtDLGFBQWxDLENBQUE7QUFDQSxJQUFBLGNBQUEsUUFBc0Isa0JBQXRCLENBQUE7O0lBRUE7QUFvQkVBLGFBcEJGLE1Bb0JFQSxDQUFvQkEsTUFBcEJBLEVBQXFDQTs4QkFwQnZDLFFBb0J1Q0E7O0FBQWpCQyxhQUFBQSxNQUFBQSxHQUFBQSxNQUFBQSxDQUFpQkQ7QUFsQjlCQyxhQUFBQSxRQUFBQSxHQUFXQTtBQUNoQkEsbUJBQU9BO0FBQ0xBLDZCQUFhQSxxQkFBYkE7YUFERkE7QUFHQUEsa0JBQU1BO0FBQ0pBLDZCQUFhQSxvQkFBYkE7YUFERkE7QUFHQUEscUJBQVNBO0FBQ1BBLDZCQUFhQSx1QkFBYkE7YUFERkE7QUFHQUEsa0JBQU1BO0FBQ0pBLDZCQUFhQSxvQkFBYkE7YUFERkE7QUFHQUEsaUJBQUtBO0FBQ0hBLDZCQUFhQSx5Q0FBYkE7YUFERkE7U0FiS0EsQ0FrQjhCRDtLQUFyQ0E7O2lCQXBCRjs7NkJBd0JvQkEsTUFBUUE7QURVcEIsbUJBQU8sVUFBVSxJQUFWLEVBQWdCLEtBQUssQ0FBTCxFQUFRLE9BQXhCLDBCQUFpQzs7Ozs7OENDVHBDRSxLQUFLQSxDQUFMQSxDQUFPQSxDQUFQQTtnRUFDREEsOEJBR0FBLDZCQUdBQSxnQ0FHQUEsOEJBR0FBOzs7Ozt1Q0FYR0EsS0FBS0EsS0FBTEEsQ0FBV0EsU0FBWEEsRUFBc0JBLEtBQUtBLEdBQUxBOzs7aUVBQ3JCQTs7Ozt1Q0FFREEsS0FBS0EsSUFBTEE7OztpRUFDQ0E7Ozs7dUNBRURBLEtBQUtBLE9BQUxBOzs7aUVBQ0NBOzs7O3VDQUVEQSxLQUFLQSxJQUFMQTs7O2lFQUNDQTs7Ozt1Q0FFREEsS0FBS0EsR0FBTEE7OztpRUFDQ0E7OztpRUFFQUE7Ozs7Ozs7O2FEUmlDLENBQWpDLENBQVAsQ0NWb0JGOzs7OzhCQTJCVkE7QURNVixtQkFBTyxVQUFVLElBQVYsRUFBZ0IsS0FBSyxDQUFMLEVBQVEsT0FBeEIsMEJBQWlDOzs7Ozs7dUNDTHRDRyxPQUFBQSxLQUFBQSxDQUFXQSxLQUFYQSxFQUFrQkEsQ0FBQ0EsTUFBREEsQ0FBbEJBOzs7Ozs7OzthREtzQyxDQUFqQyxDQUFQLENDTlVIOzs7OytCQUlDQTtBRE9YLG1CQUFPLFVBQVUsSUFBVixFQUFnQixLQUFLLENBQUwsRUFBUSxPQUF4QiwwQkFBaUM7Ozs7Ozt1Q0NOdENJLE9BQUFBLEtBQUFBLENBQVdBLEtBQVhBLEVBQWtCQSxDQUFDQSxNQUFEQSxDQUFsQkE7Ozs7Ozs7O2FETXNDLENBQWpDLENBQVAsQ0NQV0o7Ozs7Z0NBU29EQTtnQkFBbERBLCtEQUFvQkEseUJBQThCQTtnQkFBbkJBLDREQUFjQSxxQkFBS0E7O0FERy9ELG1CQUFPLFVBQVUsSUFBVixFQUFnQixLQUFLLENBQUwsRUFBUSxPQUF4QiwwQkFBaUM7b0JDQXhDSzs7Ozs7QUFGSkEsb0NBQUlBLENBQUNBLE1BQURBLEVBQVNBLFNBQVNBLEtBQUtBLE1BQUxBLENBQXRCQTs7dUNBRWdCQSxZQUFBQSxPQUFBQSxDQUFVQSxPQUFWQTs7O0FBQVpBOztxQ0FDQUE7Ozs7O3NDQUFXQSxJQUFJQSxLQUFKQSxDQUFVQSxHQUFWQTs7Ozt1Q0FFVEEsWUFBQUEsT0FBQUEsQ0FBVUEsUUFBVkEsQ0FBbUJBLE1BQW5CQTs7Ozt1Q0FNQUEsWUFBQUEsT0FBQUEsQ0FBVUEsVUFBVkE7Ozs7Ozs7O2FEVHNDLENBQWpDLENBQVAsQ0NIK0RMOzs7OytCQW9CdEJBO2dCQUE3QkEsK0RBQW9CQSx5QkFBU0E7O0FETnpDLG1CQUFPLFVBQVUsSUFBVixFQUFnQixLQUFLLENBQUwsRUFBUSxPQUF4QiwwQkFBaUM7b0JDU3hDTTs7Ozs7QUFGSkEsb0NBQUlBLENBQUNBLE1BQURBLEVBQVNBLFNBQVNBLEtBQUtBLE1BQUxBLENBQXRCQTs7dUNBRWdCQSxZQUFBQSxPQUFBQSxDQUFVQSxPQUFWQTs7O0FBQVpBOztxQ0FDQUE7Ozs7O3NDQUFXQSxJQUFJQSxLQUFKQSxDQUFVQSxHQUFWQTs7Ozt1Q0FFVEEsWUFBQUEsT0FBQUEsQ0FBVUEsT0FBVkEsQ0FBa0JBLE1BQWxCQTs7Ozt1Q0FNQUEsWUFBQUEsT0FBQUEsQ0FBVUEsVUFBVkE7Ozs7Ozs7O2FEbEJzQyxDQUFqQyxDQUFQLENDTXlDTjs7OztrQ0FvQk1BO2dCQUFoQ0EsK0RBQXVCQSx5QkFBU0E7O0FEZi9DLG1CQUFPLFVBQVUsSUFBVixFQUFnQixLQUFLLENBQUwsRUFBUSxPQUF4QiwwQkFBaUM7b0JDa0J4Q087Ozs7O0FBRkpBLG9DQUFJQSxDQUFDQSxNQUFEQSxFQUFTQSxTQUFTQSxLQUFLQSxNQUFMQSxDQUF0QkE7O3VDQUVnQkEsWUFBQUEsT0FBQUEsQ0FBVUEsT0FBVkE7OztBQUFaQTs7cUNBQ0FBOzs7OztzQ0FBV0EsSUFBSUEsS0FBSkEsQ0FBVUEsR0FBVkE7Ozs7dUNBRVRBLFlBQUFBLE9BQUFBLENBQVVBLE9BQVZBLENBQWtCQSxNQUFsQkE7Ozs7dUNBRUFBLFlBQUFBLE9BQUFBLENBQVVBLFFBQVZBLENBQW1CQSxNQUFuQkE7Ozs7dUNBRUFBLFlBQUFBLE9BQUFBLENBQVVBLFVBQVZBOzs7Ozs7OzthRHpCc0MsQ0FBakMsQ0FBUCxDQ2UrQ1A7Ozs7V0F4R3ZEOzs7QUFzSEEsUUFBQSxPQUFBLEdBQWUsTUFBZiIsImZpbGUiOiJjb21tYW5kcy9wbTIuanMiLCJzb3VyY2VzQ29udGVudCI6WyJ2YXIgX19hd2FpdGVyID0gKHRoaXMgJiYgdGhpcy5fX2F3YWl0ZXIpIHx8IGZ1bmN0aW9uICh0aGlzQXJnLCBfYXJndW1lbnRzLCBQcm9taXNlLCBnZW5lcmF0b3IpIHtcbiAgICByZXR1cm4gbmV3IFByb21pc2UoZnVuY3Rpb24gKHJlc29sdmUsIHJlamVjdCkge1xuICAgICAgICBnZW5lcmF0b3IgPSBnZW5lcmF0b3IuY2FsbCh0aGlzQXJnLCBfYXJndW1lbnRzKTtcbiAgICAgICAgZnVuY3Rpb24gY2FzdCh2YWx1ZSkgeyByZXR1cm4gdmFsdWUgaW5zdGFuY2VvZiBQcm9taXNlICYmIHZhbHVlLmNvbnN0cnVjdG9yID09PSBQcm9taXNlID8gdmFsdWUgOiBuZXcgUHJvbWlzZShmdW5jdGlvbiAocmVzb2x2ZSkgeyByZXNvbHZlKHZhbHVlKTsgfSk7IH1cbiAgICAgICAgZnVuY3Rpb24gb25mdWxmaWxsKHZhbHVlKSB7IHRyeSB7IHN0ZXAoXCJuZXh0XCIsIHZhbHVlKTsgfSBjYXRjaCAoZSkgeyByZWplY3QoZSk7IH0gfVxuICAgICAgICBmdW5jdGlvbiBvbnJlamVjdCh2YWx1ZSkgeyB0cnkgeyBzdGVwKFwidGhyb3dcIiwgdmFsdWUpOyB9IGNhdGNoIChlKSB7IHJlamVjdChlKTsgfSB9XG4gICAgICAgIGZ1bmN0aW9uIHN0ZXAodmVyYiwgdmFsdWUpIHtcbiAgICAgICAgICAgIHZhciByZXN1bHQgPSBnZW5lcmF0b3JbdmVyYl0odmFsdWUpO1xuICAgICAgICAgICAgcmVzdWx0LmRvbmUgPyByZXNvbHZlKHJlc3VsdC52YWx1ZSkgOiBjYXN0KHJlc3VsdC52YWx1ZSkudGhlbihvbmZ1bGZpbGwsIG9ucmVqZWN0KTtcbiAgICAgICAgfVxuICAgICAgICBzdGVwKFwibmV4dFwiLCB2b2lkIDApO1xuICAgIH0pO1xufTtcbnJlcXVpcmUoJy4vSUNvbW1hbmRMaW5lJyk7XG52YXIgZXhlY18xID0gcmVxdWlyZSgnLi4vbGliL2V4ZWMnKTtcbnZhciBQTTJIZWxwZXJfMSA9IHJlcXVpcmUoJy4uL2xpYi9QTTJIZWxwZXInKTtcbmNsYXNzIFBNMkNMSSB7XG4gICAgY29uc3RydWN0b3IoY29uZmlnKSB7XG4gICAgICAgIHRoaXMuY29uZmlnID0gY29uZmlnO1xuICAgICAgICB0aGlzLmNvbW1hbmRzID0ge1xuICAgICAgICAgICAgc3RhcnQ6IHtcbiAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogJ3N0YXJ0IGFsbCBwcm9jZXNzZXMnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgc3RvcDoge1xuICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiAnc3RvcCBhbGwgcHJvY2Vzc2VzJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHJlc3RhcnQ6IHtcbiAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogJ3Jlc3RhcnQgYWxsIHByb2Nlc3NlcydcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBsaXN0OiB7XG4gICAgICAgICAgICAgICAgZGVzY3JpcHRpb246ICdsaXN0IGFsbCBwcm9jZXNzZXMnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgbG9nOiB7XG4gICAgICAgICAgICAgICAgZGVzY3JpcHRpb246ICdzdGFydCBzdHJlYW1pbmcgbG9ncyBmcm9tIHBtMiBwcm9jZXNzZXMnXG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG4gICAgfVxuICAgIGV4ZWMoYXJndikge1xuICAgICAgICByZXR1cm4gX19hd2FpdGVyKHRoaXMsIHZvaWQgMCwgUHJvbWlzZSwgZnVuY3Rpb24qICgpIHtcbiAgICAgICAgICAgIHN3aXRjaCAoYXJndi5fWzBdKSB7XG4gICAgICAgICAgICAgICAgY2FzZSAnc3RhcnQnOlxuICAgICAgICAgICAgICAgICAgICB5aWVsZCB0aGlzLnN0YXJ0KHVuZGVmaW5lZCwgYXJndi5sb2cpO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAgICAgICBjYXNlICdzdG9wJzpcbiAgICAgICAgICAgICAgICAgICAgeWllbGQgdGhpcy5zdG9wKCk7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgICAgICAgIGNhc2UgJ3Jlc3RhcnQnOlxuICAgICAgICAgICAgICAgICAgICB5aWVsZCB0aGlzLnJlc3RhcnQoKTtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgICAgICAgY2FzZSAnbGlzdCc6XG4gICAgICAgICAgICAgICAgICAgIHlpZWxkIHRoaXMubGlzdCgpO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAgICAgICBjYXNlICdsb2cnOlxuICAgICAgICAgICAgICAgICAgICB5aWVsZCB0aGlzLmxvZygpO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH1cbiAgICBsb2coKSB7XG4gICAgICAgIHJldHVybiBfX2F3YWl0ZXIodGhpcywgdm9pZCAwLCBQcm9taXNlLCBmdW5jdGlvbiogKCkge1xuICAgICAgICAgICAgeWllbGQgZXhlY18xLmV4ZWMyKCdwbTInLCBbJ2xvZ3MnXSk7XG4gICAgICAgIH0pO1xuICAgIH1cbiAgICBsaXN0KCkge1xuICAgICAgICByZXR1cm4gX19hd2FpdGVyKHRoaXMsIHZvaWQgMCwgUHJvbWlzZSwgZnVuY3Rpb24qICgpIHtcbiAgICAgICAgICAgIHlpZWxkIGV4ZWNfMS5leGVjMigncG0yJywgWydsaXN0J10pO1xuICAgICAgICB9KTtcbiAgICB9XG4gICAgc3RhcnQoY29uZmlnID0gdW5kZWZpbmVkLCBsb2cgPSBmYWxzZSkge1xuICAgICAgICByZXR1cm4gX19hd2FpdGVyKHRoaXMsIHZvaWQgMCwgUHJvbWlzZSwgZnVuY3Rpb24qICgpIHtcbiAgICAgICAgICAgIGlmICghY29uZmlnKVxuICAgICAgICAgICAgICAgIGNvbmZpZyA9IHRoaXMuY29uZmlnO1xuICAgICAgICAgICAgbGV0IGVyciA9IHlpZWxkIFBNMkhlbHBlcl8xLmRlZmF1bHQuY29ubmVjdCgpO1xuICAgICAgICAgICAgaWYgKGVycilcbiAgICAgICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoZXJyKTtcbiAgICAgICAgICAgIHlpZWxkIFBNMkhlbHBlcl8xLmRlZmF1bHQuc3RhcnRBbGwoY29uZmlnKTtcbiAgICAgICAgICAgIHlpZWxkIFBNMkhlbHBlcl8xLmRlZmF1bHQuZGlzY29ubmVjdCgpO1xuICAgICAgICB9KTtcbiAgICB9XG4gICAgc3RvcChjb25maWcgPSB1bmRlZmluZWQpIHtcbiAgICAgICAgcmV0dXJuIF9fYXdhaXRlcih0aGlzLCB2b2lkIDAsIFByb21pc2UsIGZ1bmN0aW9uKiAoKSB7XG4gICAgICAgICAgICBpZiAoIWNvbmZpZylcbiAgICAgICAgICAgICAgICBjb25maWcgPSB0aGlzLmNvbmZpZztcbiAgICAgICAgICAgIGxldCBlcnIgPSB5aWVsZCBQTTJIZWxwZXJfMS5kZWZhdWx0LmNvbm5lY3QoKTtcbiAgICAgICAgICAgIGlmIChlcnIpXG4gICAgICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKGVycik7XG4gICAgICAgICAgICB5aWVsZCBQTTJIZWxwZXJfMS5kZWZhdWx0LnN0b3BBbGwoY29uZmlnKTtcbiAgICAgICAgICAgIHlpZWxkIFBNMkhlbHBlcl8xLmRlZmF1bHQuZGlzY29ubmVjdCgpO1xuICAgICAgICB9KTtcbiAgICB9XG4gICAgcmVzdGFydChjb25maWcgPSB1bmRlZmluZWQpIHtcbiAgICAgICAgcmV0dXJuIF9fYXdhaXRlcih0aGlzLCB2b2lkIDAsIFByb21pc2UsIGZ1bmN0aW9uKiAoKSB7XG4gICAgICAgICAgICBpZiAoIWNvbmZpZylcbiAgICAgICAgICAgICAgICBjb25maWcgPSB0aGlzLmNvbmZpZztcbiAgICAgICAgICAgIGxldCBlcnIgPSB5aWVsZCBQTTJIZWxwZXJfMS5kZWZhdWx0LmNvbm5lY3QoKTtcbiAgICAgICAgICAgIGlmIChlcnIpXG4gICAgICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKGVycik7XG4gICAgICAgICAgICB5aWVsZCBQTTJIZWxwZXJfMS5kZWZhdWx0LnN0b3BBbGwoY29uZmlnKTtcbiAgICAgICAgICAgIHlpZWxkIFBNMkhlbHBlcl8xLmRlZmF1bHQuc3RhcnRBbGwoY29uZmlnKTtcbiAgICAgICAgICAgIHlpZWxkIFBNMkhlbHBlcl8xLmRlZmF1bHQuZGlzY29ubmVjdCgpO1xuICAgICAgICB9KTtcbiAgICB9XG59XG5leHBvcnRzLmRlZmF1bHQgPSBQTTJDTEk7XG4iLCJpbXBvcnQgJy4vSUNvbW1hbmRMaW5lJztcbmltcG9ydCB7ZXhlYzIgYXMgc2hlbGxfZXhlY30gZnJvbSAnLi4vbGliL2V4ZWMnO1xuaW1wb3J0IFBNMkhlbHBlciBmcm9tICcuLi9saWIvUE0ySGVscGVyJztcblxuY2xhc3MgUE0yQ0xJIGltcGxlbWVudHMgSUNvbW1hbmRMaW5lIHtcblxuICBwdWJsaWMgY29tbWFuZHMgPSB7XG4gICAgc3RhcnQ6IHtcbiAgICAgIGRlc2NyaXB0aW9uOiAnc3RhcnQgYWxsIHByb2Nlc3NlcydcbiAgICB9LFxuICAgIHN0b3A6IHtcbiAgICAgIGRlc2NyaXB0aW9uOiAnc3RvcCBhbGwgcHJvY2Vzc2VzJ1xuICAgIH0sXG4gICAgcmVzdGFydDoge1xuICAgICAgZGVzY3JpcHRpb246ICdyZXN0YXJ0IGFsbCBwcm9jZXNzZXMnXG4gICAgfSxcbiAgICBsaXN0OiB7XG4gICAgICBkZXNjcmlwdGlvbjogJ2xpc3QgYWxsIHByb2Nlc3NlcydcbiAgICB9LFxuICAgIGxvZzoge1xuICAgICAgZGVzY3JpcHRpb246ICdzdGFydCBzdHJlYW1pbmcgbG9ncyBmcm9tIHBtMiBwcm9jZXNzZXMnXG4gICAgfVxuICB9O1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgY29uZmlnOkFycmF5PGFueT4pIHtcblxuICB9XG5cbiAgcHVibGljIGFzeW5jIGV4ZWMoYXJndjphbnkpOlByb21pc2U8YW55PiB7XG4gICAgc3dpdGNoIChhcmd2Ll9bMF0pIHtcbiAgICAgIGNhc2UgJ3N0YXJ0JzpcbiAgICAgICAgYXdhaXQgdGhpcy5zdGFydCh1bmRlZmluZWQsIGFyZ3YubG9nKTtcbiAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICBjYXNlICdzdG9wJzpcbiAgICAgICAgYXdhaXQgdGhpcy5zdG9wKCk7XG4gICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgY2FzZSAncmVzdGFydCc6XG4gICAgICAgIGF3YWl0IHRoaXMucmVzdGFydCgpO1xuICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgIGNhc2UgJ2xpc3QnOlxuICAgICAgICBhd2FpdCB0aGlzLmxpc3QoKTtcbiAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICBjYXNlICdsb2cnOlxuICAgICAgICBhd2FpdCB0aGlzLmxvZygpO1xuICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgIGRlZmF1bHQ6XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogU3RhcnQgc3RyZWFtaW5nIHBtMiBsb2dzXG4gICAqXG4gICAqIEByZXR1cm4ge1Byb21pc2U8YW55Pn0gcmVzb2x2ZWQgd2hlbiBzY3JpcHQgaXMgZG9uZSBleGVjdXRpbmdcbiAgICovXG4gIHB1YmxpYyBhc3luYyBsb2coKTpQcm9taXNlPGFueT4ge1xuICAgIGF3YWl0IHNoZWxsX2V4ZWMoJ3BtMicsIFsnbG9ncyddKTtcbiAgfVxuXG4gIHB1YmxpYyBhc3luYyBsaXN0KCk6UHJvbWlzZTxhbnk+IHtcbiAgICBhd2FpdCBzaGVsbF9leGVjKCdwbTInLCBbJ2xpc3QnXSk7XG4gIH1cblxuICAvKipcbiAgICogU3RhcnQgcHJvY2Vzc2VzIGZvciBkZXZlbG9wbWVudFxuICAgKlxuICAgKiBAcmV0dXJuIHtQcm9taXNlPGFueT59IHJlc29sdmVkIHdoZW4gc2NyaXB0IGlzIGRvbmUgZXhlY3V0aW5nXG4gICAqL1xuICBwdWJsaWMgYXN5bmMgc3RhcnQoY29uZmlnOkFycmF5PGFueT4gPSB1bmRlZmluZWQsIGxvZzpib29sZWFuID0gZmFsc2UpOlByb21pc2U8YW55PiB7XG4gICAgaWYgKCFjb25maWcpIGNvbmZpZyA9IHRoaXMuY29uZmlnO1xuICAgIC8vIGNvbm5lY3QgdG8gcG0yXG4gICAgbGV0IGVyciA9IGF3YWl0IFBNMkhlbHBlci5jb25uZWN0KCk7XG4gICAgaWYgKGVycikgdGhyb3cgbmV3IEVycm9yKGVycik7XG4gICAgLy8gc3RhcnQgYWxsIHByb2Nlc3Nlc1xuICAgIGF3YWl0IFBNMkhlbHBlci5zdGFydEFsbChjb25maWcpO1xuXG4gICAgLy9hd2FpdCBQTTJIZWxwZXIubGlzdCgpO1xuICAgIC8vYXdhaXQgc2hlbGxfZXhlYygncG0yJywgWydsaXN0J10pO1xuXG4gICAgLy8gZGlzY29ubmVjdCBmcm9tIHBtMlxuICAgIGF3YWl0IFBNMkhlbHBlci5kaXNjb25uZWN0KCk7XG4gIH1cblxuICAvKipcbiAgICogU3RvcCBwcm9jZXNzZXMgZm9yIGRldmVsb3BtZW50XG4gICAqXG4gICAqIEByZXR1cm4ge1Byb21pc2U8YW55Pn0gcmVzb2x2ZWQgd2hlbiBzY3JpcHQgaXMgZG9uZSBleGVjdXRpbmdcbiAgICovXG4gIHB1YmxpYyBhc3luYyBzdG9wKGNvbmZpZzpBcnJheTxhbnk+ID0gdW5kZWZpbmVkKTpQcm9taXNlPGFueT4ge1xuICAgIGlmICghY29uZmlnKSBjb25maWcgPSB0aGlzLmNvbmZpZztcbiAgICAvLyBjb25uZWN0IHRvIHBtMlxuICAgIGxldCBlcnIgPSBhd2FpdCBQTTJIZWxwZXIuY29ubmVjdCgpO1xuICAgIGlmIChlcnIpIHRocm93IG5ldyBFcnJvcihlcnIpO1xuICAgIC8vIHN0YXJ0IGFsbCBwcm9jZXNzZXNcbiAgICBhd2FpdCBQTTJIZWxwZXIuc3RvcEFsbChjb25maWcpO1xuXG4gICAgLy9hd2FpdCBQTTJIZWxwZXIubGlzdCgpO1xuICAgIC8vYXdhaXQgc2hlbGxfZXhlYygncG0yJywgWydsaXN0J10pO1xuXG4gICAgLy8gZGlzY29ubmVjdCBmcm9tIHBtMlxuICAgIGF3YWl0IFBNMkhlbHBlci5kaXNjb25uZWN0KCk7XG4gIH1cblxuICAvKipcbiAgICogUmVzdGFydCBwcm9jZXNzZXMgZm9yIGRldmVsb3BtZW50XG4gICAqXG4gICAqIEByZXR1cm4ge1Byb21pc2U8YW55Pn0gcmVzb2x2ZWQgd2hlbiBzY3JpcHQgaXMgZG9uZSBleGVjdXRpbmdcbiAgICovXG4gIHB1YmxpYyBhc3luYyByZXN0YXJ0KGNvbmZpZzpBcnJheTxzdHJpbmc+ID0gdW5kZWZpbmVkKTpQcm9taXNlPGFueT4ge1xuICAgIGlmICghY29uZmlnKSBjb25maWcgPSB0aGlzLmNvbmZpZztcbiAgICAvLyBjb25uZWN0IHRvIHBtMlxuICAgIGxldCBlcnIgPSBhd2FpdCBQTTJIZWxwZXIuY29ubmVjdCgpO1xuICAgIGlmIChlcnIpIHRocm93IG5ldyBFcnJvcihlcnIpO1xuICAgIC8vIHN0b3AgYWxsIHByb2Nlc3Nlc1xuICAgIGF3YWl0IFBNMkhlbHBlci5zdG9wQWxsKGNvbmZpZyk7XG4gICAgLy8gc3RhcnQgYWxsIHByb2Nlc3Nlc1xuICAgIGF3YWl0IFBNMkhlbHBlci5zdGFydEFsbChjb25maWcpO1xuICAgIC8vIGRpc2Nvbm5lY3QgZnJvbSBwbTJcbiAgICBhd2FpdCBQTTJIZWxwZXIuZGlzY29ubmVjdCgpO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IFBNMkNMSTtcbiJdLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==
