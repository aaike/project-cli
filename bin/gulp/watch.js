import gulp from 'gulp';
import {paths, patterns} from '../config';

gulp.task('watch', 'watch for file changes and rebuild as needed', () => {
  gulp.watch(patterns.source, ['typescript']);
});
