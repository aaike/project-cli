"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var __awaiter = undefined && undefined.__awaiter || function (thisArg, _arguments, Promise, generator) {
    return new Promise(function (resolve, reject) {
        generator = generator.call(thisArg, _arguments);
        function cast(value) {
            return value instanceof Promise && value.constructor === Promise ? value : new Promise(function (resolve) {
                resolve(value);
            });
        }
        function onfulfill(value) {
            try {
                step("next", value);
            } catch (e) {
                reject(e);
            }
        }
        function onreject(value) {
            try {
                step("throw", value);
            } catch (e) {
                reject(e);
            }
        }
        function step(verb, value) {
            var result = generator[verb](value);
            result.done ? resolve(result.value) : cast(result.value).then(onfulfill, onreject);
        }
        step("next", void 0);
    });
};
var fsp = require('fs-promise');
var Logger_1 = require('../lib/Logger');
var exec_1 = require('../lib/exec');

var DependenciesCLI = function () {
    function DependenciesCLI(config) {
        _classCallCheck(this, DependenciesCLI);

        this.config = config;
        this.commands = {
            install: {
                description: 'install all dependencies'
            },
            update: {
                description: 'update all dependencies'
            }
        };
    }

    _createClass(DependenciesCLI, [{
        key: "exec",
        value: function exec(argv) {
            return __awaiter(this, void 0, Promise, regeneratorRuntime.mark(function _callee() {
                var cmd;
                return regeneratorRuntime.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                cmd = argv._[0];

                                if (this.commands[cmd]) {
                                    _context.next = 3;
                                    break;
                                }

                                return _context.abrupt("return", false);

                            case 3:
                                _context.t0 = cmd;
                                _context.next = _context.t0 === 'install' ? 6 : _context.t0 === 'update' ? 9 : 12;
                                break;

                            case 6:
                                _context.next = 8;
                                return this.installAllDependencies(this.config.installPaths);

                            case 8:
                                return _context.abrupt("return", true);

                            case 9:
                                _context.next = 11;
                                return this.installAllDependencies(this.config.installPaths, true);

                            case 11:
                                return _context.abrupt("return", true);

                            case 12:
                                return _context.abrupt("break", 13);

                            case 13:
                                return _context.abrupt("return", false);

                            case 14:
                            case "end":
                                return _context.stop();
                        }
                    }
                }, _callee, this);
            }));
        }
    }, {
        key: "installAllDependencies",
        value: function installAllDependencies(paths) {
            var update = arguments.length <= 1 || arguments[1] === undefined ? false : arguments[1];

            return __awaiter(this, void 0, Promise, regeneratorRuntime.mark(function _callee2() {
                var _iteratorNormalCompletion, _didIteratorError, _iteratorError, _iterator, _step, path;

                return regeneratorRuntime.wrap(function _callee2$(_context2) {
                    while (1) {
                        switch (_context2.prev = _context2.next) {
                            case 0:
                                _iteratorNormalCompletion = true;
                                _didIteratorError = false;
                                _iteratorError = undefined;
                                _context2.prev = 3;
                                _iterator = paths[Symbol.iterator]();

                            case 5:
                                if (_iteratorNormalCompletion = (_step = _iterator.next()).done) {
                                    _context2.next = 12;
                                    break;
                                }

                                path = _step.value;
                                _context2.next = 9;
                                return this.installDependencies(path, update);

                            case 9:
                                _iteratorNormalCompletion = true;
                                _context2.next = 5;
                                break;

                            case 12:
                                _context2.next = 18;
                                break;

                            case 14:
                                _context2.prev = 14;
                                _context2.t0 = _context2["catch"](3);
                                _didIteratorError = true;
                                _iteratorError = _context2.t0;

                            case 18:
                                _context2.prev = 18;
                                _context2.prev = 19;

                                if (!_iteratorNormalCompletion && _iterator.return) {
                                    _iterator.return();
                                }

                            case 21:
                                _context2.prev = 21;

                                if (!_didIteratorError) {
                                    _context2.next = 24;
                                    break;
                                }

                                throw _iteratorError;

                            case 24:
                                return _context2.finish(21);

                            case 25:
                                return _context2.finish(18);

                            case 26:
                                if (update) {
                                    Logger_1.logger.info('DEPENDENCIES UPDATED');
                                } else {
                                    Logger_1.logger.info('DEPENDENCIES INSTALLED');
                                }

                            case 27:
                            case "end":
                                return _context2.stop();
                        }
                    }
                }, _callee2, this, [[3, 14, 18, 26], [19,, 21, 25]]);
            }));
        }
    }, {
        key: "installDependencies",
        value: function installDependencies() {
            var cwd = arguments.length <= 0 || arguments[0] === undefined ? '.' : arguments[0];
            var update = arguments.length <= 1 || arguments[1] === undefined ? false : arguments[1];

            return __awaiter(this, void 0, Promise, regeneratorRuntime.mark(function _callee3() {
                var pkg, tsdConfig;
                return regeneratorRuntime.wrap(function _callee3$(_context3) {
                    while (1) {
                        switch (_context3.prev = _context3.next) {
                            case 0:
                                _context3.next = 2;
                                return fsp.exists(cwd + "/package.json");

                            case 2:
                                if (!_context3.sent) {
                                    _context3.next = 15;
                                    break;
                                }

                                _context3.next = 5;
                                return fsp.exists(cwd + "/node_modules");

                            case 5:
                                _context3.t0 = !_context3.sent;

                                if (_context3.t0) {
                                    _context3.next = 8;
                                    break;
                                }

                                _context3.t0 = update;

                            case 8:
                                if (!_context3.t0) {
                                    _context3.next = 14;
                                    break;
                                }

                                Logger_1.logger.info("NPM Install: " + cwd);
                                _context3.next = 12;
                                return exec_1.default('npm install', { cwd: cwd });

                            case 12:
                                _context3.next = 15;
                                break;

                            case 14:
                                console.log('npm modules already installed');

                            case 15:
                                _context3.next = 17;
                                return fsp.exists(cwd + "/bower.json");

                            case 17:
                                if (!_context3.sent) {
                                    _context3.next = 30;
                                    break;
                                }

                                _context3.t1 = update;

                                if (_context3.t1) {
                                    _context3.next = 23;
                                    break;
                                }

                                _context3.next = 22;
                                return fsp.exists(cwd + "/bower_components");

                            case 22:
                                _context3.t1 = !_context3.sent;

                            case 23:
                                if (!_context3.t1) {
                                    _context3.next = 29;
                                    break;
                                }

                                Logger_1.logger.info("Bower Install: " + cwd);
                                _context3.next = 27;
                                return exec_1.default('bower install', { cwd: cwd });

                            case 27:
                                _context3.next = 30;
                                break;

                            case 29:
                                console.log('bower components already installed');

                            case 30:
                                _context3.next = 32;
                                return fsp.exists(cwd + "/package.json");

                            case 32:
                                if (!_context3.sent) {
                                    _context3.next = 54;
                                    break;
                                }

                                _context3.t2 = JSON;
                                _context3.next = 36;
                                return fsp.readFile(cwd + "/package.json", 'utf-8');

                            case 36:
                                _context3.t3 = _context3.sent;
                                pkg = _context3.t2.parse.call(_context3.t2, _context3.t3);

                                if (!pkg.jspm) {
                                    _context3.next = 54;
                                    break;
                                }

                                _context3.t4 = update;

                                if (_context3.t4) {
                                    _context3.next = 44;
                                    break;
                                }

                                _context3.next = 43;
                                return fsp.exists(cwd + "/jspm_packages");

                            case 43:
                                _context3.t4 = !_context3.sent;

                            case 44:
                                if (!_context3.t4) {
                                    _context3.next = 53;
                                    break;
                                }

                                if (!pkg.jspm) {
                                    _context3.next = 51;
                                    break;
                                }

                                Logger_1.logger.info("JSPM Install: " + cwd);
                                _context3.next = 49;
                                return exec_1.default('jspm install -y', { cwd: cwd });

                            case 49:
                                _context3.next = 51;
                                return exec_1.default('jspm dl-loader -y', { cwd: cwd });

                            case 51:
                                _context3.next = 54;
                                break;

                            case 53:
                                console.log('jspm packages already installed');

                            case 54:
                                _context3.next = 56;
                                return fsp.exists(cwd + "/tsd.json");

                            case 56:
                                if (!_context3.sent) {
                                    _context3.next = 78;
                                    break;
                                }

                                _context3.t5 = JSON;
                                _context3.next = 60;
                                return fsp.readFile(cwd + "/tsd.json", 'utf-8');

                            case 60:
                                _context3.t6 = _context3.sent;
                                tsdConfig = _context3.t5.parse.call(_context3.t5, _context3.t6);

                                if (!(Object.keys(tsdConfig.installed).length > 0)) {
                                    _context3.next = 77;
                                    break;
                                }

                                _context3.t7 = update;

                                if (_context3.t7) {
                                    _context3.next = 68;
                                    break;
                                }

                                _context3.next = 67;
                                return fsp.exists(cwd + "/typings");

                            case 67:
                                _context3.t7 = !_context3.sent;

                            case 68:
                                if (!_context3.t7) {
                                    _context3.next = 74;
                                    break;
                                }

                                Logger_1.logger.info("TSD Install: " + cwd);
                                _context3.next = 72;
                                return exec_1.default('tsd install', { cwd: cwd });

                            case 72:
                                _context3.next = 75;
                                break;

                            case 74:
                                Logger_1.logger.log('tsd typings already installed');

                            case 75:
                                _context3.next = 78;
                                break;

                            case 77:
                                Logger_1.logger.log('no TSD typings found in tsd.json');

                            case 78:
                            case "end":
                                return _context3.stop();
                        }
                    }
                }, _callee3, this);
            }));
        }
    }]);

    return DependenciesCLI;
}();

exports.default = DependenciesCLI;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbW1hbmRzL2RlcGVuZGVuY2llcy5qcyIsImNvbW1hbmRzL2RlcGVuZGVuY2llcy50cyJdLCJuYW1lcyI6WyJEZXBlbmRlbmNpZXNDTEkiLCJEZXBlbmRlbmNpZXNDTEkuY29uc3RydWN0b3IiLCJEZXBlbmRlbmNpZXNDTEkuZXhlYyIsIkRlcGVuZGVuY2llc0NMSS5pbnN0YWxsQWxsRGVwZW5kZW5jaWVzIiwiRGVwZW5kZW5jaWVzQ0xJLmluc3RhbGxEZXBlbmRlbmNpZXMiXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLElBQUksWUFBWSxTQUFDLElBQVEsVUFBSyxTQUFMLElBQW1CLFVBQVUsT0FBVixFQUFtQixVQUFuQixFQUErQixPQUEvQixFQUF3QyxTQUF4QyxFQUFtRDtBQUMzRixXQUFPLElBQUksT0FBSixDQUFZLFVBQVUsT0FBVixFQUFtQixNQUFuQixFQUEyQjtBQUMxQyxvQkFBWSxVQUFVLElBQVYsQ0FBZSxPQUFmLEVBQXdCLFVBQXhCLENBQVosQ0FEMEM7QUFFMUMsaUJBQVMsSUFBVCxDQUFjLEtBQWQsRUFBcUI7QUFBRSxtQkFBTyxpQkFBaUIsT0FBakIsSUFBNEIsTUFBTSxXQUFOLEtBQXNCLE9BQXRCLEdBQWdDLEtBQTVELEdBQW9FLElBQUksT0FBSixDQUFZLFVBQVUsT0FBVixFQUFtQjtBQUFFLHdCQUFRLEtBQVIsRUFBRjthQUFuQixDQUFoRixDQUFUO1NBQXJCO0FBQ0EsaUJBQVMsU0FBVCxDQUFtQixLQUFuQixFQUEwQjtBQUFFLGdCQUFJO0FBQUUscUJBQUssTUFBTCxFQUFhLEtBQWIsRUFBRjthQUFKLENBQTZCLE9BQU8sQ0FBUCxFQUFVO0FBQUUsdUJBQU8sQ0FBUCxFQUFGO2FBQVY7U0FBekQ7QUFDQSxpQkFBUyxRQUFULENBQWtCLEtBQWxCLEVBQXlCO0FBQUUsZ0JBQUk7QUFBRSxxQkFBSyxPQUFMLEVBQWMsS0FBZCxFQUFGO2FBQUosQ0FBOEIsT0FBTyxDQUFQLEVBQVU7QUFBRSx1QkFBTyxDQUFQLEVBQUY7YUFBVjtTQUF6RDtBQUNBLGlCQUFTLElBQVQsQ0FBYyxJQUFkLEVBQW9CLEtBQXBCLEVBQTJCO0FBQ3ZCLGdCQUFJLFNBQVMsVUFBVSxJQUFWLEVBQWdCLEtBQWhCLENBQVQsQ0FEbUI7QUFFdkIsbUJBQU8sSUFBUCxHQUFjLFFBQVEsT0FBTyxLQUFQLENBQXRCLEdBQXNDLEtBQUssT0FBTyxLQUFQLENBQUwsQ0FBbUIsSUFBbkIsQ0FBd0IsU0FBeEIsRUFBbUMsUUFBbkMsQ0FBdEMsQ0FGdUI7U0FBM0I7QUFJQSxhQUFLLE1BQUwsRUFBYSxLQUFLLENBQUwsQ0FBYixDQVQwQztLQUEzQixDQUFuQixDQUQyRjtDQUFuRDtBQ0E1QyxJQUFZLE1BQUcsUUFBTSxZQUFOLENBQUg7QUFDWixJQUFBLFdBQUEsUUFBcUIsZUFBckIsQ0FBQTtBQUNBLElBQUEsU0FBQSxRQUF1QixhQUF2QixDQUFBOztJQUVBO0FBV0VBLGFBWEYsZUFXRUEsQ0FBb0JBLE1BQXBCQSxFQUE4QkE7OEJBWGhDLGlCQVdnQ0E7O0FBQVZDLGFBQUFBLE1BQUFBLEdBQUFBLE1BQUFBLENBQVVEO0FBVHZCQyxhQUFBQSxRQUFBQSxHQUFXQTtBQUNoQkEscUJBQVNBO0FBQ1BBLDZCQUFhQSwwQkFBYkE7YUFERkE7QUFHQUEsb0JBQVFBO0FBQ05BLDZCQUFhQSx5QkFBYkE7YUFERkE7U0FKS0EsQ0FTdUJEO0tBQTlCQTs7aUJBWEY7OzZCQWVvQkEsTUFBUUE7QURVcEIsbUJBQU8sVUFBVSxJQUFWLEVBQWdCLEtBQUssQ0FBTCxFQUFRLE9BQXhCLDBCQUFpQztvQkNUeENFOzs7OztzQ0FBTUEsS0FBS0EsQ0FBTEEsQ0FBT0EsQ0FBUEE7O29DQUNMQSxLQUFLQSxRQUFMQSxDQUFjQSxHQUFkQTs7Ozs7aUVBQTJCQTs7OzhDQUV4QkE7Z0VBQ0RBLGdDQUdBQTs7Ozs7dUNBRkdBLEtBQUtBLHNCQUFMQSxDQUE0QkEsS0FBS0EsTUFBTEEsQ0FBWUEsWUFBWkE7OztpRUFDM0JBOzs7O3VDQUVEQSxLQUFLQSxzQkFBTEEsQ0FBNEJBLEtBQUtBLE1BQUxBLENBQVlBLFlBQVpBLEVBQTBCQSxJQUF0REE7OztpRUFDQ0E7Ozs7OztpRUFJSkE7Ozs7Ozs7O2FESnFDLENBQWpDLENBQVAsQ0NWb0JGOzs7OytDQXFCVUEsT0FBMkNBO2dCQUF0QkEsK0RBQWlCQSxxQkFBS0E7O0FET3pFLG1CQUFPLFVBQVUsSUFBVixFQUFnQixLQUFLLENBQUwsRUFBUSxPQUF4QiwwQkFBaUM7b0dDTG5DRzs7Ozs7Ozs7Ozs0Q0FBUUE7Ozs7Ozs7O0FBQVJBOzt1Q0FDQ0EsS0FBS0EsbUJBQUxBLENBQXlCQSxJQUF6QkEsRUFBK0JBLE1BQS9CQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBRVZBLG9DQUFJQSxNQUFKQSxFQUFZQTtBQUNWQSw2Q0FBQUEsTUFBQUEsQ0FBT0EsSUFBUEEsQ0FBWUEsc0JBQVpBLEVBRFVBO2lDQUFaQSxNQUVNQTtBQUNKQSw2Q0FBQUEsTUFBQUEsQ0FBT0EsSUFBUEEsQ0FBWUEsd0JBQVpBLEVBRElBO2lDQUZOQTs7Ozs7Ozs7YURFNEMsQ0FBakMsQ0FBUCxDQ1B5RUg7Ozs7OENBcUJOQTtnQkFBeENBLDREQUFhQSxtQkFBMkJBO2dCQUF0QkEsK0RBQWlCQSxxQkFBS0E7O0FERG5FLG1CQUFPLFVBQVUsSUFBVixFQUFnQixLQUFLLENBQUwsRUFBUSxPQUF4QiwwQkFBaUM7b0JDdUJ0Q0ksS0FlQUE7Ozs7Ozt1Q0FsQ0lBLElBQUlBLE1BQUpBLENBQWNBLHFCQUFkQTs7Ozs7Ozs7O3VDQUNJQSxJQUFJQSxNQUFKQSxDQUFjQSxxQkFBZEE7Ozs7Ozs7Ozs7K0NBQXNDQTs7Ozs7Ozs7QUFDaERBLHlDQUFBQSxNQUFBQSxDQUFPQSxJQUFQQSxtQkFBNEJBLEdBQTVCQTs7dUNBQ01BLE9BQUFBLE9BQUFBLENBQVdBLGFBQVhBLEVBQTBCQSxFQUFDQSxRQUFEQSxFQUExQkE7Ozs7Ozs7QUFFTkEsd0NBQVFBLEdBQVJBLENBQVlBLCtCQUFaQTs7Ozt1Q0FJTUEsSUFBSUEsTUFBSkEsQ0FBY0EsbUJBQWRBOzs7Ozs7OzsrQ0FDSkE7Ozs7Ozs7O3VDQUFrQkEsSUFBSUEsTUFBSkEsQ0FBY0EseUJBQWRBOzs7Ozs7Ozs7OztBQUNwQkEseUNBQUFBLE1BQUFBLENBQU9BLElBQVBBLHFCQUE4QkEsR0FBOUJBOzt1Q0FDTUEsT0FBQUEsT0FBQUEsQ0FBV0EsZUFBWEEsRUFBNEJBLEVBQUNBLFFBQURBLEVBQTVCQTs7Ozs7OztBQUVOQSx3Q0FBUUEsR0FBUkEsQ0FBWUEsb0NBQVpBOzs7O3VDQUlNQSxJQUFJQSxNQUFKQSxDQUFjQSxxQkFBZEE7Ozs7Ozs7OytDQUNFQTs7dUNBQWlCQSxJQUFJQSxRQUFKQSxDQUFnQkEscUJBQWhCQSxFQUFvQ0EsT0FBcENBOzs7O0FBQXZCQSxtREFBV0E7O3FDQUNYQSxJQUFJQSxJQUFKQTs7Ozs7K0NBQ0VBOzs7Ozs7Ozt1Q0FBa0JBLElBQUlBLE1BQUpBLENBQWNBLHNCQUFkQTs7Ozs7Ozs7Ozs7cUNBQ2hCQSxJQUFJQSxJQUFKQTs7Ozs7QUFDRkEseUNBQUFBLE1BQUFBLENBQU9BLElBQVBBLG9CQUE2QkEsR0FBN0JBOzt1Q0FDTUEsT0FBQUEsT0FBQUEsQ0FBV0EsaUJBQVhBLEVBQThCQSxFQUFDQSxRQUFEQSxFQUE5QkE7Ozs7dUNBQ0FBLE9BQUFBLE9BQUFBLENBQVdBLG1CQUFYQSxFQUFnQ0EsRUFBQ0EsUUFBREEsRUFBaENBOzs7Ozs7O0FBR1JBLHdDQUFRQSxHQUFSQSxDQUFZQSxpQ0FBWkE7Ozs7dUNBS0lBLElBQUlBLE1BQUpBLENBQWNBLGlCQUFkQTs7Ozs7Ozs7K0NBQ1FBOzt1Q0FBaUJBLElBQUlBLFFBQUpBLENBQWdCQSxpQkFBaEJBLEVBQWdDQSxPQUFoQ0E7Ozs7QUFBN0JBLHlEQUFpQkE7O3NDQUNqQkEsT0FBT0EsSUFBUEEsQ0FBWUEsVUFBVUEsU0FBVkEsQ0FBWkEsQ0FBaUNBLE1BQWpDQSxHQUEwQ0EsQ0FBMUNBOzs7OzsrQ0FDRUE7Ozs7Ozs7O3VDQUFrQkEsSUFBSUEsTUFBSkEsQ0FBY0EsZ0JBQWRBOzs7Ozs7Ozs7OztBQUNwQkEseUNBQUFBLE1BQUFBLENBQU9BLElBQVBBLG1CQUE0QkEsR0FBNUJBOzt1Q0FDTUEsT0FBQUEsT0FBQUEsQ0FBV0EsYUFBWEEsRUFBMEJBLEVBQUNBLFFBQURBLEVBQTFCQTs7Ozs7OztBQUVOQSx5Q0FBQUEsTUFBQUEsQ0FBT0EsR0FBUEEsQ0FBV0EsK0JBQVhBOzs7Ozs7O0FBR0ZBLHlDQUFBQSxNQUFBQSxDQUFPQSxHQUFQQSxDQUFXQSxrQ0FBWEE7Ozs7Ozs7O2FEL0N3QyxDQUFqQyxDQUFQLENDQ21FSjs7OztXQXpEM0U7OztBQThHQSxRQUFBLE9BQUEsR0FBZSxlQUFmIiwiZmlsZSI6ImNvbW1hbmRzL2RlcGVuZGVuY2llcy5qcyIsInNvdXJjZXNDb250ZW50IjpbInZhciBfX2F3YWl0ZXIgPSAodGhpcyAmJiB0aGlzLl9fYXdhaXRlcikgfHwgZnVuY3Rpb24gKHRoaXNBcmcsIF9hcmd1bWVudHMsIFByb21pc2UsIGdlbmVyYXRvcikge1xuICAgIHJldHVybiBuZXcgUHJvbWlzZShmdW5jdGlvbiAocmVzb2x2ZSwgcmVqZWN0KSB7XG4gICAgICAgIGdlbmVyYXRvciA9IGdlbmVyYXRvci5jYWxsKHRoaXNBcmcsIF9hcmd1bWVudHMpO1xuICAgICAgICBmdW5jdGlvbiBjYXN0KHZhbHVlKSB7IHJldHVybiB2YWx1ZSBpbnN0YW5jZW9mIFByb21pc2UgJiYgdmFsdWUuY29uc3RydWN0b3IgPT09IFByb21pc2UgPyB2YWx1ZSA6IG5ldyBQcm9taXNlKGZ1bmN0aW9uIChyZXNvbHZlKSB7IHJlc29sdmUodmFsdWUpOyB9KTsgfVxuICAgICAgICBmdW5jdGlvbiBvbmZ1bGZpbGwodmFsdWUpIHsgdHJ5IHsgc3RlcChcIm5leHRcIiwgdmFsdWUpOyB9IGNhdGNoIChlKSB7IHJlamVjdChlKTsgfSB9XG4gICAgICAgIGZ1bmN0aW9uIG9ucmVqZWN0KHZhbHVlKSB7IHRyeSB7IHN0ZXAoXCJ0aHJvd1wiLCB2YWx1ZSk7IH0gY2F0Y2ggKGUpIHsgcmVqZWN0KGUpOyB9IH1cbiAgICAgICAgZnVuY3Rpb24gc3RlcCh2ZXJiLCB2YWx1ZSkge1xuICAgICAgICAgICAgdmFyIHJlc3VsdCA9IGdlbmVyYXRvclt2ZXJiXSh2YWx1ZSk7XG4gICAgICAgICAgICByZXN1bHQuZG9uZSA/IHJlc29sdmUocmVzdWx0LnZhbHVlKSA6IGNhc3QocmVzdWx0LnZhbHVlKS50aGVuKG9uZnVsZmlsbCwgb25yZWplY3QpO1xuICAgICAgICB9XG4gICAgICAgIHN0ZXAoXCJuZXh0XCIsIHZvaWQgMCk7XG4gICAgfSk7XG59O1xudmFyIGZzcCA9IHJlcXVpcmUoJ2ZzLXByb21pc2UnKTtcbnZhciBMb2dnZXJfMSA9IHJlcXVpcmUoJy4uL2xpYi9Mb2dnZXInKTtcbnZhciBleGVjXzEgPSByZXF1aXJlKCcuLi9saWIvZXhlYycpO1xuY2xhc3MgRGVwZW5kZW5jaWVzQ0xJIHtcbiAgICBjb25zdHJ1Y3Rvcihjb25maWcpIHtcbiAgICAgICAgdGhpcy5jb25maWcgPSBjb25maWc7XG4gICAgICAgIHRoaXMuY29tbWFuZHMgPSB7XG4gICAgICAgICAgICBpbnN0YWxsOiB7XG4gICAgICAgICAgICAgICAgZGVzY3JpcHRpb246ICdpbnN0YWxsIGFsbCBkZXBlbmRlbmNpZXMnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgdXBkYXRlOiB7XG4gICAgICAgICAgICAgICAgZGVzY3JpcHRpb246ICd1cGRhdGUgYWxsIGRlcGVuZGVuY2llcydcbiAgICAgICAgICAgIH1cbiAgICAgICAgfTtcbiAgICB9XG4gICAgZXhlYyhhcmd2KSB7XG4gICAgICAgIHJldHVybiBfX2F3YWl0ZXIodGhpcywgdm9pZCAwLCBQcm9taXNlLCBmdW5jdGlvbiogKCkge1xuICAgICAgICAgICAgbGV0IGNtZCA9IGFyZ3YuX1swXTtcbiAgICAgICAgICAgIGlmICghdGhpcy5jb21tYW5kc1tjbWRdKVxuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgIHN3aXRjaCAoY21kKSB7XG4gICAgICAgICAgICAgICAgY2FzZSAnaW5zdGFsbCc6XG4gICAgICAgICAgICAgICAgICAgIHlpZWxkIHRoaXMuaW5zdGFsbEFsbERlcGVuZGVuY2llcyh0aGlzLmNvbmZpZy5pbnN0YWxsUGF0aHMpO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAgICAgICBjYXNlICd1cGRhdGUnOlxuICAgICAgICAgICAgICAgICAgICB5aWVsZCB0aGlzLmluc3RhbGxBbGxEZXBlbmRlbmNpZXModGhpcy5jb25maWcuaW5zdGFsbFBhdGhzLCB0cnVlKTtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgIH0pO1xuICAgIH1cbiAgICBpbnN0YWxsQWxsRGVwZW5kZW5jaWVzKHBhdGhzLCB1cGRhdGUgPSBmYWxzZSkge1xuICAgICAgICByZXR1cm4gX19hd2FpdGVyKHRoaXMsIHZvaWQgMCwgUHJvbWlzZSwgZnVuY3Rpb24qICgpIHtcbiAgICAgICAgICAgIGZvciAobGV0IHBhdGggb2YgcGF0aHMpIHtcbiAgICAgICAgICAgICAgICB5aWVsZCB0aGlzLmluc3RhbGxEZXBlbmRlbmNpZXMocGF0aCwgdXBkYXRlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmICh1cGRhdGUpIHtcbiAgICAgICAgICAgICAgICBMb2dnZXJfMS5sb2dnZXIuaW5mbygnREVQRU5ERU5DSUVTIFVQREFURUQnKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIExvZ2dlcl8xLmxvZ2dlci5pbmZvKCdERVBFTkRFTkNJRVMgSU5TVEFMTEVEJyk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH1cbiAgICBpbnN0YWxsRGVwZW5kZW5jaWVzKGN3ZCA9ICcuJywgdXBkYXRlID0gZmFsc2UpIHtcbiAgICAgICAgcmV0dXJuIF9fYXdhaXRlcih0aGlzLCB2b2lkIDAsIFByb21pc2UsIGZ1bmN0aW9uKiAoKSB7XG4gICAgICAgICAgICBpZiAoeWllbGQgZnNwLmV4aXN0cyhgJHtjd2R9L3BhY2thZ2UuanNvbmApKSB7XG4gICAgICAgICAgICAgICAgaWYgKCEoeWllbGQgZnNwLmV4aXN0cyhgJHtjd2R9L25vZGVfbW9kdWxlc2ApKSB8fCB1cGRhdGUpIHtcbiAgICAgICAgICAgICAgICAgICAgTG9nZ2VyXzEubG9nZ2VyLmluZm8oYE5QTSBJbnN0YWxsOiAke2N3ZH1gKTtcbiAgICAgICAgICAgICAgICAgICAgeWllbGQgZXhlY18xLmRlZmF1bHQoJ25wbSBpbnN0YWxsJywgeyBjd2QgfSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnbnBtIG1vZHVsZXMgYWxyZWFkeSBpbnN0YWxsZWQnKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAoeWllbGQgZnNwLmV4aXN0cyhgJHtjd2R9L2Jvd2VyLmpzb25gKSkge1xuICAgICAgICAgICAgICAgIGlmICh1cGRhdGUgfHwgISh5aWVsZCBmc3AuZXhpc3RzKGAke2N3ZH0vYm93ZXJfY29tcG9uZW50c2ApKSkge1xuICAgICAgICAgICAgICAgICAgICBMb2dnZXJfMS5sb2dnZXIuaW5mbyhgQm93ZXIgSW5zdGFsbDogJHtjd2R9YCk7XG4gICAgICAgICAgICAgICAgICAgIHlpZWxkIGV4ZWNfMS5kZWZhdWx0KCdib3dlciBpbnN0YWxsJywgeyBjd2QgfSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnYm93ZXIgY29tcG9uZW50cyBhbHJlYWR5IGluc3RhbGxlZCcpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmICh5aWVsZCBmc3AuZXhpc3RzKGAke2N3ZH0vcGFja2FnZS5qc29uYCkpIHtcbiAgICAgICAgICAgICAgICBsZXQgcGtnID0gSlNPTi5wYXJzZSh5aWVsZCBmc3AucmVhZEZpbGUoYCR7Y3dkfS9wYWNrYWdlLmpzb25gLCAndXRmLTgnKSk7XG4gICAgICAgICAgICAgICAgaWYgKHBrZy5qc3BtKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmICh1cGRhdGUgfHwgISh5aWVsZCBmc3AuZXhpc3RzKGAke2N3ZH0vanNwbV9wYWNrYWdlc2ApKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHBrZy5qc3BtKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgTG9nZ2VyXzEubG9nZ2VyLmluZm8oYEpTUE0gSW5zdGFsbDogJHtjd2R9YCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgeWllbGQgZXhlY18xLmRlZmF1bHQoJ2pzcG0gaW5zdGFsbCAteScsIHsgY3dkIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHlpZWxkIGV4ZWNfMS5kZWZhdWx0KCdqc3BtIGRsLWxvYWRlciAteScsIHsgY3dkIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ2pzcG0gcGFja2FnZXMgYWxyZWFkeSBpbnN0YWxsZWQnKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmICh5aWVsZCBmc3AuZXhpc3RzKGAke2N3ZH0vdHNkLmpzb25gKSkge1xuICAgICAgICAgICAgICAgIGxldCB0c2RDb25maWcgPSBKU09OLnBhcnNlKHlpZWxkIGZzcC5yZWFkRmlsZShgJHtjd2R9L3RzZC5qc29uYCwgJ3V0Zi04JykpO1xuICAgICAgICAgICAgICAgIGlmIChPYmplY3Qua2V5cyh0c2RDb25maWcuaW5zdGFsbGVkKS5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmICh1cGRhdGUgfHwgISh5aWVsZCBmc3AuZXhpc3RzKGAke2N3ZH0vdHlwaW5nc2ApKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgTG9nZ2VyXzEubG9nZ2VyLmluZm8oYFRTRCBJbnN0YWxsOiAke2N3ZH1gKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHlpZWxkIGV4ZWNfMS5kZWZhdWx0KCd0c2QgaW5zdGFsbCcsIHsgY3dkIH0pO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgTG9nZ2VyXzEubG9nZ2VyLmxvZygndHNkIHR5cGluZ3MgYWxyZWFkeSBpbnN0YWxsZWQnKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgTG9nZ2VyXzEubG9nZ2VyLmxvZygnbm8gVFNEIHR5cGluZ3MgZm91bmQgaW4gdHNkLmpzb24nKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH1cbn1cbmV4cG9ydHMuZGVmYXVsdCA9IERlcGVuZGVuY2llc0NMSTtcbiIsImltcG9ydCAqIGFzIGZzcCBmcm9tICdmcy1wcm9taXNlJztcbmltcG9ydCB7bG9nZ2VyfSBmcm9tICcuLi9saWIvTG9nZ2VyJztcbmltcG9ydCBzaGVsbF9leGVjIGZyb20gJy4uL2xpYi9leGVjJztcblxuY2xhc3MgRGVwZW5kZW5jaWVzQ0xJIGltcGxlbWVudHMgSUNvbW1hbmRMaW5lIHtcblxuICBwdWJsaWMgY29tbWFuZHMgPSB7XG4gICAgaW5zdGFsbDoge1xuICAgICAgZGVzY3JpcHRpb246ICdpbnN0YWxsIGFsbCBkZXBlbmRlbmNpZXMnXG4gICAgfSxcbiAgICB1cGRhdGU6IHtcbiAgICAgIGRlc2NyaXB0aW9uOiAndXBkYXRlIGFsbCBkZXBlbmRlbmNpZXMnXG4gICAgfVxuICB9O1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgY29uZmlnOmFueSkge1xuXG4gIH1cblxuICBwdWJsaWMgYXN5bmMgZXhlYyhhcmd2OmFueSk6UHJvbWlzZTxhbnk+IHtcbiAgICBsZXQgY21kID0gYXJndi5fWzBdO1xuICAgIGlmICghdGhpcy5jb21tYW5kc1tjbWRdKSByZXR1cm4gZmFsc2U7XG5cbiAgICBzd2l0Y2ggKGNtZCkge1xuICAgICAgY2FzZSAnaW5zdGFsbCc6XG4gICAgICAgIGF3YWl0IHRoaXMuaW5zdGFsbEFsbERlcGVuZGVuY2llcyh0aGlzLmNvbmZpZy5pbnN0YWxsUGF0aHMpO1xuICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgIGNhc2UgJ3VwZGF0ZSc6XG4gICAgICAgIGF3YWl0IHRoaXMuaW5zdGFsbEFsbERlcGVuZGVuY2llcyh0aGlzLmNvbmZpZy5pbnN0YWxsUGF0aHMsIHRydWUpO1xuICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgIGRlZmF1bHQ6XG4gICAgICAgIGJyZWFrO1xuICAgIH1cbiAgICByZXR1cm4gZmFsc2U7XG4gIH1cblxuICAvKipcbiAgICogSW5zdGFsbCBhbGwgZGVwZW5kZW5jaWVzIGZvciB0aGlzIHByb2plY3QgYW5kIG1ha2Ugc3VyZVxuICAgKiBhbiBpbml0aWFsIGJ1aWxkIGlzIGRvbmUgdG8gbWFrZSBhc3NldHMgYXZhaWxhYmxlLlxuICAgKi9cbiAgcHVibGljIGFzeW5jIGluc3RhbGxBbGxEZXBlbmRlbmNpZXMocGF0aHM6QXJyYXk8c3RyaW5nPiwgdXBkYXRlOmJvb2xlYW4gPSBmYWxzZSk6UHJvbWlzZTxhbnk+IHtcbiAgICAvLyBpbnN0YWxsIGRlcGVuZGVuY2llcyBpbiBhbGwgZGlyc1xuICAgIGZvciAobGV0IHBhdGggb2YgcGF0aHMpIHtcbiAgICAgICAgYXdhaXQgdGhpcy5pbnN0YWxsRGVwZW5kZW5jaWVzKHBhdGgsIHVwZGF0ZSk7XG4gICAgfVxuICAgIGlmICh1cGRhdGUpIHtcbiAgICAgIGxvZ2dlci5pbmZvKCdERVBFTkRFTkNJRVMgVVBEQVRFRCcpO1xuICAgIH1lbHNlIHtcbiAgICAgIGxvZ2dlci5pbmZvKCdERVBFTkRFTkNJRVMgSU5TVEFMTEVEJyk7XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIEluc3RhbGwgYWxsIGRlcGVuZGVuY2llcyBpbiBhIGRpcmVjdG9yeS5cbiAgICpcbiAgICogVXNlIHRoZSAtLXVwZGF0ZSBmbGFnIHRvIHVwZGF0ZSB0aGUgZGVwZW5kZW5jaWVzXG4gICAqXG4gICAqIEBwYXJhbSAge3N0cmluZ30gICAgICAgY3dkICAgICBEaXJlY3RvcnkgdG8gcGVyZm9ybSBpbnN0YWxsYXRpb25zIGluXG4gICAqIEBwYXJhbSAge2Jvb2xlYW59ICAgICAgdXBkYXRlICBpbnN0YWxsIGRlcGVuZGVuY2llcyBldmVuIGlmIHRoZSBkaXJlY3RvcmllcyBhbHJlYWR5IGV4aXN0XG4gICAqIEByZXR1cm4ge1Byb21pc2U8YW55Pn0gICAgICAgICBSZXNvbHZlcyB3aGVuIGFsbCBkZXBlbmRlbmNpZXMgaGF2ZSBiZWVuIGluc3RhbGxlZFxuICAgKi9cbiAgcHVibGljIGFzeW5jIGluc3RhbGxEZXBlbmRlbmNpZXMoY3dkOnN0cmluZyA9ICcuJywgdXBkYXRlOmJvb2xlYW4gPSBmYWxzZSk6UHJvbWlzZTxhbnk+IHtcblxuICAgIC8vIGNoZWNrIGZvciBucG0sIGluc3RhbGwgaWYgdGhlcmUgaXMgYSBgcGFja2FnZS5qc29uYCBidXQgbm8gYG5vZGVfbW9kdWxlc2AgZGlyZWN0b3J5IHlldFxuICAgIGlmIChhd2FpdCBmc3AuZXhpc3RzKGAke2N3ZH0vcGFja2FnZS5qc29uYCkgKSB7XG4gICAgICBpZiAoIShhd2FpdCBmc3AuZXhpc3RzKGAke2N3ZH0vbm9kZV9tb2R1bGVzYCkpIHx8IHVwZGF0ZSkge1xuICAgICAgICBsb2dnZXIuaW5mbyhgTlBNIEluc3RhbGw6ICR7Y3dkfWApO1xuICAgICAgICBhd2FpdCBzaGVsbF9leGVjKCducG0gaW5zdGFsbCcsIHtjd2R9KTtcbiAgICAgIH1lbHNlIHtcbiAgICAgICAgY29uc29sZS5sb2coJ25wbSBtb2R1bGVzIGFscmVhZHkgaW5zdGFsbGVkJyk7XG4gICAgICB9XG4gICAgfVxuICAgIC8vIGNoZWNrIGZvciBib3dlciwgaW5zdGFsbCBpZiB0aGVyZSBpcyBhIGBib3dlci5qc29uYCBidXQgbm8gYGJvd2VyX2NvbXBvbmVudHNgIGRpcmVjdG9yeSB5ZXRcbiAgICBpZiAoYXdhaXQgZnNwLmV4aXN0cyhgJHtjd2R9L2Jvd2VyLmpzb25gKSkge1xuICAgICAgaWYgKHVwZGF0ZSB8fCAhKGF3YWl0IGZzcC5leGlzdHMoYCR7Y3dkfS9ib3dlcl9jb21wb25lbnRzYCkpKSB7XG4gICAgICAgIGxvZ2dlci5pbmZvKGBCb3dlciBJbnN0YWxsOiAke2N3ZH1gKTtcbiAgICAgICAgYXdhaXQgc2hlbGxfZXhlYygnYm93ZXIgaW5zdGFsbCcsIHtjd2R9KTtcbiAgICAgIH1lbHNlIHtcbiAgICAgICAgY29uc29sZS5sb2coJ2Jvd2VyIGNvbXBvbmVudHMgYWxyZWFkeSBpbnN0YWxsZWQnKTtcbiAgICAgIH1cbiAgICB9XG4gICAgLy8gY2hlY2sgZm9yIGpzcG0sIGluc3RhbGwgaWYgdGhlcmUgaXMgYSBqc3BtIHNlY3Rpb24gaW4gdGhlIGBwYWNrYWdlLmpzb25gIGJ1dCBubyBganNwbV9wYWNrYWdlc2AgZGlyZWN0b3J5IHlldFxuICAgIGlmIChhd2FpdCBmc3AuZXhpc3RzKGAke2N3ZH0vcGFja2FnZS5qc29uYCkpIHtcbiAgICAgIGxldCBwa2cgPSBKU09OLnBhcnNlKGF3YWl0IGZzcC5yZWFkRmlsZShgJHtjd2R9L3BhY2thZ2UuanNvbmAsICd1dGYtOCcpKTtcbiAgICAgIGlmIChwa2cuanNwbSkge1xuICAgICAgICBpZiAodXBkYXRlIHx8ICEoYXdhaXQgZnNwLmV4aXN0cyhgJHtjd2R9L2pzcG1fcGFja2FnZXNgKSkpIHtcbiAgICAgICAgICBpZiAocGtnLmpzcG0pIHtcbiAgICAgICAgICAgIGxvZ2dlci5pbmZvKGBKU1BNIEluc3RhbGw6ICR7Y3dkfWApO1xuICAgICAgICAgICAgYXdhaXQgc2hlbGxfZXhlYygnanNwbSBpbnN0YWxsIC15Jywge2N3ZH0pO1xuICAgICAgICAgICAgYXdhaXQgc2hlbGxfZXhlYygnanNwbSBkbC1sb2FkZXIgLXknLCB7Y3dkfSk7XG4gICAgICAgICAgfVxuICAgICAgICB9ZWxzZSB7XG4gICAgICAgICAgY29uc29sZS5sb2coJ2pzcG0gcGFja2FnZXMgYWxyZWFkeSBpbnN0YWxsZWQnKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgICAvLyBjaGVjayBmb3IgdHNkLCBpbnN0YWxsIGlmIHRoZXJlIGlzIGEgYHRzZC5qc29uYCBidXQgbm8gYHR5cGluZ3NgIGRpcmVjdG9yeSB5ZXRcbiAgICBpZiAoYXdhaXQgZnNwLmV4aXN0cyhgJHtjd2R9L3RzZC5qc29uYCkpIHtcbiAgICAgIGxldCB0c2RDb25maWcgPSBKU09OLnBhcnNlKGF3YWl0IGZzcC5yZWFkRmlsZShgJHtjd2R9L3RzZC5qc29uYCwgJ3V0Zi04JykpO1xuICAgICAgaWYgKE9iamVjdC5rZXlzKHRzZENvbmZpZy5pbnN0YWxsZWQpLmxlbmd0aCA+IDApIHtcbiAgICAgICAgaWYgKHVwZGF0ZSB8fCAhKGF3YWl0IGZzcC5leGlzdHMoYCR7Y3dkfS90eXBpbmdzYCkpKSB7XG4gICAgICAgICAgbG9nZ2VyLmluZm8oYFRTRCBJbnN0YWxsOiAke2N3ZH1gKTtcbiAgICAgICAgICBhd2FpdCBzaGVsbF9leGVjKCd0c2QgaW5zdGFsbCcsIHtjd2R9KTtcbiAgICAgICAgfWVsc2Uge1xuICAgICAgICAgIGxvZ2dlci5sb2coJ3RzZCB0eXBpbmdzIGFscmVhZHkgaW5zdGFsbGVkJyk7XG4gICAgICAgIH1cbiAgICAgIH1lbHNlIHtcbiAgICAgICAgbG9nZ2VyLmxvZygnbm8gVFNEIHR5cGluZ3MgZm91bmQgaW4gdHNkLmpzb24nKTtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxufVxuXG5leHBvcnQgZGVmYXVsdCBEZXBlbmRlbmNpZXNDTEk7XG4iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
