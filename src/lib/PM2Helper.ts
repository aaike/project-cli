import * as _pm2 from 'pm2';
import * as path from 'path';
import * as chalk from 'chalk';
import * as promisify from 'promisify-node';
const pm2 = promisify(_pm2);

/**
 * PM2 helper class with static methods to control processes.
 *
 * @see http://pm2.keymetrics.io/docs/usage/pm2-api/
 */
class PM2Helper {

  // ---- pm2 function aliases
  public static connect:any = pm2.connect;
  public static disconnect:any = pm2.disconnect;

  /**
   * Configure options for this helper
   *
   * @param  {any}          options hash of options
   */
  /*public static config(options:any):void {
    this.options = options;
  }*/

  /**
   * Get detailed information on a process
   *
   * @param  {string}       name name of the process (as assigned by pm2)
   * @return {Promise<any>}      [description]
   */
  public static async getProcessInfo(name:string):Promise<any> {
    let proccesses = await pm2.describe(name);

    // skip if process is not running
    if (!proccesses.length) return Promise.resolve();
    let process = proccesses[0];

    return {
      name: process.name,
      status: process.status,
      pid: process.pid,
      pm_id: process.pm_id,
      memory: process.monit.memory,
      cpu: process.monit.cpu
    };
  }

  /**
   * Check if a process is running
   *
   * @param  {string}       name name of the process (as assigned by pm2)
   * @return {Promise<any>}      [description]
   */
  public static async isRunning(name:string):Promise<any> {
    let proccesses = await pm2.describe(name);
    if (!proccesses.length) return false;
    let process = proccesses[0];
    return (process.pm2_env.status === 'online') ? true : false;
  }

  /**
   * Start a process
   *
   * @param  {string}       path    path to a script that starts a process
   * @return {Promise<any>}         resolved when the process is started or has already been started.
   */
  public static async start(config:any):Promise<any> {
    let _path = config.script;
    let basename = config.name;
    if (!basename) {
      let index = _path.indexOf(' ');
      if (index > -1) _path = _path.substr(0, index);
      // get process name from filename
      basename = path.basename(_path, path.extname(_path));
    }

    // skip if process is already running
    if (await this.isRunning(basename)) return;
    // start the process
    await pm2.start(config);

    console.log(chalk.green(`started process: ${basename}`));
  }

  /**
   * Start multiple processes
   *
   * @param  {Array<string>} paths    array of paths to scripts
   * @return {Promise<any>}           resolved when all process are started
   */
  public static async startAll(config:Array<string>):Promise<any> {
    for (let p of config) await this.start(p);
  }

  /**
   * Stop a process
   *
   * @param  {string}       path    path to a script that starts a process
   * @return {Promise<any>}         resolved when the process is started or has already been started.
   */
  public static async stop(config:any):Promise<any> {
    let _path = config.script;

    let basename = config.name;
    if (!basename) {
      let index = _path.indexOf(' ');
      if (index > -1) _path = _path.substr(0, index);
      // get process name from filename
      basename = path.basename(_path, path.extname(_path));
    }
    // skip if process is not running
    let isRunning = await this.isRunning(basename);
    if (!isRunning) return;
    // stop the process
    await pm2.stop(basename);

    // list process
    console.log(chalk.green(`stopped process: ${basename}`));
  }

  /**
   * Stop multiple processes
   *
   * @param  {Array<string>} paths array of paths to scripts
   * @return {Promise<any>}        resolved when all process are started
   */
  public static async stopAll(config:Array<any>):Promise<any> {
    for (let p of config) await this.stop(p);
  }

  /**
   * Show list of processes
   */
  public static async list():Promise<any> {
    console.log('LIST');

    return new Promise((resolve, reject) => {
      try {
        console.log('2');
        pm2.list((err, list) => {
          console.log('err', err);
          if (err) {
            reject(err);
            return;
          }
          console.log(list);
          resolve(list);
        });
      }catch (err) {
        console.log('ASDAS');
        reject(err);
      }
    });
  }

}

export default PM2Helper;
