import {exec} from 'child-process-promise';
let cmd = 'bin/cli';

describe('test', () => {

  it('should show help when no command is passed', (done) => {
    exec(cmd)
      .progress(childProcess => {
        // any logic you want to do here while process is running
      })
      .then(result => {
        let p = result.childProcess;
        expect(p.exitCode).toBe(0);
        expect(p.stdout).not.toBe('');
        done();
      })
      .fail(err => {
        console.error(err);
        done();
      });
  });

});
