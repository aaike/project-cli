import './ICommandLine';
import {exec2 as shell_exec} from '../lib/exec';
import PM2Helper from '../lib/PM2Helper';

class PM2CLI implements ICommandLine {

  public commands = {
    start: {
      description: 'start all processes'
    },
    stop: {
      description: 'stop all processes'
    },
    restart: {
      description: 'restart all processes'
    },
    list: {
      description: 'list all processes'
    },
    log: {
      description: 'start streaming logs from pm2 processes'
    }
  };

  constructor(private config:Array<any>) {

  }

  public async exec(argv:any):Promise<any> {
    switch (argv._[0]) {
      case 'start':
        await this.start(undefined, argv.log);
        return true;
      case 'stop':
        await this.stop();
        return true;
      case 'restart':
        await this.restart();
        return true;
      case 'list':
        await this.list();
        return true;
      case 'log':
        await this.log();
        return true;
      default:
        return false;
    }
  }

  /**
   * Start streaming pm2 logs
   *
   * @return {Promise<any>} resolved when script is done executing
   */
  public async log():Promise<any> {
    await shell_exec('pm2', ['logs']);
  }

  public async list():Promise<any> {
    await shell_exec('pm2', ['list']);
  }

  /**
   * Start processes for development
   *
   * @return {Promise<any>} resolved when script is done executing
   */
  public async start(config:Array<any> = undefined, log:boolean = false):Promise<any> {
    if (!config) config = this.config;
    // connect to pm2
    let err = await PM2Helper.connect();
    if (err) throw new Error(err);
    // start all processes
    await PM2Helper.startAll(config);

    //await PM2Helper.list();
    //await shell_exec('pm2', ['list']);

    // disconnect from pm2
    await PM2Helper.disconnect();
  }

  /**
   * Stop processes for development
   *
   * @return {Promise<any>} resolved when script is done executing
   */
  public async stop(config:Array<any> = undefined):Promise<any> {
    if (!config) config = this.config;
    // connect to pm2
    let err = await PM2Helper.connect();
    if (err) throw new Error(err);
    // start all processes
    await PM2Helper.stopAll(config);

    //await PM2Helper.list();
    //await shell_exec('pm2', ['list']);

    // disconnect from pm2
    await PM2Helper.disconnect();
  }

  /**
   * Restart processes for development
   *
   * @return {Promise<any>} resolved when script is done executing
   */
  public async restart(config:Array<string> = undefined):Promise<any> {
    if (!config) config = this.config;
    // connect to pm2
    let err = await PM2Helper.connect();
    if (err) throw new Error(err);
    // stop all processes
    await PM2Helper.stopAll(config);
    // start all processes
    await PM2Helper.startAll(config);
    // disconnect from pm2
    await PM2Helper.disconnect();
  }
}

export default PM2CLI;
