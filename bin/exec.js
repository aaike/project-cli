var spawn = require('child_process').spawn;

module.exports = function exec(cmd, args, options) {
  var parts = cmd.split(' ');
  var command = parts.shift();
  var arg = parts.concat(args || []);
  spawn(command, arg, Object.assign({ stdio: 'inherit' }, options))
    .on('exit', function(code) {
      console.log(code);
    })
    .on('error', function(err) {
      console.error(err);
    });
};
