import path from 'path';

export let paths = {
  source: 'src',
  output: 'dist',
  unit: 'test'
};

export let patterns = {
  source: paths.source + '/**/*.+(ts)',
  unit: paths.unit + '/**/*.spec.js'
};
