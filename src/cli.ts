/// <reference path='../typings.d.ts' />
/// <reference path='../typings/tsd.d.ts' />
'use strict';

//import './lib/es201x';
import * as minimist from 'minimist';
import {logger} from './lib/Logger';
import * as figlet from 'figlet';
import * as fsp from 'fs-promise';
import * as fs from 'fs';

// parse cli arguments using minimist
let argv:any = minimist(process.argv.slice(2));

// prod flag (force production environment with --prod)
if (argv.prod) process.env.NODE_ENV = 'production';

let packageFile = 'package.json';
if (!fs.existsSync(packageFile)) {
  logger.error('package.json not found in this directory !');
  process.exit(0);
}

//const pkg = require('../package.json');
const pkg = JSON.parse(fs.readFileSync(packageFile, 'utf-8'));
let config;

/**
 * Application CLI
 * provides common commands usable by all projects.
 *
 * dependency management: npm, bower, jspm and tsd
 * linting: tslint jscs htmlhint sass-lint
 * testing: karma, protractor
 * pm2: start/stop processes
 */
export class ProjectCLI {

  public commands:Array<any>;

  /**
   * Parse cli arguments and execute by command
   *
   * @return {[type]} [description]
   */
  public async exec() {

    let configFile = 'project.json';
    if (argv.c || argv.config) configFile = argv.c || argv.config;

    if (!fs.existsSync(configFile)) throw new Error(`${configFile} not found in this directory !`);

    //config = require('../' + configFile);
    config = JSON.parse(await fsp.readFile(configFile, 'utf-8'));

    this.commands = [];
    if (config.dependencies && config.dependencies.enabled !== false) {
      let DependenciesCLI = require('./commands/dependencies').default;
      this.commands.push(new DependenciesCLI(config.dependencies));
    }
    if (config.apps) {
      let PM2CLI = require('./commands/pm2').default;
      this.commands.push(new PM2CLI(config.apps));
    }
    if (config.linters && config.linters.enabled !== false) {
      let LintCLI = require('./commands/lint').default;
      this.commands.push(new LintCLI(config.linters));
    }

    let result;
    for (let cli of this.commands) {
      result = await cli.exec(argv);
      if (result && result.errorCount) throw new Error('Lint errors found');
      if (result) break;
    }

    if (!result) await this.showHelp();
  }

  /**
   * Show help
   */
  public showHelp() {
    return new Promise(() => {
      figlet.text(config.title, {font:'Slant'}, (err, data) => {
        logger.log(data);
        logger.log(`${pkg.name} (${pkg.version})\n`);
        logger.log(`Commands:`);

        let output = '';
        for (let cli of this.commands) {
          if (!cli.commands) break;
          for (let [key, cmdObj] of Object.entries(cli.commands)) {
            output += ` ${key} - ${cmdObj.description}\n`;
          }
        }

        logger.log(output);
        return;
      });
    });
  }

};

async function init() {
  try {
    // create instance and execute
    let cli = new ProjectCLI();
    await cli.exec();
  }catch (err) {
    //logger.fatal(err);
    logger.fatal(err.stack);
  }finally{
    process.exit(0);
  }
}

init();
