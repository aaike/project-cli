"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var __awaiter = undefined && undefined.__awaiter || function (thisArg, _arguments, Promise, generator) {
    return new Promise(function (resolve, reject) {
        generator = generator.call(thisArg, _arguments);
        function cast(value) {
            return value instanceof Promise && value.constructor === Promise ? value : new Promise(function (resolve) {
                resolve(value);
            });
        }
        function onfulfill(value) {
            try {
                step("next", value);
            } catch (e) {
                reject(e);
            }
        }
        function onreject(value) {
            try {
                step("throw", value);
            } catch (e) {
                reject(e);
            }
        }
        function step(verb, value) {
            var result = generator[verb](value);
            result.done ? resolve(result.value) : cast(result.value).then(onfulfill, onreject);
        }
        step("next", void 0);
    });
};
var _pm2 = require('pm2');
var path = require('path');
var chalk = require('chalk');
var promisify = require('promisify-node');
var pm2 = promisify(_pm2);

var PM2Helper = function () {
    function PM2Helper() {
        _classCallCheck(this, PM2Helper);
    }

    _createClass(PM2Helper, null, [{
        key: "getProcessInfo",
        value: function getProcessInfo(name) {
            return __awaiter(this, void 0, Promise, regeneratorRuntime.mark(function _callee() {
                var proccesses, process;
                return regeneratorRuntime.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                _context.next = 2;
                                return pm2.describe(name);

                            case 2:
                                proccesses = _context.sent;

                                if (proccesses.length) {
                                    _context.next = 5;
                                    break;
                                }

                                return _context.abrupt("return", Promise.resolve());

                            case 5:
                                process = proccesses[0];
                                return _context.abrupt("return", {
                                    name: process.name,
                                    status: process.status,
                                    pid: process.pid,
                                    pm_id: process.pm_id,
                                    memory: process.monit.memory,
                                    cpu: process.monit.cpu
                                });

                            case 7:
                            case "end":
                                return _context.stop();
                        }
                    }
                }, _callee, this);
            }));
        }
    }, {
        key: "isRunning",
        value: function isRunning(name) {
            return __awaiter(this, void 0, Promise, regeneratorRuntime.mark(function _callee2() {
                var proccesses, process;
                return regeneratorRuntime.wrap(function _callee2$(_context2) {
                    while (1) {
                        switch (_context2.prev = _context2.next) {
                            case 0:
                                _context2.next = 2;
                                return pm2.describe(name);

                            case 2:
                                proccesses = _context2.sent;

                                if (proccesses.length) {
                                    _context2.next = 5;
                                    break;
                                }

                                return _context2.abrupt("return", false);

                            case 5:
                                process = proccesses[0];
                                return _context2.abrupt("return", process.pm2_env.status === 'online' ? true : false);

                            case 7:
                            case "end":
                                return _context2.stop();
                        }
                    }
                }, _callee2, this);
            }));
        }
    }, {
        key: "start",
        value: function start(config) {
            return __awaiter(this, void 0, Promise, regeneratorRuntime.mark(function _callee3() {
                var _path, basename, index;

                return regeneratorRuntime.wrap(function _callee3$(_context3) {
                    while (1) {
                        switch (_context3.prev = _context3.next) {
                            case 0:
                                _path = config.script;
                                basename = config.name;

                                if (!basename) {
                                    index = _path.indexOf(' ');

                                    if (index > -1) _path = _path.substr(0, index);
                                    basename = path.basename(_path, path.extname(_path));
                                }
                                _context3.next = 5;
                                return this.isRunning(basename);

                            case 5:
                                if (!_context3.sent) {
                                    _context3.next = 7;
                                    break;
                                }

                                return _context3.abrupt("return");

                            case 7:
                                _context3.next = 9;
                                return pm2.start(config);

                            case 9:
                                console.log(chalk.green("started process: " + basename));

                            case 10:
                            case "end":
                                return _context3.stop();
                        }
                    }
                }, _callee3, this);
            }));
        }
    }, {
        key: "startAll",
        value: function startAll(config) {
            return __awaiter(this, void 0, Promise, regeneratorRuntime.mark(function _callee4() {
                var _iteratorNormalCompletion, _didIteratorError, _iteratorError, _iterator, _step, p;

                return regeneratorRuntime.wrap(function _callee4$(_context4) {
                    while (1) {
                        switch (_context4.prev = _context4.next) {
                            case 0:
                                _iteratorNormalCompletion = true;
                                _didIteratorError = false;
                                _iteratorError = undefined;
                                _context4.prev = 3;
                                _iterator = config[Symbol.iterator]();

                            case 5:
                                if (_iteratorNormalCompletion = (_step = _iterator.next()).done) {
                                    _context4.next = 12;
                                    break;
                                }

                                p = _step.value;
                                _context4.next = 9;
                                return this.start(p);

                            case 9:
                                _iteratorNormalCompletion = true;
                                _context4.next = 5;
                                break;

                            case 12:
                                _context4.next = 18;
                                break;

                            case 14:
                                _context4.prev = 14;
                                _context4.t0 = _context4["catch"](3);
                                _didIteratorError = true;
                                _iteratorError = _context4.t0;

                            case 18:
                                _context4.prev = 18;
                                _context4.prev = 19;

                                if (!_iteratorNormalCompletion && _iterator.return) {
                                    _iterator.return();
                                }

                            case 21:
                                _context4.prev = 21;

                                if (!_didIteratorError) {
                                    _context4.next = 24;
                                    break;
                                }

                                throw _iteratorError;

                            case 24:
                                return _context4.finish(21);

                            case 25:
                                return _context4.finish(18);

                            case 26:
                            case "end":
                                return _context4.stop();
                        }
                    }
                }, _callee4, this, [[3, 14, 18, 26], [19,, 21, 25]]);
            }));
        }
    }, {
        key: "stop",
        value: function stop(config) {
            return __awaiter(this, void 0, Promise, regeneratorRuntime.mark(function _callee5() {
                var _path, basename, _index, isRunning;

                return regeneratorRuntime.wrap(function _callee5$(_context5) {
                    while (1) {
                        switch (_context5.prev = _context5.next) {
                            case 0:
                                _path = config.script;
                                basename = config.name;

                                if (!basename) {
                                    _index = _path.indexOf(' ');

                                    if (_index > -1) _path = _path.substr(0, _index);
                                    basename = path.basename(_path, path.extname(_path));
                                }
                                _context5.next = 5;
                                return this.isRunning(basename);

                            case 5:
                                isRunning = _context5.sent;

                                if (isRunning) {
                                    _context5.next = 8;
                                    break;
                                }

                                return _context5.abrupt("return");

                            case 8:
                                _context5.next = 10;
                                return pm2.stop(basename);

                            case 10:
                                console.log(chalk.green("stopped process: " + basename));

                            case 11:
                            case "end":
                                return _context5.stop();
                        }
                    }
                }, _callee5, this);
            }));
        }
    }, {
        key: "stopAll",
        value: function stopAll(config) {
            return __awaiter(this, void 0, Promise, regeneratorRuntime.mark(function _callee6() {
                var _iteratorNormalCompletion2, _didIteratorError2, _iteratorError2, _iterator2, _step2, _p;

                return regeneratorRuntime.wrap(function _callee6$(_context6) {
                    while (1) {
                        switch (_context6.prev = _context6.next) {
                            case 0:
                                _iteratorNormalCompletion2 = true;
                                _didIteratorError2 = false;
                                _iteratorError2 = undefined;
                                _context6.prev = 3;
                                _iterator2 = config[Symbol.iterator]();

                            case 5:
                                if (_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done) {
                                    _context6.next = 12;
                                    break;
                                }

                                _p = _step2.value;
                                _context6.next = 9;
                                return this.stop(_p);

                            case 9:
                                _iteratorNormalCompletion2 = true;
                                _context6.next = 5;
                                break;

                            case 12:
                                _context6.next = 18;
                                break;

                            case 14:
                                _context6.prev = 14;
                                _context6.t0 = _context6["catch"](3);
                                _didIteratorError2 = true;
                                _iteratorError2 = _context6.t0;

                            case 18:
                                _context6.prev = 18;
                                _context6.prev = 19;

                                if (!_iteratorNormalCompletion2 && _iterator2.return) {
                                    _iterator2.return();
                                }

                            case 21:
                                _context6.prev = 21;

                                if (!_didIteratorError2) {
                                    _context6.next = 24;
                                    break;
                                }

                                throw _iteratorError2;

                            case 24:
                                return _context6.finish(21);

                            case 25:
                                return _context6.finish(18);

                            case 26:
                            case "end":
                                return _context6.stop();
                        }
                    }
                }, _callee6, this, [[3, 14, 18, 26], [19,, 21, 25]]);
            }));
        }
    }, {
        key: "list",
        value: function list() {
            return __awaiter(this, void 0, Promise, regeneratorRuntime.mark(function _callee7() {
                return regeneratorRuntime.wrap(function _callee7$(_context7) {
                    while (1) {
                        switch (_context7.prev = _context7.next) {
                            case 0:
                                console.log('LIST');
                                return _context7.abrupt("return", new Promise(function (resolve, reject) {
                                    try {
                                        console.log('2');
                                        pm2.list(function (err, list) {
                                            console.log('err', err);
                                            if (err) {
                                                reject(err);
                                                return;
                                            }
                                            console.log(list);
                                            resolve(list);
                                        });
                                    } catch (err) {
                                        console.log('ASDAS');
                                        reject(err);
                                    }
                                }));

                            case 2:
                            case "end":
                                return _context7.stop();
                        }
                    }
                }, _callee7, this);
            }));
        }
    }]);

    return PM2Helper;
}();

PM2Helper.connect = pm2.connect;
PM2Helper.disconnect = pm2.disconnect;
exports.default = PM2Helper;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImxpYi9QTTJIZWxwZXIuanMiLCJsaWIvUE0ySGVscGVyLnRzIl0sIm5hbWVzIjpbIlBNMkhlbHBlciIsIlBNMkhlbHBlci5nZXRQcm9jZXNzSW5mbyIsIlBNMkhlbHBlci5pc1J1bm5pbmciLCJQTTJIZWxwZXIuc3RhcnQiLCJQTTJIZWxwZXIuc3RhcnRBbGwiLCJQTTJIZWxwZXIuc3RvcCIsIlBNMkhlbHBlci5zdG9wQWxsIiwiUE0ySGVscGVyLmxpc3QiXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLElBQUksWUFBWSxTQUFDLElBQVEsVUFBSyxTQUFMLElBQW1CLFVBQVUsT0FBVixFQUFtQixVQUFuQixFQUErQixPQUEvQixFQUF3QyxTQUF4QyxFQUFtRDtBQUMzRixXQUFPLElBQUksT0FBSixDQUFZLFVBQVUsT0FBVixFQUFtQixNQUFuQixFQUEyQjtBQUMxQyxvQkFBWSxVQUFVLElBQVYsQ0FBZSxPQUFmLEVBQXdCLFVBQXhCLENBQVosQ0FEMEM7QUFFMUMsaUJBQVMsSUFBVCxDQUFjLEtBQWQsRUFBcUI7QUFBRSxtQkFBTyxpQkFBaUIsT0FBakIsSUFBNEIsTUFBTSxXQUFOLEtBQXNCLE9BQXRCLEdBQWdDLEtBQTVELEdBQW9FLElBQUksT0FBSixDQUFZLFVBQVUsT0FBVixFQUFtQjtBQUFFLHdCQUFRLEtBQVIsRUFBRjthQUFuQixDQUFoRixDQUFUO1NBQXJCO0FBQ0EsaUJBQVMsU0FBVCxDQUFtQixLQUFuQixFQUEwQjtBQUFFLGdCQUFJO0FBQUUscUJBQUssTUFBTCxFQUFhLEtBQWIsRUFBRjthQUFKLENBQTZCLE9BQU8sQ0FBUCxFQUFVO0FBQUUsdUJBQU8sQ0FBUCxFQUFGO2FBQVY7U0FBekQ7QUFDQSxpQkFBUyxRQUFULENBQWtCLEtBQWxCLEVBQXlCO0FBQUUsZ0JBQUk7QUFBRSxxQkFBSyxPQUFMLEVBQWMsS0FBZCxFQUFGO2FBQUosQ0FBOEIsT0FBTyxDQUFQLEVBQVU7QUFBRSx1QkFBTyxDQUFQLEVBQUY7YUFBVjtTQUF6RDtBQUNBLGlCQUFTLElBQVQsQ0FBYyxJQUFkLEVBQW9CLEtBQXBCLEVBQTJCO0FBQ3ZCLGdCQUFJLFNBQVMsVUFBVSxJQUFWLEVBQWdCLEtBQWhCLENBQVQsQ0FEbUI7QUFFdkIsbUJBQU8sSUFBUCxHQUFjLFFBQVEsT0FBTyxLQUFQLENBQXRCLEdBQXNDLEtBQUssT0FBTyxLQUFQLENBQUwsQ0FBbUIsSUFBbkIsQ0FBd0IsU0FBeEIsRUFBbUMsUUFBbkMsQ0FBdEMsQ0FGdUI7U0FBM0I7QUFJQSxhQUFLLE1BQUwsRUFBYSxLQUFLLENBQUwsQ0FBYixDQVQwQztLQUEzQixDQUFuQixDQUQyRjtDQUFuRDtBQ0E1QyxJQUFZLE9BQUksUUFBTSxLQUFOLENBQUo7QUFDWixJQUFZLE9BQUksUUFBTSxNQUFOLENBQUo7QUFDWixJQUFZLFFBQUssUUFBTSxPQUFOLENBQUw7QUFDWixJQUFZLFlBQVMsUUFBTSxnQkFBTixDQUFUO0FBQ1osSUFBTSxNQUFNLFVBQVUsSUFBVixDQUFOOztJQU9OOzs7Ozs7O3VDQXFCcUNBLE1BQVdBO0FEWnhDLG1CQUFPLFVBQVUsSUFBVixFQUFnQixLQUFLLENBQUwsRUFBUSxPQUF4QiwwQkFBaUM7b0JDYXhDQyxZQUlBQTs7Ozs7O3VDQUptQkEsSUFBSUEsUUFBSkEsQ0FBYUEsSUFBYkE7OztBQUFuQkE7O29DQUdDQSxXQUFXQSxNQUFYQTs7Ozs7aUVBQTBCQSxRQUFRQSxPQUFSQTs7O0FBQzNCQSwwQ0FBVUEsV0FBV0EsQ0FBWEE7aUVBRVBBO0FBQ0xBLDBDQUFNQSxRQUFRQSxJQUFSQTtBQUNOQSw0Q0FBUUEsUUFBUUEsTUFBUkE7QUFDUkEseUNBQUtBLFFBQVFBLEdBQVJBO0FBQ0xBLDJDQUFPQSxRQUFRQSxLQUFSQTtBQUNQQSw0Q0FBUUEsUUFBUUEsS0FBUkEsQ0FBY0EsTUFBZEE7QUFDUkEseUNBQUtBLFFBQVFBLEtBQVJBLENBQWNBLEdBQWRBOzs7Ozs7Ozs7YUR6QnFDLENBQWpDLENBQVAsQ0NZd0NEOzs7O2tDQXVCaEJBLE1BQVdBO0FEbkJuQyxtQkFBTyxVQUFVLElBQVYsRUFBZ0IsS0FBSyxDQUFMLEVBQVEsT0FBeEIsMEJBQWlDO29CQ29CeENFLFlBRUFBOzs7Ozs7dUNBRm1CQSxJQUFJQSxRQUFKQSxDQUFhQSxJQUFiQTs7O0FBQW5CQTs7b0NBQ0NBLFdBQVdBLE1BQVhBOzs7OztrRUFBMEJBOzs7QUFDM0JBLDBDQUFVQSxXQUFXQSxDQUFYQTtrRUFDUEEsT0FBQ0EsQ0FBUUEsT0FBUkEsQ0FBZ0JBLE1BQWhCQSxLQUEyQkEsUUFBM0JBLEdBQXVDQSxJQUF4Q0EsR0FBK0NBLEtBQS9DQTs7Ozs7Ozs7YUR2QnFDLENBQWpDLENBQVAsQ0NtQm1DRjs7Ozs4QkFhZkEsUUFBVUE7QUR2QjlCLG1CQUFPLFVBQVUsSUFBVixFQUFnQixLQUFLLENBQUwsRUFBUSxPQUF4QiwwQkFBaUM7b0JDd0J4Q0csT0FDQUEsVUFFRUE7Ozs7OztBQUhGQSx3Q0FBUUEsT0FBT0EsTUFBUEE7QUFDUkEsMkNBQVdBLE9BQU9BLElBQVBBOztBQUNmQSxvQ0FBSUEsQ0FBQ0EsUUFBREEsRUFBV0E7QUFDVEEsNENBQVFBLE1BQU1BLE9BQU5BLENBQWNBLEdBQWRBLEVBRENBOztBQUViQSx3Q0FBSUEsUUFBUUEsQ0FBQ0EsQ0FBREEsRUFBSUEsUUFBUUEsTUFBTUEsTUFBTkEsQ0FBYUEsQ0FBYkEsRUFBZ0JBLEtBQWhCQSxDQUFSQSxDQUFoQkE7QUFFQUEsK0NBQVdBLEtBQUtBLFFBQUxBLENBQWNBLEtBQWRBLEVBQXFCQSxLQUFLQSxPQUFMQSxDQUFhQSxLQUFiQSxDQUFyQkEsQ0FBWEEsQ0FKYUE7aUNBQWZBOzt1Q0FRVUEsS0FBS0EsU0FBTEEsQ0FBZUEsUUFBZkE7Ozs7Ozs7Ozs7Ozt1Q0FFSkEsSUFBSUEsS0FBSkEsQ0FBVUEsTUFBVkE7OztBQUVOQSx3Q0FBUUEsR0FBUkEsQ0FBWUEsTUFBTUEsS0FBTkEsdUJBQWdDQSxRQUFoQ0EsQ0FBWkE7Ozs7Ozs7O2FEdEM0QyxDQUFqQyxDQUFQLENDdUI4Qkg7Ozs7aUNBd0JQQSxRQUFvQkE7QUQvQjNDLG1CQUFPLFVBQVUsSUFBVixFQUFnQixLQUFLLENBQUwsRUFBUSxPQUF4QiwwQkFBaUM7b0dDZ0NuQ0k7Ozs7Ozs7Ozs7NENBQUtBOzs7Ozs7OztBQUFMQTs7dUNBQW1CQSxLQUFLQSxLQUFMQSxDQUFXQSxDQUFYQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7YURoQ2dCLENBQWpDLENBQVAsQ0MrQjJDSjs7Ozs2QkFVeEJBLFFBQVVBO0FEbkM3QixtQkFBTyxVQUFVLElBQVYsRUFBZ0IsS0FBSyxDQUFMLEVBQVEsT0FBeEIsMEJBQWlDO29CQ29DeENLLE9BRUFBLFVBRUVBLFFBTUZBOzs7Ozs7QUFWQUEsd0NBQVFBLE9BQU9BLE1BQVBBO0FBRVJBLDJDQUFXQSxPQUFPQSxJQUFQQTs7QUFDZkEsb0NBQUlBLENBQUNBLFFBQURBLEVBQVdBO0FBQ1RBLDZDQUFRQSxNQUFNQSxPQUFOQSxDQUFjQSxHQUFkQSxFQURDQTs7QUFFYkEsd0NBQUlBLFNBQVFBLENBQUNBLENBQURBLEVBQUlBLFFBQVFBLE1BQU1BLE1BQU5BLENBQWFBLENBQWJBLEVBQWdCQSxNQUFoQkEsQ0FBUkEsQ0FBaEJBO0FBRUFBLCtDQUFXQSxLQUFLQSxRQUFMQSxDQUFjQSxLQUFkQSxFQUFxQkEsS0FBS0EsT0FBTEEsQ0FBYUEsS0FBYkEsQ0FBckJBLENBQVhBLENBSmFBO2lDQUFmQTs7dUNBT3NCQSxLQUFLQSxTQUFMQSxDQUFlQSxRQUFmQTs7O0FBQWxCQTs7b0NBQ0NBOzs7Ozs7Ozs7dUNBRUNBLElBQUlBLElBQUpBLENBQVNBLFFBQVRBOzs7QUFHTkEsd0NBQVFBLEdBQVJBLENBQVlBLE1BQU1BLEtBQU5BLHVCQUFnQ0EsUUFBaENBLENBQVpBOzs7Ozs7OzthRHBENEMsQ0FBakMsQ0FBUCxDQ21DNkJMOzs7O2dDQTBCUEEsUUFBaUJBO0FENUN2QyxtQkFBTyxVQUFVLElBQVYsRUFBZ0IsS0FBSyxDQUFMLEVBQVEsT0FBeEIsMEJBQWlDO3lHQzZDbkNNOzs7Ozs7Ozs7OzZDQUFLQTs7Ozs7Ozs7QUFBTEE7O3VDQUFtQkEsS0FBS0EsSUFBTEEsQ0FBVUEsRUFBVkE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O2FEN0NnQixDQUFqQyxDQUFQLENDNEN1Q047Ozs7K0JBT3JCQTtBRDdDbEIsbUJBQU8sVUFBVSxJQUFWLEVBQWdCLEtBQUssQ0FBTCxFQUFRLE9BQXhCLDBCQUFpQzs7Ozs7QUM4QzVDTyx3Q0FBUUEsR0FBUkEsQ0FBWUEsTUFBWkE7a0VBRU9BLElBQUlBLE9BQUpBLENBQVlBLFVBQUNBLE9BQURBLEVBQVVBLE1BQVZBLEVBQWdCQTtBQUNqQ0Esd0NBQUlBO0FBQ0ZBLGdEQUFRQSxHQUFSQSxDQUFZQSxHQUFaQSxFQURFQTtBQUVGQSw0Q0FBSUEsSUFBSkEsQ0FBU0EsVUFBQ0EsR0FBREEsRUFBTUEsSUFBTkEsRUFBVUE7QUFDakJBLG9EQUFRQSxHQUFSQSxDQUFZQSxLQUFaQSxFQUFtQkEsR0FBbkJBLEVBRGlCQTtBQUVqQkEsZ0RBQUlBLEdBQUpBLEVBQVNBO0FBQ1BBLHVEQUFPQSxHQUFQQSxFQURPQTtBQUVQQSx1REFGT0E7NkNBQVRBO0FBSUFBLG9EQUFRQSxHQUFSQSxDQUFZQSxJQUFaQSxFQU5pQkE7QUFPakJBLG9EQUFRQSxJQUFSQSxFQVBpQkE7eUNBQVZBLENBQVRBLENBRkVBO3FDQUFKQSxDQVdDQSxPQUFPQSxHQUFQQSxFQUFZQTtBQUNYQSxnREFBUUEsR0FBUkEsQ0FBWUEsT0FBWkEsRUFEV0E7QUFFWEEsK0NBQU9BLEdBQVBBLEVBRldBO3FDQUFaQTtpQ0FaZ0JBOzs7Ozs7OzthRGhEeUIsQ0FBakMsQ0FBUCxDQzZDa0JQOzs7O1dBNUgxQjs7O0FBR2dCLFVBQUEsT0FBQSxHQUFjLElBQUksT0FBSjtBQUNkLFVBQUEsVUFBQSxHQUFpQixJQUFJLFVBQUo7QUFnSmpDLFFBQUEsT0FBQSxHQUFlLFNBQWYiLCJmaWxlIjoibGliL1BNMkhlbHBlci5qcyIsInNvdXJjZXNDb250ZW50IjpbInZhciBfX2F3YWl0ZXIgPSAodGhpcyAmJiB0aGlzLl9fYXdhaXRlcikgfHwgZnVuY3Rpb24gKHRoaXNBcmcsIF9hcmd1bWVudHMsIFByb21pc2UsIGdlbmVyYXRvcikge1xuICAgIHJldHVybiBuZXcgUHJvbWlzZShmdW5jdGlvbiAocmVzb2x2ZSwgcmVqZWN0KSB7XG4gICAgICAgIGdlbmVyYXRvciA9IGdlbmVyYXRvci5jYWxsKHRoaXNBcmcsIF9hcmd1bWVudHMpO1xuICAgICAgICBmdW5jdGlvbiBjYXN0KHZhbHVlKSB7IHJldHVybiB2YWx1ZSBpbnN0YW5jZW9mIFByb21pc2UgJiYgdmFsdWUuY29uc3RydWN0b3IgPT09IFByb21pc2UgPyB2YWx1ZSA6IG5ldyBQcm9taXNlKGZ1bmN0aW9uIChyZXNvbHZlKSB7IHJlc29sdmUodmFsdWUpOyB9KTsgfVxuICAgICAgICBmdW5jdGlvbiBvbmZ1bGZpbGwodmFsdWUpIHsgdHJ5IHsgc3RlcChcIm5leHRcIiwgdmFsdWUpOyB9IGNhdGNoIChlKSB7IHJlamVjdChlKTsgfSB9XG4gICAgICAgIGZ1bmN0aW9uIG9ucmVqZWN0KHZhbHVlKSB7IHRyeSB7IHN0ZXAoXCJ0aHJvd1wiLCB2YWx1ZSk7IH0gY2F0Y2ggKGUpIHsgcmVqZWN0KGUpOyB9IH1cbiAgICAgICAgZnVuY3Rpb24gc3RlcCh2ZXJiLCB2YWx1ZSkge1xuICAgICAgICAgICAgdmFyIHJlc3VsdCA9IGdlbmVyYXRvclt2ZXJiXSh2YWx1ZSk7XG4gICAgICAgICAgICByZXN1bHQuZG9uZSA/IHJlc29sdmUocmVzdWx0LnZhbHVlKSA6IGNhc3QocmVzdWx0LnZhbHVlKS50aGVuKG9uZnVsZmlsbCwgb25yZWplY3QpO1xuICAgICAgICB9XG4gICAgICAgIHN0ZXAoXCJuZXh0XCIsIHZvaWQgMCk7XG4gICAgfSk7XG59O1xudmFyIF9wbTIgPSByZXF1aXJlKCdwbTInKTtcbnZhciBwYXRoID0gcmVxdWlyZSgncGF0aCcpO1xudmFyIGNoYWxrID0gcmVxdWlyZSgnY2hhbGsnKTtcbnZhciBwcm9taXNpZnkgPSByZXF1aXJlKCdwcm9taXNpZnktbm9kZScpO1xuY29uc3QgcG0yID0gcHJvbWlzaWZ5KF9wbTIpO1xuY2xhc3MgUE0ySGVscGVyIHtcbiAgICBzdGF0aWMgZ2V0UHJvY2Vzc0luZm8obmFtZSkge1xuICAgICAgICByZXR1cm4gX19hd2FpdGVyKHRoaXMsIHZvaWQgMCwgUHJvbWlzZSwgZnVuY3Rpb24qICgpIHtcbiAgICAgICAgICAgIGxldCBwcm9jY2Vzc2VzID0geWllbGQgcG0yLmRlc2NyaWJlKG5hbWUpO1xuICAgICAgICAgICAgaWYgKCFwcm9jY2Vzc2VzLmxlbmd0aClcbiAgICAgICAgICAgICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKCk7XG4gICAgICAgICAgICBsZXQgcHJvY2VzcyA9IHByb2NjZXNzZXNbMF07XG4gICAgICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgICAgIG5hbWU6IHByb2Nlc3MubmFtZSxcbiAgICAgICAgICAgICAgICBzdGF0dXM6IHByb2Nlc3Muc3RhdHVzLFxuICAgICAgICAgICAgICAgIHBpZDogcHJvY2Vzcy5waWQsXG4gICAgICAgICAgICAgICAgcG1faWQ6IHByb2Nlc3MucG1faWQsXG4gICAgICAgICAgICAgICAgbWVtb3J5OiBwcm9jZXNzLm1vbml0Lm1lbW9yeSxcbiAgICAgICAgICAgICAgICBjcHU6IHByb2Nlc3MubW9uaXQuY3B1XG4gICAgICAgICAgICB9O1xuICAgICAgICB9KTtcbiAgICB9XG4gICAgc3RhdGljIGlzUnVubmluZyhuYW1lKSB7XG4gICAgICAgIHJldHVybiBfX2F3YWl0ZXIodGhpcywgdm9pZCAwLCBQcm9taXNlLCBmdW5jdGlvbiogKCkge1xuICAgICAgICAgICAgbGV0IHByb2NjZXNzZXMgPSB5aWVsZCBwbTIuZGVzY3JpYmUobmFtZSk7XG4gICAgICAgICAgICBpZiAoIXByb2NjZXNzZXMubGVuZ3RoKVxuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgIGxldCBwcm9jZXNzID0gcHJvY2Nlc3Nlc1swXTtcbiAgICAgICAgICAgIHJldHVybiAocHJvY2Vzcy5wbTJfZW52LnN0YXR1cyA9PT0gJ29ubGluZScpID8gdHJ1ZSA6IGZhbHNlO1xuICAgICAgICB9KTtcbiAgICB9XG4gICAgc3RhdGljIHN0YXJ0KGNvbmZpZykge1xuICAgICAgICByZXR1cm4gX19hd2FpdGVyKHRoaXMsIHZvaWQgMCwgUHJvbWlzZSwgZnVuY3Rpb24qICgpIHtcbiAgICAgICAgICAgIGxldCBfcGF0aCA9IGNvbmZpZy5zY3JpcHQ7XG4gICAgICAgICAgICBsZXQgYmFzZW5hbWUgPSBjb25maWcubmFtZTtcbiAgICAgICAgICAgIGlmICghYmFzZW5hbWUpIHtcbiAgICAgICAgICAgICAgICBsZXQgaW5kZXggPSBfcGF0aC5pbmRleE9mKCcgJyk7XG4gICAgICAgICAgICAgICAgaWYgKGluZGV4ID4gLTEpXG4gICAgICAgICAgICAgICAgICAgIF9wYXRoID0gX3BhdGguc3Vic3RyKDAsIGluZGV4KTtcbiAgICAgICAgICAgICAgICBiYXNlbmFtZSA9IHBhdGguYmFzZW5hbWUoX3BhdGgsIHBhdGguZXh0bmFtZShfcGF0aCkpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKHlpZWxkIHRoaXMuaXNSdW5uaW5nKGJhc2VuYW1lKSlcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB5aWVsZCBwbTIuc3RhcnQoY29uZmlnKTtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKGNoYWxrLmdyZWVuKGBzdGFydGVkIHByb2Nlc3M6ICR7YmFzZW5hbWV9YCkpO1xuICAgICAgICB9KTtcbiAgICB9XG4gICAgc3RhdGljIHN0YXJ0QWxsKGNvbmZpZykge1xuICAgICAgICByZXR1cm4gX19hd2FpdGVyKHRoaXMsIHZvaWQgMCwgUHJvbWlzZSwgZnVuY3Rpb24qICgpIHtcbiAgICAgICAgICAgIGZvciAobGV0IHAgb2YgY29uZmlnKVxuICAgICAgICAgICAgICAgIHlpZWxkIHRoaXMuc3RhcnQocCk7XG4gICAgICAgIH0pO1xuICAgIH1cbiAgICBzdGF0aWMgc3RvcChjb25maWcpIHtcbiAgICAgICAgcmV0dXJuIF9fYXdhaXRlcih0aGlzLCB2b2lkIDAsIFByb21pc2UsIGZ1bmN0aW9uKiAoKSB7XG4gICAgICAgICAgICBsZXQgX3BhdGggPSBjb25maWcuc2NyaXB0O1xuICAgICAgICAgICAgbGV0IGJhc2VuYW1lID0gY29uZmlnLm5hbWU7XG4gICAgICAgICAgICBpZiAoIWJhc2VuYW1lKSB7XG4gICAgICAgICAgICAgICAgbGV0IGluZGV4ID0gX3BhdGguaW5kZXhPZignICcpO1xuICAgICAgICAgICAgICAgIGlmIChpbmRleCA+IC0xKVxuICAgICAgICAgICAgICAgICAgICBfcGF0aCA9IF9wYXRoLnN1YnN0cigwLCBpbmRleCk7XG4gICAgICAgICAgICAgICAgYmFzZW5hbWUgPSBwYXRoLmJhc2VuYW1lKF9wYXRoLCBwYXRoLmV4dG5hbWUoX3BhdGgpKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGxldCBpc1J1bm5pbmcgPSB5aWVsZCB0aGlzLmlzUnVubmluZyhiYXNlbmFtZSk7XG4gICAgICAgICAgICBpZiAoIWlzUnVubmluZylcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB5aWVsZCBwbTIuc3RvcChiYXNlbmFtZSk7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhjaGFsay5ncmVlbihgc3RvcHBlZCBwcm9jZXNzOiAke2Jhc2VuYW1lfWApKTtcbiAgICAgICAgfSk7XG4gICAgfVxuICAgIHN0YXRpYyBzdG9wQWxsKGNvbmZpZykge1xuICAgICAgICByZXR1cm4gX19hd2FpdGVyKHRoaXMsIHZvaWQgMCwgUHJvbWlzZSwgZnVuY3Rpb24qICgpIHtcbiAgICAgICAgICAgIGZvciAobGV0IHAgb2YgY29uZmlnKVxuICAgICAgICAgICAgICAgIHlpZWxkIHRoaXMuc3RvcChwKTtcbiAgICAgICAgfSk7XG4gICAgfVxuICAgIHN0YXRpYyBsaXN0KCkge1xuICAgICAgICByZXR1cm4gX19hd2FpdGVyKHRoaXMsIHZvaWQgMCwgUHJvbWlzZSwgZnVuY3Rpb24qICgpIHtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKCdMSVNUJyk7XG4gICAgICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCcyJyk7XG4gICAgICAgICAgICAgICAgICAgIHBtMi5saXN0KChlcnIsIGxpc3QpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdlcnInLCBlcnIpO1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGVycikge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlamVjdChlcnIpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGxpc3QpO1xuICAgICAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZShsaXN0KTtcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGNhdGNoIChlcnIpIHtcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ0FTREFTJyk7XG4gICAgICAgICAgICAgICAgICAgIHJlamVjdChlcnIpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICB9XG59XG5QTTJIZWxwZXIuY29ubmVjdCA9IHBtMi5jb25uZWN0O1xuUE0ySGVscGVyLmRpc2Nvbm5lY3QgPSBwbTIuZGlzY29ubmVjdDtcbmV4cG9ydHMuZGVmYXVsdCA9IFBNMkhlbHBlcjtcbiIsImltcG9ydCAqIGFzIF9wbTIgZnJvbSAncG0yJztcbmltcG9ydCAqIGFzIHBhdGggZnJvbSAncGF0aCc7XG5pbXBvcnQgKiBhcyBjaGFsayBmcm9tICdjaGFsayc7XG5pbXBvcnQgKiBhcyBwcm9taXNpZnkgZnJvbSAncHJvbWlzaWZ5LW5vZGUnO1xuY29uc3QgcG0yID0gcHJvbWlzaWZ5KF9wbTIpO1xuXG4vKipcbiAqIFBNMiBoZWxwZXIgY2xhc3Mgd2l0aCBzdGF0aWMgbWV0aG9kcyB0byBjb250cm9sIHByb2Nlc3Nlcy5cbiAqXG4gKiBAc2VlIGh0dHA6Ly9wbTIua2V5bWV0cmljcy5pby9kb2NzL3VzYWdlL3BtMi1hcGkvXG4gKi9cbmNsYXNzIFBNMkhlbHBlciB7XG5cbiAgLy8gLS0tLSBwbTIgZnVuY3Rpb24gYWxpYXNlc1xuICBwdWJsaWMgc3RhdGljIGNvbm5lY3Q6YW55ID0gcG0yLmNvbm5lY3Q7XG4gIHB1YmxpYyBzdGF0aWMgZGlzY29ubmVjdDphbnkgPSBwbTIuZGlzY29ubmVjdDtcblxuICAvKipcbiAgICogQ29uZmlndXJlIG9wdGlvbnMgZm9yIHRoaXMgaGVscGVyXG4gICAqXG4gICAqIEBwYXJhbSAge2FueX0gICAgICAgICAgb3B0aW9ucyBoYXNoIG9mIG9wdGlvbnNcbiAgICovXG4gIC8qcHVibGljIHN0YXRpYyBjb25maWcob3B0aW9uczphbnkpOnZvaWQge1xuICAgIHRoaXMub3B0aW9ucyA9IG9wdGlvbnM7XG4gIH0qL1xuXG4gIC8qKlxuICAgKiBHZXQgZGV0YWlsZWQgaW5mb3JtYXRpb24gb24gYSBwcm9jZXNzXG4gICAqXG4gICAqIEBwYXJhbSAge3N0cmluZ30gICAgICAgbmFtZSBuYW1lIG9mIHRoZSBwcm9jZXNzIChhcyBhc3NpZ25lZCBieSBwbTIpXG4gICAqIEByZXR1cm4ge1Byb21pc2U8YW55Pn0gICAgICBbZGVzY3JpcHRpb25dXG4gICAqL1xuICBwdWJsaWMgc3RhdGljIGFzeW5jIGdldFByb2Nlc3NJbmZvKG5hbWU6c3RyaW5nKTpQcm9taXNlPGFueT4ge1xuICAgIGxldCBwcm9jY2Vzc2VzID0gYXdhaXQgcG0yLmRlc2NyaWJlKG5hbWUpO1xuXG4gICAgLy8gc2tpcCBpZiBwcm9jZXNzIGlzIG5vdCBydW5uaW5nXG4gICAgaWYgKCFwcm9jY2Vzc2VzLmxlbmd0aCkgcmV0dXJuIFByb21pc2UucmVzb2x2ZSgpO1xuICAgIGxldCBwcm9jZXNzID0gcHJvY2Nlc3Nlc1swXTtcblxuICAgIHJldHVybiB7XG4gICAgICBuYW1lOiBwcm9jZXNzLm5hbWUsXG4gICAgICBzdGF0dXM6IHByb2Nlc3Muc3RhdHVzLFxuICAgICAgcGlkOiBwcm9jZXNzLnBpZCxcbiAgICAgIHBtX2lkOiBwcm9jZXNzLnBtX2lkLFxuICAgICAgbWVtb3J5OiBwcm9jZXNzLm1vbml0Lm1lbW9yeSxcbiAgICAgIGNwdTogcHJvY2Vzcy5tb25pdC5jcHVcbiAgICB9O1xuICB9XG5cbiAgLyoqXG4gICAqIENoZWNrIGlmIGEgcHJvY2VzcyBpcyBydW5uaW5nXG4gICAqXG4gICAqIEBwYXJhbSAge3N0cmluZ30gICAgICAgbmFtZSBuYW1lIG9mIHRoZSBwcm9jZXNzIChhcyBhc3NpZ25lZCBieSBwbTIpXG4gICAqIEByZXR1cm4ge1Byb21pc2U8YW55Pn0gICAgICBbZGVzY3JpcHRpb25dXG4gICAqL1xuICBwdWJsaWMgc3RhdGljIGFzeW5jIGlzUnVubmluZyhuYW1lOnN0cmluZyk6UHJvbWlzZTxhbnk+IHtcbiAgICBsZXQgcHJvY2Nlc3NlcyA9IGF3YWl0IHBtMi5kZXNjcmliZShuYW1lKTtcbiAgICBpZiAoIXByb2NjZXNzZXMubGVuZ3RoKSByZXR1cm4gZmFsc2U7XG4gICAgbGV0IHByb2Nlc3MgPSBwcm9jY2Vzc2VzWzBdO1xuICAgIHJldHVybiAocHJvY2Vzcy5wbTJfZW52LnN0YXR1cyA9PT0gJ29ubGluZScpID8gdHJ1ZSA6IGZhbHNlO1xuICB9XG5cbiAgLyoqXG4gICAqIFN0YXJ0IGEgcHJvY2Vzc1xuICAgKlxuICAgKiBAcGFyYW0gIHtzdHJpbmd9ICAgICAgIHBhdGggICAgcGF0aCB0byBhIHNjcmlwdCB0aGF0IHN0YXJ0cyBhIHByb2Nlc3NcbiAgICogQHJldHVybiB7UHJvbWlzZTxhbnk+fSAgICAgICAgIHJlc29sdmVkIHdoZW4gdGhlIHByb2Nlc3MgaXMgc3RhcnRlZCBvciBoYXMgYWxyZWFkeSBiZWVuIHN0YXJ0ZWQuXG4gICAqL1xuICBwdWJsaWMgc3RhdGljIGFzeW5jIHN0YXJ0KGNvbmZpZzphbnkpOlByb21pc2U8YW55PiB7XG4gICAgbGV0IF9wYXRoID0gY29uZmlnLnNjcmlwdDtcbiAgICBsZXQgYmFzZW5hbWUgPSBjb25maWcubmFtZTtcbiAgICBpZiAoIWJhc2VuYW1lKSB7XG4gICAgICBsZXQgaW5kZXggPSBfcGF0aC5pbmRleE9mKCcgJyk7XG4gICAgICBpZiAoaW5kZXggPiAtMSkgX3BhdGggPSBfcGF0aC5zdWJzdHIoMCwgaW5kZXgpO1xuICAgICAgLy8gZ2V0IHByb2Nlc3MgbmFtZSBmcm9tIGZpbGVuYW1lXG4gICAgICBiYXNlbmFtZSA9IHBhdGguYmFzZW5hbWUoX3BhdGgsIHBhdGguZXh0bmFtZShfcGF0aCkpO1xuICAgIH1cblxuICAgIC8vIHNraXAgaWYgcHJvY2VzcyBpcyBhbHJlYWR5IHJ1bm5pbmdcbiAgICBpZiAoYXdhaXQgdGhpcy5pc1J1bm5pbmcoYmFzZW5hbWUpKSByZXR1cm47XG4gICAgLy8gc3RhcnQgdGhlIHByb2Nlc3NcbiAgICBhd2FpdCBwbTIuc3RhcnQoY29uZmlnKTtcblxuICAgIGNvbnNvbGUubG9nKGNoYWxrLmdyZWVuKGBzdGFydGVkIHByb2Nlc3M6ICR7YmFzZW5hbWV9YCkpO1xuICB9XG5cbiAgLyoqXG4gICAqIFN0YXJ0IG11bHRpcGxlIHByb2Nlc3Nlc1xuICAgKlxuICAgKiBAcGFyYW0gIHtBcnJheTxzdHJpbmc+fSBwYXRocyAgICBhcnJheSBvZiBwYXRocyB0byBzY3JpcHRzXG4gICAqIEByZXR1cm4ge1Byb21pc2U8YW55Pn0gICAgICAgICAgIHJlc29sdmVkIHdoZW4gYWxsIHByb2Nlc3MgYXJlIHN0YXJ0ZWRcbiAgICovXG4gIHB1YmxpYyBzdGF0aWMgYXN5bmMgc3RhcnRBbGwoY29uZmlnOkFycmF5PHN0cmluZz4pOlByb21pc2U8YW55PiB7XG4gICAgZm9yIChsZXQgcCBvZiBjb25maWcpIGF3YWl0IHRoaXMuc3RhcnQocCk7XG4gIH1cblxuICAvKipcbiAgICogU3RvcCBhIHByb2Nlc3NcbiAgICpcbiAgICogQHBhcmFtICB7c3RyaW5nfSAgICAgICBwYXRoICAgIHBhdGggdG8gYSBzY3JpcHQgdGhhdCBzdGFydHMgYSBwcm9jZXNzXG4gICAqIEByZXR1cm4ge1Byb21pc2U8YW55Pn0gICAgICAgICByZXNvbHZlZCB3aGVuIHRoZSBwcm9jZXNzIGlzIHN0YXJ0ZWQgb3IgaGFzIGFscmVhZHkgYmVlbiBzdGFydGVkLlxuICAgKi9cbiAgcHVibGljIHN0YXRpYyBhc3luYyBzdG9wKGNvbmZpZzphbnkpOlByb21pc2U8YW55PiB7XG4gICAgbGV0IF9wYXRoID0gY29uZmlnLnNjcmlwdDtcblxuICAgIGxldCBiYXNlbmFtZSA9IGNvbmZpZy5uYW1lO1xuICAgIGlmICghYmFzZW5hbWUpIHtcbiAgICAgIGxldCBpbmRleCA9IF9wYXRoLmluZGV4T2YoJyAnKTtcbiAgICAgIGlmIChpbmRleCA+IC0xKSBfcGF0aCA9IF9wYXRoLnN1YnN0cigwLCBpbmRleCk7XG4gICAgICAvLyBnZXQgcHJvY2VzcyBuYW1lIGZyb20gZmlsZW5hbWVcbiAgICAgIGJhc2VuYW1lID0gcGF0aC5iYXNlbmFtZShfcGF0aCwgcGF0aC5leHRuYW1lKF9wYXRoKSk7XG4gICAgfVxuICAgIC8vIHNraXAgaWYgcHJvY2VzcyBpcyBub3QgcnVubmluZ1xuICAgIGxldCBpc1J1bm5pbmcgPSBhd2FpdCB0aGlzLmlzUnVubmluZyhiYXNlbmFtZSk7XG4gICAgaWYgKCFpc1J1bm5pbmcpIHJldHVybjtcbiAgICAvLyBzdG9wIHRoZSBwcm9jZXNzXG4gICAgYXdhaXQgcG0yLnN0b3AoYmFzZW5hbWUpO1xuXG4gICAgLy8gbGlzdCBwcm9jZXNzXG4gICAgY29uc29sZS5sb2coY2hhbGsuZ3JlZW4oYHN0b3BwZWQgcHJvY2VzczogJHtiYXNlbmFtZX1gKSk7XG4gIH1cblxuICAvKipcbiAgICogU3RvcCBtdWx0aXBsZSBwcm9jZXNzZXNcbiAgICpcbiAgICogQHBhcmFtICB7QXJyYXk8c3RyaW5nPn0gcGF0aHMgYXJyYXkgb2YgcGF0aHMgdG8gc2NyaXB0c1xuICAgKiBAcmV0dXJuIHtQcm9taXNlPGFueT59ICAgICAgICByZXNvbHZlZCB3aGVuIGFsbCBwcm9jZXNzIGFyZSBzdGFydGVkXG4gICAqL1xuICBwdWJsaWMgc3RhdGljIGFzeW5jIHN0b3BBbGwoY29uZmlnOkFycmF5PGFueT4pOlByb21pc2U8YW55PiB7XG4gICAgZm9yIChsZXQgcCBvZiBjb25maWcpIGF3YWl0IHRoaXMuc3RvcChwKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBTaG93IGxpc3Qgb2YgcHJvY2Vzc2VzXG4gICAqL1xuICBwdWJsaWMgc3RhdGljIGFzeW5jIGxpc3QoKTpQcm9taXNlPGFueT4ge1xuICAgIGNvbnNvbGUubG9nKCdMSVNUJyk7XG5cbiAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgdHJ5IHtcbiAgICAgICAgY29uc29sZS5sb2coJzInKTtcbiAgICAgICAgcG0yLmxpc3QoKGVyciwgbGlzdCkgPT4ge1xuICAgICAgICAgIGNvbnNvbGUubG9nKCdlcnInLCBlcnIpO1xuICAgICAgICAgIGlmIChlcnIpIHtcbiAgICAgICAgICAgIHJlamVjdChlcnIpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgIH1cbiAgICAgICAgICBjb25zb2xlLmxvZyhsaXN0KTtcbiAgICAgICAgICByZXNvbHZlKGxpc3QpO1xuICAgICAgICB9KTtcbiAgICAgIH1jYXRjaCAoZXJyKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKCdBU0RBUycpO1xuICAgICAgICByZWplY3QoZXJyKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG59XG5cbmV4cG9ydCBkZWZhdWx0IFBNMkhlbHBlcjtcbiJdLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==
