# Project CLI

This is a command line interface that can be used to accomplish common tasks while developing web and mobile applications.

Settings for the various cli commands are configured in a `project.json` file.

Various functionalities are added through cli plugins.
These can be enabled/disabled in the `project.json` file.
Below is a list of the cli plugins and what they do.

The project.json can have an "apps" section that holds a
pm2 [application declaration](http://pm2.keymetrics.io/docs/usage/application-declaration).

### Dependencies plugin

Adds the `install` and `update` commands.  
Installs all needed dependencies and will do an initial build of assets.

### Linter plugin

Adds the `lint` command.
Provides an easy way to run all linters for all used languages in the project and have a compact report.

At the moment `jscs`, `tslint`, `sasslint` and `htmlhint` is supported.

## Installation

    npm install git+https://git@bitbucket.org/aaike/project-cli.git

Make sure your project has a `project.json` file.
An example config is included in this repo named `project-sample.json`.
each plugin in the project.json can have an enabled boolean property to toggle it.

## Usage

Make sure the package is installed.
The cli will be available at `./node_modules/.bin/project-cli`
Add a cli file to the root of you repository to get easy access to it.

/cli
    #!/usr/bin/env node
    require('./node_modules/.bin/project-cli');

Then run the cli using

    node cli

Alternatively install the package globally to use it anywhere.
Once installed the cli will be available as `project-cli`.
It will look for a `project.json` file in the directory where it is executed.

## Development Setup

Make sure the necessary global dependencies are installed

    npm install -g tsd gulp pm2

Install local dependencies.

    npm run setup

## Development

The TypeScript files are transpiled with TSC.
A gulp task is available that will transpile all files.

    npm run build

Top watch for changed use the `watch` task.

    npm run watch

## Linting

Linting should be integrated with your IDE for a good development experience.  
When using Atom make sure the needed plugins are installed.

The linters can also be executed from the cli.

    npm run lint

## Testing

### Unit tests

Run unit tests with karma/jasmine

    npm run unit

### Static tests

There are some test included in this repo that can be used to test the linters using the fixtures in  test/fixtures.

To run a lint test that should not have any errors execute:

    npm run lint-test

To run a lint test that will have errors execute:

    npm run lint-test-errors

## Build

Bundle files into the dist directory.

    npm run build
