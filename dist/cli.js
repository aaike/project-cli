'use strict';

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var __awaiter = undefined && undefined.__awaiter || function (thisArg, _arguments, Promise, generator) {
    return new Promise(function (resolve, reject) {
        generator = generator.call(thisArg, _arguments);
        function cast(value) {
            return value instanceof Promise && value.constructor === Promise ? value : new Promise(function (resolve) {
                resolve(value);
            });
        }
        function onfulfill(value) {
            try {
                step("next", value);
            } catch (e) {
                reject(e);
            }
        }
        function onreject(value) {
            try {
                step("throw", value);
            } catch (e) {
                reject(e);
            }
        }
        function step(verb, value) {
            var result = generator[verb](value);
            result.done ? resolve(result.value) : cast(result.value).then(onfulfill, onreject);
        }
        step("next", void 0);
    });
};
var minimist = require('minimist');
var Logger_1 = require('./lib/Logger');
var figlet = require('figlet');
var fsp = require('fs-promise');
var fs = require('fs');
var argv = minimist(process.argv.slice(2));
if (argv.prod) process.env.NODE_ENV = 'production';
var packageFile = 'package.json';
if (!fs.existsSync(packageFile)) {
    Logger_1.logger.error('package.json not found in this directory !');
    process.exit(0);
}
var pkg = JSON.parse(fs.readFileSync(packageFile, 'utf-8'));
var config = undefined;

var ProjectCLI = function () {
    function ProjectCLI() {
        _classCallCheck(this, ProjectCLI);
    }

    _createClass(ProjectCLI, [{
        key: "exec",
        value: function exec() {
            return __awaiter(this, void 0, Promise, regeneratorRuntime.mark(function _callee() {
                var configFile, DependenciesCLI, PM2CLI, LintCLI, result, _iteratorNormalCompletion, _didIteratorError, _iteratorError, _iterator, _step, cli;

                return regeneratorRuntime.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                configFile = 'project.json';

                                if (argv.c || argv.config) configFile = argv.c || argv.config;

                                if (fs.existsSync(configFile)) {
                                    _context.next = 4;
                                    break;
                                }

                                throw new Error(configFile + " not found in this directory !");

                            case 4:
                                _context.t0 = JSON;
                                _context.next = 7;
                                return fsp.readFile(configFile, 'utf-8');

                            case 7:
                                _context.t1 = _context.sent;
                                config = _context.t0.parse.call(_context.t0, _context.t1);

                                this.commands = [];
                                if (config.dependencies && config.dependencies.enabled !== false) {
                                    DependenciesCLI = require('./commands/dependencies').default;

                                    this.commands.push(new DependenciesCLI(config.dependencies));
                                }
                                if (config.apps) {
                                    PM2CLI = require('./commands/pm2').default;

                                    this.commands.push(new PM2CLI(config.apps));
                                }
                                if (config.linters && config.linters.enabled !== false) {
                                    LintCLI = require('./commands/lint').default;

                                    this.commands.push(new LintCLI(config.linters));
                                }
                                result = undefined;
                                _iteratorNormalCompletion = true;
                                _didIteratorError = false;
                                _iteratorError = undefined;
                                _context.prev = 17;
                                _iterator = this.commands[Symbol.iterator]();

                            case 19:
                                if (_iteratorNormalCompletion = (_step = _iterator.next()).done) {
                                    _context.next = 31;
                                    break;
                                }

                                cli = _step.value;
                                _context.next = 23;
                                return cli.exec(argv);

                            case 23:
                                result = _context.sent;

                                if (!(result && result.errorCount)) {
                                    _context.next = 26;
                                    break;
                                }

                                throw new Error('Lint errors found');

                            case 26:
                                if (!result) {
                                    _context.next = 28;
                                    break;
                                }

                                return _context.abrupt("break", 31);

                            case 28:
                                _iteratorNormalCompletion = true;
                                _context.next = 19;
                                break;

                            case 31:
                                _context.next = 37;
                                break;

                            case 33:
                                _context.prev = 33;
                                _context.t2 = _context["catch"](17);
                                _didIteratorError = true;
                                _iteratorError = _context.t2;

                            case 37:
                                _context.prev = 37;
                                _context.prev = 38;

                                if (!_iteratorNormalCompletion && _iterator.return) {
                                    _iterator.return();
                                }

                            case 40:
                                _context.prev = 40;

                                if (!_didIteratorError) {
                                    _context.next = 43;
                                    break;
                                }

                                throw _iteratorError;

                            case 43:
                                return _context.finish(40);

                            case 44:
                                return _context.finish(37);

                            case 45:
                                if (result) {
                                    _context.next = 48;
                                    break;
                                }

                                _context.next = 48;
                                return this.showHelp();

                            case 48:
                            case "end":
                                return _context.stop();
                        }
                    }
                }, _callee, this, [[17, 33, 37, 45], [38,, 40, 44]]);
            }));
        }
    }, {
        key: "showHelp",
        value: function showHelp() {
            var _this = this;

            return new Promise(function () {
                figlet.text(config.title, { font: 'Slant' }, function (err, data) {
                    Logger_1.logger.log(data);
                    Logger_1.logger.log(pkg.name + " (" + pkg.version + ")\n");
                    Logger_1.logger.log("Commands:");
                    var output = '';
                    var _iteratorNormalCompletion2 = true;
                    var _didIteratorError2 = false;
                    var _iteratorError2 = undefined;

                    try {
                        for (var _iterator2 = _this.commands[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                            var _cli = _step2.value;

                            if (!_cli.commands) break;
                            var _iteratorNormalCompletion3 = true;
                            var _didIteratorError3 = false;
                            var _iteratorError3 = undefined;

                            try {
                                for (var _iterator3 = Object.entries(_cli.commands)[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
                                    var _step3$value = _slicedToArray(_step3.value, 2);

                                    var key = _step3$value[0];
                                    var cmdObj = _step3$value[1];

                                    output += " " + key + " - " + cmdObj.description + "\n";
                                }
                            } catch (err) {
                                _didIteratorError3 = true;
                                _iteratorError3 = err;
                            } finally {
                                try {
                                    if (!_iteratorNormalCompletion3 && _iterator3.return) {
                                        _iterator3.return();
                                    }
                                } finally {
                                    if (_didIteratorError3) {
                                        throw _iteratorError3;
                                    }
                                }
                            }
                        }
                    } catch (err) {
                        _didIteratorError2 = true;
                        _iteratorError2 = err;
                    } finally {
                        try {
                            if (!_iteratorNormalCompletion2 && _iterator2.return) {
                                _iterator2.return();
                            }
                        } finally {
                            if (_didIteratorError2) {
                                throw _iteratorError2;
                            }
                        }
                    }

                    Logger_1.logger.log(output);
                    return;
                });
            });
        }
    }]);

    return ProjectCLI;
}();

exports.ProjectCLI = ProjectCLI;
;
function init() {
    return __awaiter(this, void 0, Promise, regeneratorRuntime.mark(function _callee2() {
        var _cli2;

        return regeneratorRuntime.wrap(function _callee2$(_context2) {
            while (1) {
                switch (_context2.prev = _context2.next) {
                    case 0:
                        _context2.prev = 0;
                        _cli2 = new ProjectCLI();
                        _context2.next = 4;
                        return _cli2.exec();

                    case 4:
                        _context2.next = 9;
                        break;

                    case 6:
                        _context2.prev = 6;
                        _context2.t0 = _context2["catch"](0);

                        Logger_1.logger.fatal(_context2.t0.stack);

                    case 9:
                        _context2.prev = 9;

                        process.exit(0);
                        return _context2.finish(9);

                    case 12:
                    case "end":
                        return _context2.stop();
                }
            }
        }, _callee2, this, [[0, 6, 9, 12]]);
    }));
}
init();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNsaS50cyIsImNsaS5qcyJdLCJuYW1lcyI6WyJQcm9qZWN0Q0xJIiwiUHJvamVjdENMSS5leGVjIiwiUHJvamVjdENMSS5zaG93SGVscCIsImluaXQiXSwibWFwcGluZ3MiOiJBQUVBOzs7Ozs7OztBQ0RBLElBQUksWUFBWSxTQUFDLElBQVEsVUFBSyxTQUFMLElBQW1CLFVBQVUsT0FBVixFQUFtQixVQUFuQixFQUErQixPQUEvQixFQUF3QyxTQUF4QyxFQUFtRDtBQUMzRixXQUFPLElBQUksT0FBSixDQUFZLFVBQVUsT0FBVixFQUFtQixNQUFuQixFQUEyQjtBQUMxQyxvQkFBWSxVQUFVLElBQVYsQ0FBZSxPQUFmLEVBQXdCLFVBQXhCLENBQVosQ0FEMEM7QUFFMUMsaUJBQVMsSUFBVCxDQUFjLEtBQWQsRUFBcUI7QUFBRSxtQkFBTyxpQkFBaUIsT0FBakIsSUFBNEIsTUFBTSxXQUFOLEtBQXNCLE9BQXRCLEdBQWdDLEtBQTVELEdBQW9FLElBQUksT0FBSixDQUFZLFVBQVUsT0FBVixFQUFtQjtBQUFFLHdCQUFRLEtBQVIsRUFBRjthQUFuQixDQUFoRixDQUFUO1NBQXJCO0FBQ0EsaUJBQVMsU0FBVCxDQUFtQixLQUFuQixFQUEwQjtBQUFFLGdCQUFJO0FBQUUscUJBQUssTUFBTCxFQUFhLEtBQWIsRUFBRjthQUFKLENBQTZCLE9BQU8sQ0FBUCxFQUFVO0FBQUUsdUJBQU8sQ0FBUCxFQUFGO2FBQVY7U0FBekQ7QUFDQSxpQkFBUyxRQUFULENBQWtCLEtBQWxCLEVBQXlCO0FBQUUsZ0JBQUk7QUFBRSxxQkFBSyxPQUFMLEVBQWMsS0FBZCxFQUFGO2FBQUosQ0FBOEIsT0FBTyxDQUFQLEVBQVU7QUFBRSx1QkFBTyxDQUFQLEVBQUY7YUFBVjtTQUF6RDtBQUNBLGlCQUFTLElBQVQsQ0FBYyxJQUFkLEVBQW9CLEtBQXBCLEVBQTJCO0FBQ3ZCLGdCQUFJLFNBQVMsVUFBVSxJQUFWLEVBQWdCLEtBQWhCLENBQVQsQ0FEbUI7QUFFdkIsbUJBQU8sSUFBUCxHQUFjLFFBQVEsT0FBTyxLQUFQLENBQXRCLEdBQXNDLEtBQUssT0FBTyxLQUFQLENBQUwsQ0FBbUIsSUFBbkIsQ0FBd0IsU0FBeEIsRUFBbUMsUUFBbkMsQ0FBdEMsQ0FGdUI7U0FBM0I7QUFJQSxhQUFLLE1BQUwsRUFBYSxLQUFLLENBQUwsQ0FBYixDQVQwQztLQUEzQixDQUFuQixDQUQyRjtDQUFuRDtBREk1QyxJQUFZLFdBQVEsUUFBTSxVQUFOLENBQVI7QUFDWixJQUFBLFdBQUEsUUFBcUIsY0FBckIsQ0FBQTtBQUNBLElBQVksU0FBTSxRQUFNLFFBQU4sQ0FBTjtBQUNaLElBQVksTUFBRyxRQUFNLFlBQU4sQ0FBSDtBQUNaLElBQVksS0FBRSxRQUFNLElBQU4sQ0FBRjtBQUdaLElBQUksT0FBVyxTQUFTLFFBQVEsSUFBUixDQUFhLEtBQWIsQ0FBbUIsQ0FBbkIsQ0FBVCxDQUFYO0FBR0osSUFBSSxLQUFLLElBQUwsRUFBVyxRQUFRLEdBQVIsQ0FBWSxRQUFaLEdBQXVCLFlBQXZCLENBQWY7QUFFQSxJQUFJLGNBQWMsY0FBZDtBQUNKLElBQUksQ0FBQyxHQUFHLFVBQUgsQ0FBYyxXQUFkLENBQUQsRUFBNkI7QUFDL0IsYUFBQSxNQUFBLENBQU8sS0FBUCxDQUFhLDRDQUFiLEVBRCtCO0FBRS9CLFlBQVEsSUFBUixDQUFhLENBQWIsRUFGK0I7Q0FBakM7QUFNQSxJQUFNLE1BQU0sS0FBSyxLQUFMLENBQVcsR0FBRyxZQUFILENBQWdCLFdBQWhCLEVBQTZCLE9BQTdCLENBQVgsQ0FBTjtBQUNOLElBQUksa0JBQUo7O0lBV0E7Ozs7Ozs7K0JBU21CQTtBQ2RYLG1CQUFPLFVBQVUsSUFBVixFQUFnQixLQUFLLENBQUwsRUFBUSxPQUF4QiwwQkFBaUM7b0JEZ0J4Q0MsWUFVRUEsaUJBSUFBLFFBSUFBLFNBSUZBLHdGQUNLQTs7Ozs7O0FBdkJMQSw2Q0FBYUE7O0FBQ2pCQSxvQ0FBSUEsS0FBS0EsQ0FBTEEsSUFBVUEsS0FBS0EsTUFBTEEsRUFBYUEsYUFBYUEsS0FBS0EsQ0FBTEEsSUFBVUEsS0FBS0EsTUFBTEEsQ0FBbERBOztvQ0FFS0EsR0FBR0EsVUFBSEEsQ0FBY0EsVUFBZEE7Ozs7O3NDQUFpQ0EsSUFBSUEsS0FBSkEsQ0FBYUEsNkNBQWJBOzs7OENBRzdCQTs7dUNBQWlCQSxJQUFJQSxRQUFKQSxDQUFhQSxVQUFiQSxFQUF5QkEsT0FBekJBOzs7O0FBQTFCQSxxREFBY0E7O0FBRWRBLHFDQUFLQSxRQUFMQSxHQUFnQkEsRUFBaEJBO0FBQ0FBLG9DQUFJQSxPQUFPQSxZQUFQQSxJQUF1QkEsT0FBT0EsWUFBUEEsQ0FBb0JBLE9BQXBCQSxLQUFnQ0EsS0FBaENBLEVBQXVDQTtBQUM1REEsc0RBQWtCQSxRQUFRQSx5QkFBUkEsRUFBbUNBLE9BQW5DQSxDQUQwQ0E7O0FBRWhFQSx5Q0FBS0EsUUFBTEEsQ0FBY0EsSUFBZEEsQ0FBbUJBLElBQUlBLGVBQUpBLENBQW9CQSxPQUFPQSxZQUFQQSxDQUF2Q0EsRUFGZ0VBO2lDQUFsRUE7QUFJQUEsb0NBQUlBLE9BQU9BLElBQVBBLEVBQWFBO0FBQ1hBLDZDQUFTQSxRQUFRQSxnQkFBUkEsRUFBMEJBLE9BQTFCQSxDQURFQTs7QUFFZkEseUNBQUtBLFFBQUxBLENBQWNBLElBQWRBLENBQW1CQSxJQUFJQSxNQUFKQSxDQUFXQSxPQUFPQSxJQUFQQSxDQUE5QkEsRUFGZUE7aUNBQWpCQTtBQUlBQSxvQ0FBSUEsT0FBT0EsT0FBUEEsSUFBa0JBLE9BQU9BLE9BQVBBLENBQWVBLE9BQWZBLEtBQTJCQSxLQUEzQkEsRUFBa0NBO0FBQ2xEQSw4Q0FBVUEsUUFBUUEsaUJBQVJBLEVBQTJCQSxPQUEzQkEsQ0FEd0NBOztBQUV0REEseUNBQUtBLFFBQUxBLENBQWNBLElBQWRBLENBQW1CQSxJQUFJQSxPQUFKQSxDQUFZQSxPQUFPQSxPQUFQQSxDQUEvQkEsRUFGc0RBO2lDQUF4REE7QUFLSUE7Ozs7OzRDQUNZQSxLQUFLQSxRQUFMQTs7Ozs7Ozs7QUFBUEE7O3VDQUNRQSxJQUFJQSxJQUFKQSxDQUFTQSxJQUFUQTs7O0FBQWZBOztzQ0FDSUEsVUFBVUEsT0FBT0EsVUFBUEE7Ozs7O3NDQUF5QkEsSUFBSUEsS0FBSkEsQ0FBVUEsbUJBQVZBOzs7cUNBQ25DQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7b0NBR0RBOzs7Ozs7dUNBQWNBLEtBQUtBLFFBQUxBOzs7Ozs7OzthQzdDeUIsQ0FBakMsQ0FBUCxDRGNXRDs7OzttQ0FxQ0ZBOzs7QUFDYkUsbUJBQU9BLElBQUlBLE9BQUpBLENBQVlBLFlBQUFBO0FBQ2pCQSx1QkFBT0EsSUFBUEEsQ0FBWUEsT0FBT0EsS0FBUEEsRUFBY0EsRUFBQ0EsTUFBS0EsT0FBTEEsRUFBM0JBLEVBQTBDQSxVQUFDQSxHQUFEQSxFQUFNQSxJQUFOQSxFQUFVQTtBQUNsREEsNkJBQUFBLE1BQUFBLENBQU9BLEdBQVBBLENBQVdBLElBQVhBLEVBRGtEQTtBQUVsREEsNkJBQUFBLE1BQUFBLENBQU9BLEdBQVBBLENBQWNBLElBQUlBLElBQUpBLFVBQWFBLElBQUlBLE9BQUpBLFFBQTNCQSxFQUZrREE7QUFHbERBLDZCQUFBQSxNQUFBQSxDQUFPQSxHQUFQQSxjQUhrREE7QUFLbERBLHdCQUFJQSxTQUFTQSxFQUFUQSxDQUw4Q0E7Ozs7OztBQU1sREEsOENBQWdCQSxNQUFLQSxRQUFMQSwyQkFBaEJBLHdHQUErQkE7Z0NBQXRCQSxvQkFBc0JBOztBQUM3QkEsZ0NBQUlBLENBQUNBLEtBQUlBLFFBQUpBLEVBQWNBLE1BQW5CQTtrRUFENkJBOzs7OztBQUU3QkEsc0RBQTBCQSxPQUFPQSxPQUFQQSxDQUFlQSxLQUFJQSxRQUFKQSw0QkFBekNBLHdHQUF3REE7Ozt3Q0FBOUNBLHNCQUE4Q0E7d0NBQXpDQSx5QkFBeUNBOztBQUN0REEsb0RBQWNBLGNBQVNBLE9BQU9BLFdBQVBBLE9BQXZCQSxDQURzREE7aUNBQXhEQTs7Ozs7Ozs7Ozs7Ozs7NkJBRjZCQTt5QkFBL0JBOzs7Ozs7Ozs7Ozs7OztxQkFOa0RBOztBQWFsREEsNkJBQUFBLE1BQUFBLENBQU9BLEdBQVBBLENBQVdBLE1BQVhBLEVBYmtEQTtBQWNsREEsMkJBZGtEQTtpQkFBVkEsQ0FBMUNBLENBRGlCQTthQUFBQSxDQUFuQkEsQ0FEYUY7Ozs7V0E5Q2pCOzs7QUFBYSxRQUFBLFVBQUEsR0FBVSxVQUFWO0FBbUVaO0FBRUQsU0FBQSxJQUFBLEdBQUE7QUNuQkksV0FBTyxVQUFVLElBQVYsRUFBZ0IsS0FBSyxDQUFMLEVBQVEsT0FBeEIsMEJBQWlDO1lEc0JwQ0c7Ozs7Ozs7Z0NBQU1BLElBQUlBLFVBQUpBOzsrQkFDSkEsTUFBSUEsSUFBSkE7Ozs7Ozs7Ozs7QUFHTkEsaUNBQUFBLE1BQUFBLENBQU9BLEtBQVBBLENBQWFBLGFBQUlBLEtBQUpBLENBQWJBOzs7OztBQUVBQSxnQ0FBUUEsSUFBUkEsQ0FBYUEsQ0FBYkE7Ozs7Ozs7OztLQzVCd0MsQ0FBakMsQ0FBUCxDRG1CSjtDQUFBO0FBYUEiLCJmaWxlIjoiY2xpLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLy8vIDxyZWZlcmVuY2UgcGF0aD0nLi4vdHlwaW5ncy5kLnRzJyAvPlxuLy8vIDxyZWZlcmVuY2UgcGF0aD0nLi4vdHlwaW5ncy90c2QuZC50cycgLz5cbid1c2Ugc3RyaWN0JztcblxuLy9pbXBvcnQgJy4vbGliL2VzMjAxeCc7XG5pbXBvcnQgKiBhcyBtaW5pbWlzdCBmcm9tICdtaW5pbWlzdCc7XG5pbXBvcnQge2xvZ2dlcn0gZnJvbSAnLi9saWIvTG9nZ2VyJztcbmltcG9ydCAqIGFzIGZpZ2xldCBmcm9tICdmaWdsZXQnO1xuaW1wb3J0ICogYXMgZnNwIGZyb20gJ2ZzLXByb21pc2UnO1xuaW1wb3J0ICogYXMgZnMgZnJvbSAnZnMnO1xuXG4vLyBwYXJzZSBjbGkgYXJndW1lbnRzIHVzaW5nIG1pbmltaXN0XG5sZXQgYXJndjphbnkgPSBtaW5pbWlzdChwcm9jZXNzLmFyZ3Yuc2xpY2UoMikpO1xuXG4vLyBwcm9kIGZsYWcgKGZvcmNlIHByb2R1Y3Rpb24gZW52aXJvbm1lbnQgd2l0aCAtLXByb2QpXG5pZiAoYXJndi5wcm9kKSBwcm9jZXNzLmVudi5OT0RFX0VOViA9ICdwcm9kdWN0aW9uJztcblxubGV0IHBhY2thZ2VGaWxlID0gJ3BhY2thZ2UuanNvbic7XG5pZiAoIWZzLmV4aXN0c1N5bmMocGFja2FnZUZpbGUpKSB7XG4gIGxvZ2dlci5lcnJvcigncGFja2FnZS5qc29uIG5vdCBmb3VuZCBpbiB0aGlzIGRpcmVjdG9yeSAhJyk7XG4gIHByb2Nlc3MuZXhpdCgwKTtcbn1cblxuLy9jb25zdCBwa2cgPSByZXF1aXJlKCcuLi9wYWNrYWdlLmpzb24nKTtcbmNvbnN0IHBrZyA9IEpTT04ucGFyc2UoZnMucmVhZEZpbGVTeW5jKHBhY2thZ2VGaWxlLCAndXRmLTgnKSk7XG5sZXQgY29uZmlnO1xuXG4vKipcbiAqIEFwcGxpY2F0aW9uIENMSVxuICogcHJvdmlkZXMgY29tbW9uIGNvbW1hbmRzIHVzYWJsZSBieSBhbGwgcHJvamVjdHMuXG4gKlxuICogZGVwZW5kZW5jeSBtYW5hZ2VtZW50OiBucG0sIGJvd2VyLCBqc3BtIGFuZCB0c2RcbiAqIGxpbnRpbmc6IHRzbGludCBqc2NzIGh0bWxoaW50IHNhc3MtbGludFxuICogdGVzdGluZzoga2FybWEsIHByb3RyYWN0b3JcbiAqIHBtMjogc3RhcnQvc3RvcCBwcm9jZXNzZXNcbiAqL1xuZXhwb3J0IGNsYXNzIFByb2plY3RDTEkge1xuXG4gIHB1YmxpYyBjb21tYW5kczpBcnJheTxhbnk+O1xuXG4gIC8qKlxuICAgKiBQYXJzZSBjbGkgYXJndW1lbnRzIGFuZCBleGVjdXRlIGJ5IGNvbW1hbmRcbiAgICpcbiAgICogQHJldHVybiB7W3R5cGVdfSBbZGVzY3JpcHRpb25dXG4gICAqL1xuICBwdWJsaWMgYXN5bmMgZXhlYygpIHtcblxuICAgIGxldCBjb25maWdGaWxlID0gJ3Byb2plY3QuanNvbic7XG4gICAgaWYgKGFyZ3YuYyB8fCBhcmd2LmNvbmZpZykgY29uZmlnRmlsZSA9IGFyZ3YuYyB8fCBhcmd2LmNvbmZpZztcblxuICAgIGlmICghZnMuZXhpc3RzU3luYyhjb25maWdGaWxlKSkgdGhyb3cgbmV3IEVycm9yKGAke2NvbmZpZ0ZpbGV9IG5vdCBmb3VuZCBpbiB0aGlzIGRpcmVjdG9yeSAhYCk7XG5cbiAgICAvL2NvbmZpZyA9IHJlcXVpcmUoJy4uLycgKyBjb25maWdGaWxlKTtcbiAgICBjb25maWcgPSBKU09OLnBhcnNlKGF3YWl0IGZzcC5yZWFkRmlsZShjb25maWdGaWxlLCAndXRmLTgnKSk7XG5cbiAgICB0aGlzLmNvbW1hbmRzID0gW107XG4gICAgaWYgKGNvbmZpZy5kZXBlbmRlbmNpZXMgJiYgY29uZmlnLmRlcGVuZGVuY2llcy5lbmFibGVkICE9PSBmYWxzZSkge1xuICAgICAgbGV0IERlcGVuZGVuY2llc0NMSSA9IHJlcXVpcmUoJy4vY29tbWFuZHMvZGVwZW5kZW5jaWVzJykuZGVmYXVsdDtcbiAgICAgIHRoaXMuY29tbWFuZHMucHVzaChuZXcgRGVwZW5kZW5jaWVzQ0xJKGNvbmZpZy5kZXBlbmRlbmNpZXMpKTtcbiAgICB9XG4gICAgaWYgKGNvbmZpZy5hcHBzKSB7XG4gICAgICBsZXQgUE0yQ0xJID0gcmVxdWlyZSgnLi9jb21tYW5kcy9wbTInKS5kZWZhdWx0O1xuICAgICAgdGhpcy5jb21tYW5kcy5wdXNoKG5ldyBQTTJDTEkoY29uZmlnLmFwcHMpKTtcbiAgICB9XG4gICAgaWYgKGNvbmZpZy5saW50ZXJzICYmIGNvbmZpZy5saW50ZXJzLmVuYWJsZWQgIT09IGZhbHNlKSB7XG4gICAgICBsZXQgTGludENMSSA9IHJlcXVpcmUoJy4vY29tbWFuZHMvbGludCcpLmRlZmF1bHQ7XG4gICAgICB0aGlzLmNvbW1hbmRzLnB1c2gobmV3IExpbnRDTEkoY29uZmlnLmxpbnRlcnMpKTtcbiAgICB9XG5cbiAgICBsZXQgcmVzdWx0O1xuICAgIGZvciAobGV0IGNsaSBvZiB0aGlzLmNvbW1hbmRzKSB7XG4gICAgICByZXN1bHQgPSBhd2FpdCBjbGkuZXhlYyhhcmd2KTtcbiAgICAgIGlmIChyZXN1bHQgJiYgcmVzdWx0LmVycm9yQ291bnQpIHRocm93IG5ldyBFcnJvcignTGludCBlcnJvcnMgZm91bmQnKTtcbiAgICAgIGlmIChyZXN1bHQpIGJyZWFrO1xuICAgIH1cblxuICAgIGlmICghcmVzdWx0KSBhd2FpdCB0aGlzLnNob3dIZWxwKCk7XG4gIH1cblxuICAvKipcbiAgICogU2hvdyBoZWxwXG4gICAqL1xuICBwdWJsaWMgc2hvd0hlbHAoKSB7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKCgpID0+IHtcbiAgICAgIGZpZ2xldC50ZXh0KGNvbmZpZy50aXRsZSwge2ZvbnQ6J1NsYW50J30sIChlcnIsIGRhdGEpID0+IHtcbiAgICAgICAgbG9nZ2VyLmxvZyhkYXRhKTtcbiAgICAgICAgbG9nZ2VyLmxvZyhgJHtwa2cubmFtZX0gKCR7cGtnLnZlcnNpb259KVxcbmApO1xuICAgICAgICBsb2dnZXIubG9nKGBDb21tYW5kczpgKTtcblxuICAgICAgICBsZXQgb3V0cHV0ID0gJyc7XG4gICAgICAgIGZvciAobGV0IGNsaSBvZiB0aGlzLmNvbW1hbmRzKSB7XG4gICAgICAgICAgaWYgKCFjbGkuY29tbWFuZHMpIGJyZWFrO1xuICAgICAgICAgIGZvciAobGV0IFtrZXksIGNtZE9ial0gb2YgT2JqZWN0LmVudHJpZXMoY2xpLmNvbW1hbmRzKSkge1xuICAgICAgICAgICAgb3V0cHV0ICs9IGAgJHtrZXl9IC0gJHtjbWRPYmouZGVzY3JpcHRpb259XFxuYDtcbiAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBsb2dnZXIubG9nKG91dHB1dCk7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH0pO1xuICAgIH0pO1xuICB9XG5cbn07XG5cbmFzeW5jIGZ1bmN0aW9uIGluaXQoKSB7XG4gIHRyeSB7XG4gICAgLy8gY3JlYXRlIGluc3RhbmNlIGFuZCBleGVjdXRlXG4gICAgbGV0IGNsaSA9IG5ldyBQcm9qZWN0Q0xJKCk7XG4gICAgYXdhaXQgY2xpLmV4ZWMoKTtcbiAgfWNhdGNoIChlcnIpIHtcbiAgICAvL2xvZ2dlci5mYXRhbChlcnIpO1xuICAgIGxvZ2dlci5mYXRhbChlcnIuc3RhY2spO1xuICB9ZmluYWxseXtcbiAgICBwcm9jZXNzLmV4aXQoMCk7XG4gIH1cbn1cblxuaW5pdCgpO1xuIiwiJ3VzZSBzdHJpY3QnO1xudmFyIF9fYXdhaXRlciA9ICh0aGlzICYmIHRoaXMuX19hd2FpdGVyKSB8fCBmdW5jdGlvbiAodGhpc0FyZywgX2FyZ3VtZW50cywgUHJvbWlzZSwgZ2VuZXJhdG9yKSB7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKGZ1bmN0aW9uIChyZXNvbHZlLCByZWplY3QpIHtcbiAgICAgICAgZ2VuZXJhdG9yID0gZ2VuZXJhdG9yLmNhbGwodGhpc0FyZywgX2FyZ3VtZW50cyk7XG4gICAgICAgIGZ1bmN0aW9uIGNhc3QodmFsdWUpIHsgcmV0dXJuIHZhbHVlIGluc3RhbmNlb2YgUHJvbWlzZSAmJiB2YWx1ZS5jb25zdHJ1Y3RvciA9PT0gUHJvbWlzZSA/IHZhbHVlIDogbmV3IFByb21pc2UoZnVuY3Rpb24gKHJlc29sdmUpIHsgcmVzb2x2ZSh2YWx1ZSk7IH0pOyB9XG4gICAgICAgIGZ1bmN0aW9uIG9uZnVsZmlsbCh2YWx1ZSkgeyB0cnkgeyBzdGVwKFwibmV4dFwiLCB2YWx1ZSk7IH0gY2F0Y2ggKGUpIHsgcmVqZWN0KGUpOyB9IH1cbiAgICAgICAgZnVuY3Rpb24gb25yZWplY3QodmFsdWUpIHsgdHJ5IHsgc3RlcChcInRocm93XCIsIHZhbHVlKTsgfSBjYXRjaCAoZSkgeyByZWplY3QoZSk7IH0gfVxuICAgICAgICBmdW5jdGlvbiBzdGVwKHZlcmIsIHZhbHVlKSB7XG4gICAgICAgICAgICB2YXIgcmVzdWx0ID0gZ2VuZXJhdG9yW3ZlcmJdKHZhbHVlKTtcbiAgICAgICAgICAgIHJlc3VsdC5kb25lID8gcmVzb2x2ZShyZXN1bHQudmFsdWUpIDogY2FzdChyZXN1bHQudmFsdWUpLnRoZW4ob25mdWxmaWxsLCBvbnJlamVjdCk7XG4gICAgICAgIH1cbiAgICAgICAgc3RlcChcIm5leHRcIiwgdm9pZCAwKTtcbiAgICB9KTtcbn07XG52YXIgbWluaW1pc3QgPSByZXF1aXJlKCdtaW5pbWlzdCcpO1xudmFyIExvZ2dlcl8xID0gcmVxdWlyZSgnLi9saWIvTG9nZ2VyJyk7XG52YXIgZmlnbGV0ID0gcmVxdWlyZSgnZmlnbGV0Jyk7XG52YXIgZnNwID0gcmVxdWlyZSgnZnMtcHJvbWlzZScpO1xudmFyIGZzID0gcmVxdWlyZSgnZnMnKTtcbmxldCBhcmd2ID0gbWluaW1pc3QocHJvY2Vzcy5hcmd2LnNsaWNlKDIpKTtcbmlmIChhcmd2LnByb2QpXG4gICAgcHJvY2Vzcy5lbnYuTk9ERV9FTlYgPSAncHJvZHVjdGlvbic7XG5sZXQgcGFja2FnZUZpbGUgPSAncGFja2FnZS5qc29uJztcbmlmICghZnMuZXhpc3RzU3luYyhwYWNrYWdlRmlsZSkpIHtcbiAgICBMb2dnZXJfMS5sb2dnZXIuZXJyb3IoJ3BhY2thZ2UuanNvbiBub3QgZm91bmQgaW4gdGhpcyBkaXJlY3RvcnkgIScpO1xuICAgIHByb2Nlc3MuZXhpdCgwKTtcbn1cbmNvbnN0IHBrZyA9IEpTT04ucGFyc2UoZnMucmVhZEZpbGVTeW5jKHBhY2thZ2VGaWxlLCAndXRmLTgnKSk7XG5sZXQgY29uZmlnO1xuY2xhc3MgUHJvamVjdENMSSB7XG4gICAgZXhlYygpIHtcbiAgICAgICAgcmV0dXJuIF9fYXdhaXRlcih0aGlzLCB2b2lkIDAsIFByb21pc2UsIGZ1bmN0aW9uKiAoKSB7XG4gICAgICAgICAgICBsZXQgY29uZmlnRmlsZSA9ICdwcm9qZWN0Lmpzb24nO1xuICAgICAgICAgICAgaWYgKGFyZ3YuYyB8fCBhcmd2LmNvbmZpZylcbiAgICAgICAgICAgICAgICBjb25maWdGaWxlID0gYXJndi5jIHx8IGFyZ3YuY29uZmlnO1xuICAgICAgICAgICAgaWYgKCFmcy5leGlzdHNTeW5jKGNvbmZpZ0ZpbGUpKVxuICAgICAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihgJHtjb25maWdGaWxlfSBub3QgZm91bmQgaW4gdGhpcyBkaXJlY3RvcnkgIWApO1xuICAgICAgICAgICAgY29uZmlnID0gSlNPTi5wYXJzZSh5aWVsZCBmc3AucmVhZEZpbGUoY29uZmlnRmlsZSwgJ3V0Zi04JykpO1xuICAgICAgICAgICAgdGhpcy5jb21tYW5kcyA9IFtdO1xuICAgICAgICAgICAgaWYgKGNvbmZpZy5kZXBlbmRlbmNpZXMgJiYgY29uZmlnLmRlcGVuZGVuY2llcy5lbmFibGVkICE9PSBmYWxzZSkge1xuICAgICAgICAgICAgICAgIGxldCBEZXBlbmRlbmNpZXNDTEkgPSByZXF1aXJlKCcuL2NvbW1hbmRzL2RlcGVuZGVuY2llcycpLmRlZmF1bHQ7XG4gICAgICAgICAgICAgICAgdGhpcy5jb21tYW5kcy5wdXNoKG5ldyBEZXBlbmRlbmNpZXNDTEkoY29uZmlnLmRlcGVuZGVuY2llcykpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKGNvbmZpZy5hcHBzKSB7XG4gICAgICAgICAgICAgICAgbGV0IFBNMkNMSSA9IHJlcXVpcmUoJy4vY29tbWFuZHMvcG0yJykuZGVmYXVsdDtcbiAgICAgICAgICAgICAgICB0aGlzLmNvbW1hbmRzLnB1c2gobmV3IFBNMkNMSShjb25maWcuYXBwcykpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKGNvbmZpZy5saW50ZXJzICYmIGNvbmZpZy5saW50ZXJzLmVuYWJsZWQgIT09IGZhbHNlKSB7XG4gICAgICAgICAgICAgICAgbGV0IExpbnRDTEkgPSByZXF1aXJlKCcuL2NvbW1hbmRzL2xpbnQnKS5kZWZhdWx0O1xuICAgICAgICAgICAgICAgIHRoaXMuY29tbWFuZHMucHVzaChuZXcgTGludENMSShjb25maWcubGludGVycykpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgbGV0IHJlc3VsdDtcbiAgICAgICAgICAgIGZvciAobGV0IGNsaSBvZiB0aGlzLmNvbW1hbmRzKSB7XG4gICAgICAgICAgICAgICAgcmVzdWx0ID0geWllbGQgY2xpLmV4ZWMoYXJndik7XG4gICAgICAgICAgICAgICAgaWYgKHJlc3VsdCAmJiByZXN1bHQuZXJyb3JDb3VudClcbiAgICAgICAgICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdMaW50IGVycm9ycyBmb3VuZCcpO1xuICAgICAgICAgICAgICAgIGlmIChyZXN1bHQpXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKCFyZXN1bHQpXG4gICAgICAgICAgICAgICAgeWllbGQgdGhpcy5zaG93SGVscCgpO1xuICAgICAgICB9KTtcbiAgICB9XG4gICAgc2hvd0hlbHAoKSB7XG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZSgoKSA9PiB7XG4gICAgICAgICAgICBmaWdsZXQudGV4dChjb25maWcudGl0bGUsIHsgZm9udDogJ1NsYW50JyB9LCAoZXJyLCBkYXRhKSA9PiB7XG4gICAgICAgICAgICAgICAgTG9nZ2VyXzEubG9nZ2VyLmxvZyhkYXRhKTtcbiAgICAgICAgICAgICAgICBMb2dnZXJfMS5sb2dnZXIubG9nKGAke3BrZy5uYW1lfSAoJHtwa2cudmVyc2lvbn0pXFxuYCk7XG4gICAgICAgICAgICAgICAgTG9nZ2VyXzEubG9nZ2VyLmxvZyhgQ29tbWFuZHM6YCk7XG4gICAgICAgICAgICAgICAgbGV0IG91dHB1dCA9ICcnO1xuICAgICAgICAgICAgICAgIGZvciAobGV0IGNsaSBvZiB0aGlzLmNvbW1hbmRzKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmICghY2xpLmNvbW1hbmRzKVxuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgICAgIGZvciAobGV0IFtrZXksIGNtZE9ial0gb2YgT2JqZWN0LmVudHJpZXMoY2xpLmNvbW1hbmRzKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgb3V0cHV0ICs9IGAgJHtrZXl9IC0gJHtjbWRPYmouZGVzY3JpcHRpb259XFxuYDtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBMb2dnZXJfMS5sb2dnZXIubG9nKG91dHB1dCk7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgIH1cbn1cbmV4cG9ydHMuUHJvamVjdENMSSA9IFByb2plY3RDTEk7XG47XG5mdW5jdGlvbiBpbml0KCkge1xuICAgIHJldHVybiBfX2F3YWl0ZXIodGhpcywgdm9pZCAwLCBQcm9taXNlLCBmdW5jdGlvbiogKCkge1xuICAgICAgICB0cnkge1xuICAgICAgICAgICAgbGV0IGNsaSA9IG5ldyBQcm9qZWN0Q0xJKCk7XG4gICAgICAgICAgICB5aWVsZCBjbGkuZXhlYygpO1xuICAgICAgICB9XG4gICAgICAgIGNhdGNoIChlcnIpIHtcbiAgICAgICAgICAgIExvZ2dlcl8xLmxvZ2dlci5mYXRhbChlcnIuc3RhY2spO1xuICAgICAgICB9XG4gICAgICAgIGZpbmFsbHkge1xuICAgICAgICAgICAgcHJvY2Vzcy5leGl0KDApO1xuICAgICAgICB9XG4gICAgfSk7XG59XG5pbml0KCk7XG4iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
