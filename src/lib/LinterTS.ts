//import * as fsp from 'fs-promise';
import * as fs from 'fs';

import {logger} from '../lib/Logger';
// import shell_exec from '../lib/exec';
import * as glob from 'glob';
import * as tslint from 'tslint';

interface ITSlinter {
  lint(globs:Array<string>):Promise<any>;
  lintFile(file:string, verbose:boolean):Promise<any>;
}

/**
 * Linter CLI
 */
class TSLinter implements ITSlinter {

  constructor(private config:any = {}) {

  }

  /**
   * Run TSlint on all ts files mathing glob patterns
   */
  public async lint(globs:Array<string>):Promise<any> {
    let results:any = {
      failureCount: 0,
      failures: [],
      errors: []
    };
    // console.log('globs', globs);

    try {
      for ( let pattern of globs) {
        // logger.debug(`pattern: ${pattern}`);
        let files = await glob.sync(pattern);
        // logger.debug(`files: ${files}`);
        for ( let file of files) {
          // logger.debug(`file: ${file}`);
          let result = this.lintFile(file);
          // console.log('result', result);
          if (!result.result.failureCount) continue;
          results.failureCount += result.result.failureCount;
          results.failures.push({
            file,
            failures: result.result.failures
          });
          results.errors = results.errors.concat(result.errors);
        }
        // console.log('---');
      }

      //logger.warn(results.errors.join('\n'));

    }catch (err) {
      logger.error(err);
      throw err;
    }

    //logger.info(`TSLINT results ${JSON.stringify(results)}`);
    return results;
  }

  public lintFile(file:string):any {

    let errors = [];

    let options:any = {
      formatter: 'json',
      configuration: this.config,
      // rulesDirectory: 'customRules/', // can be an array of directories
      // formattersDirectory: 'customFormatters/'
    };

    // logger.debug(`linting TS: ${file}`);
    let result;

    //let contents = await fsp.readFile(file, {encoding: 'utf8'});
    let contents = fs.readFileSync(file, 'utf8');
    //console.log('contents', contents);
    let ll = new tslint(file, contents, options);
    result = ll.lint();
    for ( let err of result.failures) {
      //console.log('err', err);
      let pos = `${err.startPosition.lineAndCharacter.line}:${err.startPosition.lineAndCharacter.character}`;
      errors.push(`${err.fileName} (${pos}): ${err.failure} (${err.ruleName})`);
    }

    return {result, errors};
  }

}

export default TSLinter;
