import * as childProcessPromise from 'child-process-promise';
import {logger} from './Logger';

const shellExec:any = childProcessPromise.exec;

// 1 mb buffer for piping to streams to stdout and stderr
const maxBuffer = 1024 * 5000;

/**
 * Execute a shell command and pipe all stdout and sterr outputs.
 *
 * @param  {string}       cmd     command to execute
 * @param  {any={}}       options child process options
 * @return {Promise<any>}         resolves when the process exists
 */
async function exec(cmd:string, options:any = {}):Promise<any> {
  return new Promise(async (resolve, reject) => {
    let allOutput:any = '';

    shellExec(cmd, Object.assign({
      maxBuffer: maxBuffer
    }, options))
      .progress((childProcess) => {
        childProcess.stdout.on('data', (data) => {
          let str = data;
          if (!options.silent) logger.debug(str);
          allOutput += str;
        });
        if (!options.noErrors)
        childProcess.stderr.on('data', (data) => {
          logger.error(data);
        });
      })
      .then((childProcess) => {
        resolve({output: childProcess.stdout.toString(), childProcess});
        //console.log('[spawn] stdout: ', childProcess.stdout.toString());
      })
      .fail((err) => {
        //logger.error('FAIL :' + err.toString('utf8'));
        reject({error:err.toString('utf8'), output:allOutput});
      });
  });
}

export async function exec2(cmd:string, args:Array<any> = [],  options:any = {}):Promise<any> {
  return new Promise((resolve, reject) => {
    try {
      let spawn = require('child_process').spawn;
      spawn(cmd, args, { stdio: 'inherit' })
      .on('exit', function(code){
        resolve(code);
      })
      .on('error', function(err){
        reject(err);
      });
    }catch (err) {
      reject(err);
    }
  });
}



export default exec;
