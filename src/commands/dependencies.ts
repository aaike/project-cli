import * as fsp from 'fs-promise';
import {logger} from '../lib/Logger';
import shell_exec from '../lib/exec';

class DependenciesCLI implements ICommandLine {

  public commands = {
    install: {
      description: 'install all dependencies'
    },
    update: {
      description: 'update all dependencies'
    }
  };

  constructor(private config:any) {

  }

  public async exec(argv:any):Promise<any> {
    let cmd = argv._[0];
    if (!this.commands[cmd]) return false;

    switch (cmd) {
      case 'install':
        await this.installAllDependencies(this.config.installPaths);
        return true;
      case 'update':
        await this.installAllDependencies(this.config.installPaths, true);
        return true;
      default:
        break;
    }
    return false;
  }

  /**
   * Install all dependencies for this project and make sure
   * an initial build is done to make assets available.
   */
  public async installAllDependencies(paths:Array<string>, update:boolean = false):Promise<any> {
    // install dependencies in all dirs
    for (let path of paths) {
        await this.installDependencies(path, update);
    }
    if (update) {
      logger.info('DEPENDENCIES UPDATED');
    }else {
      logger.info('DEPENDENCIES INSTALLED');
    }
  }

  /**
   * Install all dependencies in a directory.
   *
   * Use the --update flag to update the dependencies
   *
   * @param  {string}       cwd     Directory to perform installations in
   * @param  {boolean}      update  install dependencies even if the directories already exist
   * @return {Promise<any>}         Resolves when all dependencies have been installed
   */
  public async installDependencies(cwd:string = '.', update:boolean = false):Promise<any> {

    // check for npm, install if there is a `package.json` but no `node_modules` directory yet
    if (await fsp.exists(`${cwd}/package.json`) ) {
      if (!(await fsp.exists(`${cwd}/node_modules`)) || update) {
        logger.info(`NPM Install: ${cwd}`);
        await shell_exec('npm install', {cwd});
      }else {
        console.log('npm modules already installed');
      }
    }
    // check for bower, install if there is a `bower.json` but no `bower_components` directory yet
    if (await fsp.exists(`${cwd}/bower.json`)) {
      if (update || !(await fsp.exists(`${cwd}/bower_components`))) {
        logger.info(`Bower Install: ${cwd}`);
        await shell_exec('bower install', {cwd});
      }else {
        console.log('bower components already installed');
      }
    }
    // check for jspm, install if there is a jspm section in the `package.json` but no `jspm_packages` directory yet
    if (await fsp.exists(`${cwd}/package.json`)) {
      let pkg = JSON.parse(await fsp.readFile(`${cwd}/package.json`, 'utf-8'));
      if (pkg.jspm) {
        if (update || !(await fsp.exists(`${cwd}/jspm_packages`))) {
          if (pkg.jspm) {
            logger.info(`JSPM Install: ${cwd}`);
            await shell_exec('jspm install -y', {cwd});
            await shell_exec('jspm dl-loader -y', {cwd});
          }
        }else {
          console.log('jspm packages already installed');
        }
      }
    }
    // check for tsd, install if there is a `tsd.json` but no `typings` directory yet
    if (await fsp.exists(`${cwd}/tsd.json`)) {
      let tsdConfig = JSON.parse(await fsp.readFile(`${cwd}/tsd.json`, 'utf-8'));
      if (Object.keys(tsdConfig.installed).length > 0) {
        if (update || !(await fsp.exists(`${cwd}/typings`))) {
          logger.info(`TSD Install: ${cwd}`);
          await shell_exec('tsd install', {cwd});
        }else {
          logger.log('tsd typings already installed');
        }
      }else {
        logger.log('no TSD typings found in tsd.json');
      }
    }
  }

}

export default DependenciesCLI;
