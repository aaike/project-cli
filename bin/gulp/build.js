import gulp from 'gulp';
import typescript from 'gulp-typescript';
import tsc from 'typescript';
import babel from 'gulp-babel';
import del from 'del';
import vinylPaths from 'vinyl-paths';
import runSequence from 'run-sequence';
import plumber from 'gulp-plumber';
import changed from 'gulp-changed';
import sourcemaps from 'gulp-sourcemaps';
import fs from 'fs';
import {paths, patterns} from '../config';

var tsProject = typescript.createProject('./tsconfig.json', { typescript: tsc });

//let config = JSON.parse(fs.readFileSync('tsconfig.json', 'utf-8'));
let babelConfig = JSON.parse(fs.readFileSync('.babelrc', 'utf-8'));

gulp.task('build-app', 'compile application files', () => {
  gulp.src(patterns.source)
    .pipe(sourcemaps.init({ loadMaps: true }))
    .pipe(plumber())
    .pipe(changed(paths.output, { extension: '.js' }))
    .pipe(typescript(tsProject))
    .pipe(babel(babelConfig))
    .pipe(plumber.stop())
    .pipe(sourcemaps.write({ includeContent: true }))
    .pipe(gulp.dest(paths.output));
});

gulp.task('clean', 'clean the dist directory', () => {
  return gulp.src(paths.output)
    .pipe(vinylPaths(del));
});

gulp.task('build', (cb) => {
  return runSequence(
    'clean',
    ['build-app'],
    cb
  );
});
