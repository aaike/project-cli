'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var chalk = require('chalk');

var Logger = function () {
    function Logger() {
        var level = arguments.length <= 0 || arguments[0] === undefined ? 10 : arguments[0];

        _classCallCheck(this, Logger);

        this.level = level;
    }

    _createClass(Logger, [{
        key: 'log',
        value: function log(msg) {
            console.log(msg);
        }
    }, {
        key: 'debug',
        value: function debug(msg) {
            if (this.level >= Logger.DEBUG) this.log(msg);
        }
    }, {
        key: 'info',
        value: function info(msg) {
            if (this.level >= Logger.INFO) this.log(chalk.blue(msg));
        }
    }, {
        key: 'success',
        value: function success(msg) {
            if (this.level >= Logger.INFO) this.log(chalk.green(msg));
        }
    }, {
        key: 'warn',
        value: function warn(msg) {
            if (this.level >= Logger.WARN) this.log(chalk.yellow(msg));
        }
    }, {
        key: 'error',
        value: function error(msg) {
            if (this.level >= Logger.ERROR) this.log(chalk.red(msg));
        }
    }, {
        key: 'fatal',
        value: function fatal(msg) {
            if (this.level >= Logger.ERROR) this.log(chalk.red(msg));
        }
    }]);

    return Logger;
}();

Logger.SILENT = 0;
Logger.DEBUG = 2;
Logger.INFO = 5;
Logger.WARN = 6;
Logger.ERROR = 7;
Logger.FATAL = 10;
exports.logger = new Logger();
exports.default = Logger;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImxpYi9Mb2dnZXIudHMiXSwibmFtZXMiOlsiTG9nZ2VyIiwiTG9nZ2VyLmNvbnN0cnVjdG9yIiwiTG9nZ2VyLmxvZyIsIkxvZ2dlci5kZWJ1ZyIsIkxvZ2dlci5pbmZvIiwiTG9nZ2VyLnN1Y2Nlc3MiLCJMb2dnZXIud2FybiIsIkxvZ2dlci5lcnJvciIsIkxvZ2dlci5mYXRhbCJdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUEsSUFBWSxRQUFLLFFBQU0sT0FBTixDQUFMOztJQU1aO0FBU0VBLGFBVEYsTUFTRUEsR0FBcUNBO1lBQWpCQSw4REFBZUEsa0JBQUVBOzs4QkFUdkMsUUFTdUNBOztBQUFqQkMsYUFBQUEsS0FBQUEsR0FBQUEsS0FBQUEsQ0FBaUJEO0tBQXJDQTs7aUJBVEY7OzRCQWtCYUEsS0FBT0E7QUFDaEJFLG9CQUFRQSxHQUFSQSxDQUFZQSxHQUFaQSxFQURnQkY7Ozs7OEJBU0xBLEtBQVVBO0FBQ3JCRyxnQkFBSUEsS0FBS0EsS0FBTEEsSUFBY0EsT0FBT0EsS0FBUEEsRUFBY0EsS0FBS0EsR0FBTEEsQ0FBU0EsR0FBVEEsRUFBaENBOzs7OzZCQVFVSCxLQUFVQTtBQUNwQkksZ0JBQUlBLEtBQUtBLEtBQUxBLElBQWNBLE9BQU9BLElBQVBBLEVBQWFBLEtBQUtBLEdBQUxBLENBQVNBLE1BQU1BLElBQU5BLENBQVdBLEdBQVhBLENBQVRBLEVBQS9CQTs7OztnQ0FHYUosS0FBVUE7QUFDdkJLLGdCQUFJQSxLQUFLQSxLQUFMQSxJQUFjQSxPQUFPQSxJQUFQQSxFQUFhQSxLQUFLQSxHQUFMQSxDQUFTQSxNQUFNQSxLQUFOQSxDQUFZQSxHQUFaQSxDQUFUQSxFQUEvQkE7Ozs7NkJBUVVMLEtBQVVBO0FBQ3BCTSxnQkFBSUEsS0FBS0EsS0FBTEEsSUFBY0EsT0FBT0EsSUFBUEEsRUFBYUEsS0FBS0EsR0FBTEEsQ0FBU0EsTUFBTUEsTUFBTkEsQ0FBYUEsR0FBYkEsQ0FBVEEsRUFBL0JBOzs7OzhCQVFXTixLQUFVQTtBQUNyQk8sZ0JBQUlBLEtBQUtBLEtBQUxBLElBQWNBLE9BQU9BLEtBQVBBLEVBQWNBLEtBQUtBLEdBQUxBLENBQVNBLE1BQU1BLEdBQU5BLENBQVVBLEdBQVZBLENBQVRBLEVBQWhDQTs7Ozs4QkFRV1AsS0FBVUE7QUFDckJRLGdCQUFJQSxLQUFLQSxLQUFMQSxJQUFjQSxPQUFPQSxLQUFQQSxFQUFjQSxLQUFLQSxHQUFMQSxDQUFTQSxNQUFNQSxHQUFOQSxDQUFVQSxHQUFWQSxDQUFUQSxFQUFoQ0E7Ozs7V0FwRUo7OztBQUVnQixPQUFBLE1BQUEsR0FBUyxDQUFUO0FBQ0EsT0FBQSxLQUFBLEdBQVEsQ0FBUjtBQUNBLE9BQUEsSUFBQSxHQUFPLENBQVA7QUFDQSxPQUFBLElBQUEsR0FBTyxDQUFQO0FBQ0EsT0FBQSxLQUFBLEdBQVEsQ0FBUjtBQUNBLE9BQUEsS0FBQSxHQUFRLEVBQVI7QUFrRUwsUUFBQSxNQUFBLEdBQVMsSUFBSSxNQUFKLEVBQVQ7QUFDWCxRQUFBLE9BQUEsR0FBZSxNQUFmIiwiZmlsZSI6ImxpYi9Mb2dnZXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgKiBhcyBjaGFsayBmcm9tICdjaGFsayc7XG5cbi8qKlxuICogTG9nZ2VyIGNsYXNzXG4gKlxuICovXG5jbGFzcyBMb2dnZXIge1xuXG4gIHB1YmxpYyBzdGF0aWMgU0lMRU5UID0gMDtcbiAgcHVibGljIHN0YXRpYyBERUJVRyA9IDI7XG4gIHB1YmxpYyBzdGF0aWMgSU5GTyA9IDU7XG4gIHB1YmxpYyBzdGF0aWMgV0FSTiA9IDY7XG4gIHB1YmxpYyBzdGF0aWMgRVJST1IgPSA3O1xuICBwdWJsaWMgc3RhdGljIEZBVEFMID0gMTA7XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBsZXZlbDpudW1iZXIgPSAxMCkge1xuXG4gIH1cblxuICAvKipcbiAgICogTG9nIGEgbWVzc2FnZSBvZiBhbnkgdHlwZVxuICAgKlxuICAgKiBAcGFyYW0ge3N0cmluZ30gbXNnIG1lc3NhZ2UgdG8gbG9nXG4gICAqL1xuICBwdWJsaWMgbG9nKG1zZzphbnkpOnZvaWQge1xuICAgIGNvbnNvbGUubG9nKG1zZyk7XG4gIH1cblxuICAvKipcbiAgICogTG9nIGFuIGRlYnVnIG1lc3NhZ2VcbiAgICpcbiAgICogQHBhcmFtIHtzdHJpbmd9IG1zZyBtZXNzYWdlIHRvIGxvZ1xuICAgKi9cbiAgcHVibGljIGRlYnVnKG1zZzpzdHJpbmcpOnZvaWQge1xuICAgIGlmICh0aGlzLmxldmVsID49IExvZ2dlci5ERUJVRykgdGhpcy5sb2cobXNnKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBMb2cgYW4gaW5mbyBtZXNzYWdlXG4gICAqXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBtc2cgbWVzc2FnZSB0byBsb2dcbiAgICovXG4gIHB1YmxpYyBpbmZvKG1zZzpzdHJpbmcpOnZvaWQge1xuICAgIGlmICh0aGlzLmxldmVsID49IExvZ2dlci5JTkZPKSB0aGlzLmxvZyhjaGFsay5ibHVlKG1zZykpO1xuICB9XG5cbiAgcHVibGljIHN1Y2Nlc3MobXNnOnN0cmluZyk6dm9pZCB7XG4gICAgaWYgKHRoaXMubGV2ZWwgPj0gTG9nZ2VyLklORk8pIHRoaXMubG9nKGNoYWxrLmdyZWVuKG1zZykpO1xuICB9XG5cbiAgLyoqXG4gICAqIExvZyBhIHdhcm5pbmdcbiAgICpcbiAgICogQHBhcmFtIHtzdHJpbmd9IG1zZyBtZXNzYWdlIHRvIGxvZ1xuICAgKi9cbiAgcHVibGljIHdhcm4obXNnOnN0cmluZyk6dm9pZCB7XG4gICAgaWYgKHRoaXMubGV2ZWwgPj0gTG9nZ2VyLldBUk4pIHRoaXMubG9nKGNoYWxrLnllbGxvdyhtc2cpKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBMb2cgYW4gZXJyb3JcbiAgICpcbiAgICogQHBhcmFtIHtzdHJpbmd9IG1zZyBtZXNzYWdlIHRvIGxvZ1xuICAgKi9cbiAgcHVibGljIGVycm9yKG1zZzpzdHJpbmcpOnZvaWQge1xuICAgIGlmICh0aGlzLmxldmVsID49IExvZ2dlci5FUlJPUikgdGhpcy5sb2coY2hhbGsucmVkKG1zZykpO1xuICB9XG5cbiAgLyoqXG4gICAqIExvZyBhbiBmYXRhbCBlcnJvclxuICAgKlxuICAgKiBAcGFyYW0ge3N0cmluZ30gbXNnIG1lc3NhZ2UgdG8gbG9nXG4gICAqL1xuICBwdWJsaWMgZmF0YWwobXNnOnN0cmluZyk6dm9pZCB7XG4gICAgaWYgKHRoaXMubGV2ZWwgPj0gTG9nZ2VyLkVSUk9SKSB0aGlzLmxvZyhjaGFsay5yZWQobXNnKSk7XG4gIH1cblxufVxuXG5leHBvcnQgbGV0IGxvZ2dlciA9IG5ldyBMb2dnZXIoKTtcbmV4cG9ydCBkZWZhdWx0IExvZ2dlcjtcbiJdLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==
