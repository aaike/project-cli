import * as chalk from 'chalk';

/**
 * Logger class
 *
 */
export default class Logger {

  public static SILENT = 0
  public static DEBUG = 2;
  public static INFO = 5;
  public static WARN = 6;
  public static ERROR = 7;
  public static FATAL = 10;

  constructor(private level:number = 10): void {

  }

  /**
   * Log a message of any type
   *
   * @param {string} msg message to log
   */
  public log(msg:any):void {
    console.log(mesg);
  }

  /**
   * Log an debug message
   *
   * @param {string} msg message to log
   */
  public debug(msg:string):void {
    if (this.level >= Logger.DEBUG) this.log(msg);
  }

  /**
   * Log an info message
   *
   * @param {string} msg message to log
   */
  public info(msg:string) :void {
    if (this.level >= Logger.INFO) this.log(chalk.blue(msg));
  }

  public success(msg:string):void {
    if (this.level >= Logger.INFO) this.log(chalk.green(msg));
  }

  /**
   * Log a warning
   *
   * @param {string} msg message to log
   */
  public warn(msg:string):void {
    if (this.level >= Logger.WARN) this.log(chalk.yellow(msg))
  }

  /**
   * Log an error
   *
   * @param {string} msg message to log
   */
  public error(msg:string):void {
    if (this.level >= Logger.ERROR) this.log(chalk.red(msg));
  }

  /**
   * Log an fatal error
   *
   * @param {string} msg message to log
   */
  public fatal(msg:string):void {
    if (this.level >= Logger.ERROR) this.log(chalk.red(msg));
  }

}

export let logger = new Logger();
