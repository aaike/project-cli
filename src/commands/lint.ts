import './ICommandLine';
import {logger} from '../lib/Logger';
//import * as path from 'path';
import * as fsp from 'fs-promise';
import * as fs from 'fs';
import shellExec, {exec2 as shellExec2} from '../lib/exec';

class LintCLI implements ICommandLine {

  public commands = {
    lint: {
      description: 'run linters on all source files'
    }
  };

  /**
   * Metadata for supported linters
   *
   * @type {Object}
   */
  private linterPackages = {
    tslint: {
      code: 'TypeScript',
      package: 'project-cli/dist/lib/LinterTS'
    },
    jscs: {
      code: 'JavaScript ES201x'
    },
    sasslint: {
      code: 'SASS/SCSS',
      package: 'sass-lint'
    },
    htmlhint: {
      code: 'HTML',
      package: 'htmlhint'
    }
  };

  constructor(private linters:any) {

  }

  public async exec(argv:any):Promise<any> {
    let cmd = argv._[0];

    if (!this.commands[cmd]) return false;

    switch (cmd) {
      case 'lint':
        let result = await this.lint();
        if (result.errorCount) {
          return true;
        }else {
          logger.success('looking good !');
        }

        return true;
      default:
        return false;
    }
  }

  public async lint() {
    let results = [];

    let errorCount = 0;

    logger.info(`Linting all code`);

    for ( let [packageName, packageObj] of Object.entries(this.linters)) {
      let result;
      let pkg = this.linterPackages[packageName];

      switch (packageName) {
        case 'tslint':
          result = await this.lintTS(packageObj.config, packageObj.paths, pkg.package);
          errorCount += result.errorCount;
          break;
        case 'jscs':
          result = await this.lintJS(packageObj.config, packageObj.paths);
          errorCount += result.errorCount;
          break;
        case 'sasslint':
          result = await this.lintSass(packageObj.config, packageObj.paths);
          errorCount += result.errorCount;
          errorCount += result.warningCount;
          break;
        case 'htmlhint':
          result = await this.lintHtml(packageObj.config, packageObj.paths);
          errorCount += result.errorCount;
          break;
        default:
          throw new Error(`linter '${packageName}' not supported`);
      }
      results[packageName] = result;
    }

    return {errorCount, results};
  }

  public async lintTS(configFile:string, globs:Array<string>, modulePath:string):Promise<any> {

    let cfg = {};
    if (fs.existsSync(configFile)) {
      cfg = JSON.parse(await fsp.readFile(configFile, 'utf8'));
    }

    let TSLinter = require(modulePath).default;
    let linter = new TSLinter(cfg);
    let result = await linter.lint(globs);
    if (result.failureCount) {
      logger.error(`✗ TypeScript: ${result.failureCount} errors`);
      logger.warn(result.errors.join('\n'));
    }else {
      logger.success(`✓ TypeScript`);
    }
    result.errorCount = result.failureCount;

    return result;
  }

  public async lintJS(configFile:String, globs:Array<string>) {
    let results;
    let result;
    let errorCount = 0;
    for (let globPattern of globs) {
      let lintOutputs = [];

      try {
        result = await shellExec(`jscs -c ${configFile} ${globPattern} --reporter json`, {
          noErrors: true,
          silent: true
        });
      }catch (errorResult) {
        result = JSON.parse(errorResult.output);

        for (let [key, errors] of Object.entries(result)) {
        // Object.keys(result).map(errors => {
          //console.log('errors', key, errors);
          for ( let err of errors) {
            lintOutputs.push(`${key} (${err.line}:${err.column}): ${err.message}`);
          }
          if (errors.length) {
            //linting errors found
            errorCount += errors.length;
          }
        }
      }

      if (errorCount) {
        logger.error(`✗ JavaScript: ${errorCount} errors`);
        logger.warn(lintOutputs.join('\n'));
      }else {
        logger.success(`✓ JavaScript`);
      }

    }
    return {results, errorCount};
  }

  public async lintSass(configFile:String, globs:Array<string>):Promise<any> {
    let result;
    let errorCount = 0;
    let warningCount = 0;

    try {
      result = await shellExec(`./node_modules/.bin/sass-lint -c ${configFile} --format json --verbose`, {
        noErrors: true,
        silent: true
      });
    }catch (errorResult) {
      if (errorResult.error) {
        logger.error(`✗ Sass`);
        //result = JSON.parse(errorResult.error);
        throw new Error(errorResult.error);
      }
    }

    //console.log('result', result);

    let lintResult;
    let lintOutputs = [];
    if (result.output) {
      lintResult = JSON.parse(result.output);
      for (result of lintResult) {
        if (result.warningCount) warningCount += result.warningCount;
        if (result.errorCount) errorCount += result.errorCount;

        for ( let err of result.messages) {
          lintOutputs.push(`${result.filePath} (${err.line}:${err.column}): [severity: ${err.severity}] ${err.ruleId} (${err.message})`);
        }
      }
    }

    if (warningCount || errorCount) {
      logger.error(`✗ Sass: ${errorCount} errors, ${warningCount} warnings `);
      logger.warn(lintOutputs.join('\n'));
    }else {
      logger.success(`✓ Sass`);
    }

    return {result: lintResult, errorCount, warningCount};
  }

  public async lintHtml(configFile:String, globs:Array<string>):Promise<any> {
    let results = [];
    let errorResults = [];
    let errorCount = 0;
    let lintOutputs = [];

    for (let globPattern of globs) {
      try {
        results.push(await shellExec(`./node_modules/.bin/htmlhint -c ${configFile} ${globPattern} --format json `, {
          noErrors: true,
          silent: true
        }));
      }catch (errorResult) {
        if (errorResult.output) {
          errorResults = errorResults.concat(JSON.parse(errorResult.output));
        }else {
          logger.error(`✗ HTML`);
          throw new Error(errorResult.error);
        }
      }

      if (errorResults.length)
      for (let result of errorResults) {
        if (result.messages && result.messages.length) {
          for ( let err of result.messages) {
            if (err.type === 'error') {
              lintOutputs.push(`${result.file} (${err.line}:${err.col}): ${err.rule.description} (${err.rule.id})`);
              errorCount++;
            }
          }
        }
      }
    }

    if (errorCount) {
      logger.error(`✗ HTML5: ${errorCount} errors`);
      logger.warn(lintOutputs.join('\n'));
    }else {
      logger.success(`✓ HTML5`);
    }

    return {results: results, errorCount};
  }
}

export default LintCLI;
