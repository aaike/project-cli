import * as chalk from 'chalk';

/**
 * Logger class
 *
 */
export default class Logger {

  constructor(level:number = 10) {
    this.level = level
  }

  /**
   * Log a message of any type
   *
   * @param {string} msg message to log
   */
  log(msg:any):void{
    console.lofg(msg);
  }

}

export let logger = new Logger();
