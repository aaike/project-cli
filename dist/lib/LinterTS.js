"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var __awaiter = undefined && undefined.__awaiter || function (thisArg, _arguments, Promise, generator) {
    return new Promise(function (resolve, reject) {
        generator = generator.call(thisArg, _arguments);
        function cast(value) {
            return value instanceof Promise && value.constructor === Promise ? value : new Promise(function (resolve) {
                resolve(value);
            });
        }
        function onfulfill(value) {
            try {
                step("next", value);
            } catch (e) {
                reject(e);
            }
        }
        function onreject(value) {
            try {
                step("throw", value);
            } catch (e) {
                reject(e);
            }
        }
        function step(verb, value) {
            var result = generator[verb](value);
            result.done ? resolve(result.value) : cast(result.value).then(onfulfill, onreject);
        }
        step("next", void 0);
    });
};
var fs = require('fs');
var Logger_1 = require('../lib/Logger');
var glob = require('glob');
var tslint = require('tslint');

var TSLinter = function () {
    function TSLinter() {
        var config = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];

        _classCallCheck(this, TSLinter);

        this.config = config;
    }

    _createClass(TSLinter, [{
        key: "lint",
        value: function lint(globs) {
            return __awaiter(this, void 0, Promise, regeneratorRuntime.mark(function _callee() {
                var results, _iteratorNormalCompletion, _didIteratorError, _iteratorError, _iterator, _step, pattern, files, _iteratorNormalCompletion2, _didIteratorError2, _iteratorError2, _iterator2, _step2, file, result;

                return regeneratorRuntime.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                results = {
                                    failureCount: 0,
                                    failures: [],
                                    errors: []
                                };
                                _context.prev = 1;
                                _iteratorNormalCompletion = true;
                                _didIteratorError = false;
                                _iteratorError = undefined;
                                _context.prev = 5;
                                _iterator = globs[Symbol.iterator]();

                            case 7:
                                if (_iteratorNormalCompletion = (_step = _iterator.next()).done) {
                                    _context.next = 45;
                                    break;
                                }

                                pattern = _step.value;
                                _context.next = 11;
                                return glob.sync(pattern);

                            case 11:
                                files = _context.sent;
                                _iteratorNormalCompletion2 = true;
                                _didIteratorError2 = false;
                                _iteratorError2 = undefined;
                                _context.prev = 15;
                                _iterator2 = files[Symbol.iterator]();

                            case 17:
                                if (_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done) {
                                    _context.next = 28;
                                    break;
                                }

                                file = _step2.value;
                                result = this.lintFile(file);

                                if (result.result.failureCount) {
                                    _context.next = 22;
                                    break;
                                }

                                return _context.abrupt("continue", 25);

                            case 22:
                                results.failureCount += result.result.failureCount;
                                results.failures.push({
                                    file: file,
                                    failures: result.result.failures
                                });
                                results.errors = results.errors.concat(result.errors);

                            case 25:
                                _iteratorNormalCompletion2 = true;
                                _context.next = 17;
                                break;

                            case 28:
                                _context.next = 34;
                                break;

                            case 30:
                                _context.prev = 30;
                                _context.t0 = _context["catch"](15);
                                _didIteratorError2 = true;
                                _iteratorError2 = _context.t0;

                            case 34:
                                _context.prev = 34;
                                _context.prev = 35;

                                if (!_iteratorNormalCompletion2 && _iterator2.return) {
                                    _iterator2.return();
                                }

                            case 37:
                                _context.prev = 37;

                                if (!_didIteratorError2) {
                                    _context.next = 40;
                                    break;
                                }

                                throw _iteratorError2;

                            case 40:
                                return _context.finish(37);

                            case 41:
                                return _context.finish(34);

                            case 42:
                                _iteratorNormalCompletion = true;
                                _context.next = 7;
                                break;

                            case 45:
                                _context.next = 51;
                                break;

                            case 47:
                                _context.prev = 47;
                                _context.t1 = _context["catch"](5);
                                _didIteratorError = true;
                                _iteratorError = _context.t1;

                            case 51:
                                _context.prev = 51;
                                _context.prev = 52;

                                if (!_iteratorNormalCompletion && _iterator.return) {
                                    _iterator.return();
                                }

                            case 54:
                                _context.prev = 54;

                                if (!_didIteratorError) {
                                    _context.next = 57;
                                    break;
                                }

                                throw _iteratorError;

                            case 57:
                                return _context.finish(54);

                            case 58:
                                return _context.finish(51);

                            case 59:
                                _context.next = 65;
                                break;

                            case 61:
                                _context.prev = 61;
                                _context.t2 = _context["catch"](1);

                                Logger_1.logger.error(_context.t2);
                                throw _context.t2;

                            case 65:
                                return _context.abrupt("return", results);

                            case 66:
                            case "end":
                                return _context.stop();
                        }
                    }
                }, _callee, this, [[1, 61], [5, 47, 51, 59], [15, 30, 34, 42], [35,, 37, 41], [52,, 54, 58]]);
            }));
        }
    }, {
        key: "lintFile",
        value: function lintFile(file) {
            var errors = [];
            var options = {
                formatter: 'json',
                configuration: this.config
            };
            var result = undefined;
            var contents = fs.readFileSync(file, 'utf8');
            var ll = new tslint(file, contents, options);
            result = ll.lint();
            var _iteratorNormalCompletion3 = true;
            var _didIteratorError3 = false;
            var _iteratorError3 = undefined;

            try {
                for (var _iterator3 = result.failures[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
                    var err = _step3.value;

                    var pos = err.startPosition.lineAndCharacter.line + ":" + err.startPosition.lineAndCharacter.character;
                    errors.push(err.fileName + " (" + pos + "): " + err.failure + " (" + err.ruleName + ")");
                }
            } catch (err) {
                _didIteratorError3 = true;
                _iteratorError3 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion3 && _iterator3.return) {
                        _iterator3.return();
                    }
                } finally {
                    if (_didIteratorError3) {
                        throw _iteratorError3;
                    }
                }
            }

            return { result: result, errors: errors };
        }
    }]);

    return TSLinter;
}();

exports.default = TSLinter;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImxpYi9MaW50ZXJUUy5qcyIsImxpYi9MaW50ZXJUUy50cyJdLCJuYW1lcyI6WyJUU0xpbnRlciIsIlRTTGludGVyLmNvbnN0cnVjdG9yIiwiVFNMaW50ZXIubGludCIsIlRTTGludGVyLmxpbnRGaWxlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7QUFBQSxJQUFJLFlBQVksU0FBQyxJQUFRLFVBQUssU0FBTCxJQUFtQixVQUFVLE9BQVYsRUFBbUIsVUFBbkIsRUFBK0IsT0FBL0IsRUFBd0MsU0FBeEMsRUFBbUQ7QUFDM0YsV0FBTyxJQUFJLE9BQUosQ0FBWSxVQUFVLE9BQVYsRUFBbUIsTUFBbkIsRUFBMkI7QUFDMUMsb0JBQVksVUFBVSxJQUFWLENBQWUsT0FBZixFQUF3QixVQUF4QixDQUFaLENBRDBDO0FBRTFDLGlCQUFTLElBQVQsQ0FBYyxLQUFkLEVBQXFCO0FBQUUsbUJBQU8saUJBQWlCLE9BQWpCLElBQTRCLE1BQU0sV0FBTixLQUFzQixPQUF0QixHQUFnQyxLQUE1RCxHQUFvRSxJQUFJLE9BQUosQ0FBWSxVQUFVLE9BQVYsRUFBbUI7QUFBRSx3QkFBUSxLQUFSLEVBQUY7YUFBbkIsQ0FBaEYsQ0FBVDtTQUFyQjtBQUNBLGlCQUFTLFNBQVQsQ0FBbUIsS0FBbkIsRUFBMEI7QUFBRSxnQkFBSTtBQUFFLHFCQUFLLE1BQUwsRUFBYSxLQUFiLEVBQUY7YUFBSixDQUE2QixPQUFPLENBQVAsRUFBVTtBQUFFLHVCQUFPLENBQVAsRUFBRjthQUFWO1NBQXpEO0FBQ0EsaUJBQVMsUUFBVCxDQUFrQixLQUFsQixFQUF5QjtBQUFFLGdCQUFJO0FBQUUscUJBQUssT0FBTCxFQUFjLEtBQWQsRUFBRjthQUFKLENBQThCLE9BQU8sQ0FBUCxFQUFVO0FBQUUsdUJBQU8sQ0FBUCxFQUFGO2FBQVY7U0FBekQ7QUFDQSxpQkFBUyxJQUFULENBQWMsSUFBZCxFQUFvQixLQUFwQixFQUEyQjtBQUN2QixnQkFBSSxTQUFTLFVBQVUsSUFBVixFQUFnQixLQUFoQixDQUFULENBRG1CO0FBRXZCLG1CQUFPLElBQVAsR0FBYyxRQUFRLE9BQU8sS0FBUCxDQUF0QixHQUFzQyxLQUFLLE9BQU8sS0FBUCxDQUFMLENBQW1CLElBQW5CLENBQXdCLFNBQXhCLEVBQW1DLFFBQW5DLENBQXRDLENBRnVCO1NBQTNCO0FBSUEsYUFBSyxNQUFMLEVBQWEsS0FBSyxDQUFMLENBQWIsQ0FUMEM7S0FBM0IsQ0FBbkIsQ0FEMkY7Q0FBbkQ7QUNDNUMsSUFBWSxLQUFFLFFBQU0sSUFBTixDQUFGO0FBRVosSUFBQSxXQUFBLFFBQXFCLGVBQXJCLENBQUE7QUFFQSxJQUFZLE9BQUksUUFBTSxNQUFOLENBQUo7QUFDWixJQUFZLFNBQU0sUUFBTSxRQUFOLENBQU47O0lBVVo7QUFFRUEsYUFGRixRQUVFQSxHQUFtQ0E7WUFBZkEsK0RBQWFBLGtCQUFFQTs7OEJBRnJDLFVBRXFDQTs7QUFBZkMsYUFBQUEsTUFBQUEsR0FBQUEsTUFBQUEsQ0FBZUQ7S0FBbkNBOztpQkFGRjs7NkJBU29CQSxPQUFtQkE7QURIL0IsbUJBQU8sVUFBVSxJQUFWLEVBQWdCLEtBQUssQ0FBTCxFQUFRLE9BQXhCLDBCQUFpQztvQkNJeENFLHlGQVFRQSxTQUVKQSw0RkFFTUEsTUFFSkE7Ozs7OztBQWROQSwwQ0FBY0E7QUFDaEJBLGtEQUFjQSxDQUFkQTtBQUNBQSw4Q0FBVUEsRUFBVkE7QUFDQUEsNENBQVFBLEVBQVJBOzs7Ozs7OzRDQUtxQkE7Ozs7Ozs7O0FBQVhBOzt1Q0FFVUEsS0FBS0EsSUFBTEEsQ0FBVUEsT0FBVkE7OztBQUFkQTs7Ozs7NkNBRWNBOzs7Ozs7OztBQUFSQTtBQUVKQSx5Q0FBU0EsS0FBS0EsUUFBTEEsQ0FBY0EsSUFBZEE7O29DQUVSQSxPQUFPQSxNQUFQQSxDQUFjQSxZQUFkQTs7Ozs7Ozs7QUFDTEEsd0NBQVFBLFlBQVJBLElBQXdCQSxPQUFPQSxNQUFQQSxDQUFjQSxZQUFkQTtBQUN4QkEsd0NBQVFBLFFBQVJBLENBQWlCQSxJQUFqQkEsQ0FBc0JBO0FBQ3BCQSw4Q0FEb0JBO0FBRXBCQSw4Q0FBVUEsT0FBT0EsTUFBUEEsQ0FBY0EsUUFBZEE7aUNBRlpBO0FBSUFBLHdDQUFRQSxNQUFSQSxHQUFpQkEsUUFBUUEsTUFBUkEsQ0FBZUEsTUFBZkEsQ0FBc0JBLE9BQU9BLE1BQVBBLENBQXZDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQVFKQSx5Q0FBQUEsTUFBQUEsQ0FBT0EsS0FBUEE7Ozs7aUVBS0tBOzs7Ozs7OzthRHZDcUMsQ0FBakMsQ0FBUCxDQ0crQkY7Ozs7aUNBdUNyQkEsTUFBV0E7QUFFekJHLGdCQUFJQSxTQUFTQSxFQUFUQSxDQUZxQkg7QUFJekJHLGdCQUFJQSxVQUFjQTtBQUNoQkEsMkJBQVdBLE1BQVhBO0FBQ0FBLCtCQUFlQSxLQUFLQSxNQUFMQTthQUZiQSxDQUpxQkg7QUFZekJHLGdCQUFJQSxrQkFBSkEsQ0FaeUJIO0FBZXpCRyxnQkFBSUEsV0FBV0EsR0FBR0EsWUFBSEEsQ0FBZ0JBLElBQWhCQSxFQUFzQkEsTUFBdEJBLENBQVhBLENBZnFCSDtBQWlCekJHLGdCQUFJQSxLQUFLQSxJQUFJQSxNQUFKQSxDQUFXQSxJQUFYQSxFQUFpQkEsUUFBakJBLEVBQTJCQSxPQUEzQkEsQ0FBTEEsQ0FqQnFCSDtBQWtCekJHLHFCQUFTQSxHQUFHQSxJQUFIQSxFQUFUQSxDQWxCeUJIOzs7Ozs7QUFtQnpCRyxzQ0FBaUJBLE9BQU9BLFFBQVBBLDJCQUFqQkEsd0dBQWtDQTt3QkFBeEJBLG1CQUF3QkE7O0FBRWhDQSx3QkFBSUEsTUFBU0EsSUFBSUEsYUFBSkEsQ0FBa0JBLGdCQUFsQkEsQ0FBbUNBLElBQW5DQSxTQUEyQ0EsSUFBSUEsYUFBSkEsQ0FBa0JBLGdCQUFsQkEsQ0FBbUNBLFNBQW5DQSxDQUZ4QkE7QUFHaENBLDJCQUFPQSxJQUFQQSxDQUFlQSxJQUFJQSxRQUFKQSxVQUFpQkEsY0FBU0EsSUFBSUEsT0FBSkEsVUFBZ0JBLElBQUlBLFFBQUpBLE1BQXpEQSxFQUhnQ0E7aUJBQWxDQTs7Ozs7Ozs7Ozs7Ozs7YUFuQnlCSDs7QUF5QnpCRyxtQkFBT0EsRUFBQ0EsY0FBREEsRUFBU0EsY0FBVEEsRUFBUEEsQ0F6QnlCSDs7OztXQWhEN0I7OztBQThFQSxRQUFBLE9BQUEsR0FBZSxRQUFmIiwiZmlsZSI6ImxpYi9MaW50ZXJUUy5qcyIsInNvdXJjZXNDb250ZW50IjpbInZhciBfX2F3YWl0ZXIgPSAodGhpcyAmJiB0aGlzLl9fYXdhaXRlcikgfHwgZnVuY3Rpb24gKHRoaXNBcmcsIF9hcmd1bWVudHMsIFByb21pc2UsIGdlbmVyYXRvcikge1xuICAgIHJldHVybiBuZXcgUHJvbWlzZShmdW5jdGlvbiAocmVzb2x2ZSwgcmVqZWN0KSB7XG4gICAgICAgIGdlbmVyYXRvciA9IGdlbmVyYXRvci5jYWxsKHRoaXNBcmcsIF9hcmd1bWVudHMpO1xuICAgICAgICBmdW5jdGlvbiBjYXN0KHZhbHVlKSB7IHJldHVybiB2YWx1ZSBpbnN0YW5jZW9mIFByb21pc2UgJiYgdmFsdWUuY29uc3RydWN0b3IgPT09IFByb21pc2UgPyB2YWx1ZSA6IG5ldyBQcm9taXNlKGZ1bmN0aW9uIChyZXNvbHZlKSB7IHJlc29sdmUodmFsdWUpOyB9KTsgfVxuICAgICAgICBmdW5jdGlvbiBvbmZ1bGZpbGwodmFsdWUpIHsgdHJ5IHsgc3RlcChcIm5leHRcIiwgdmFsdWUpOyB9IGNhdGNoIChlKSB7IHJlamVjdChlKTsgfSB9XG4gICAgICAgIGZ1bmN0aW9uIG9ucmVqZWN0KHZhbHVlKSB7IHRyeSB7IHN0ZXAoXCJ0aHJvd1wiLCB2YWx1ZSk7IH0gY2F0Y2ggKGUpIHsgcmVqZWN0KGUpOyB9IH1cbiAgICAgICAgZnVuY3Rpb24gc3RlcCh2ZXJiLCB2YWx1ZSkge1xuICAgICAgICAgICAgdmFyIHJlc3VsdCA9IGdlbmVyYXRvclt2ZXJiXSh2YWx1ZSk7XG4gICAgICAgICAgICByZXN1bHQuZG9uZSA/IHJlc29sdmUocmVzdWx0LnZhbHVlKSA6IGNhc3QocmVzdWx0LnZhbHVlKS50aGVuKG9uZnVsZmlsbCwgb25yZWplY3QpO1xuICAgICAgICB9XG4gICAgICAgIHN0ZXAoXCJuZXh0XCIsIHZvaWQgMCk7XG4gICAgfSk7XG59O1xudmFyIGZzID0gcmVxdWlyZSgnZnMnKTtcbnZhciBMb2dnZXJfMSA9IHJlcXVpcmUoJy4uL2xpYi9Mb2dnZXInKTtcbnZhciBnbG9iID0gcmVxdWlyZSgnZ2xvYicpO1xudmFyIHRzbGludCA9IHJlcXVpcmUoJ3RzbGludCcpO1xuY2xhc3MgVFNMaW50ZXIge1xuICAgIGNvbnN0cnVjdG9yKGNvbmZpZyA9IHt9KSB7XG4gICAgICAgIHRoaXMuY29uZmlnID0gY29uZmlnO1xuICAgIH1cbiAgICBsaW50KGdsb2JzKSB7XG4gICAgICAgIHJldHVybiBfX2F3YWl0ZXIodGhpcywgdm9pZCAwLCBQcm9taXNlLCBmdW5jdGlvbiogKCkge1xuICAgICAgICAgICAgbGV0IHJlc3VsdHMgPSB7XG4gICAgICAgICAgICAgICAgZmFpbHVyZUNvdW50OiAwLFxuICAgICAgICAgICAgICAgIGZhaWx1cmVzOiBbXSxcbiAgICAgICAgICAgICAgICBlcnJvcnM6IFtdXG4gICAgICAgICAgICB9O1xuICAgICAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgICAgICBmb3IgKGxldCBwYXR0ZXJuIG9mIGdsb2JzKSB7XG4gICAgICAgICAgICAgICAgICAgIGxldCBmaWxlcyA9IHlpZWxkIGdsb2Iuc3luYyhwYXR0ZXJuKTtcbiAgICAgICAgICAgICAgICAgICAgZm9yIChsZXQgZmlsZSBvZiBmaWxlcykge1xuICAgICAgICAgICAgICAgICAgICAgICAgbGV0IHJlc3VsdCA9IHRoaXMubGludEZpbGUoZmlsZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoIXJlc3VsdC5yZXN1bHQuZmFpbHVyZUNvdW50KVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnRpbnVlO1xuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdWx0cy5mYWlsdXJlQ291bnQgKz0gcmVzdWx0LnJlc3VsdC5mYWlsdXJlQ291bnQ7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXN1bHRzLmZhaWx1cmVzLnB1c2goe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZpbGUsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZmFpbHVyZXM6IHJlc3VsdC5yZXN1bHQuZmFpbHVyZXNcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdWx0cy5lcnJvcnMgPSByZXN1bHRzLmVycm9ycy5jb25jYXQocmVzdWx0LmVycm9ycyk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBjYXRjaCAoZXJyKSB7XG4gICAgICAgICAgICAgICAgTG9nZ2VyXzEubG9nZ2VyLmVycm9yKGVycik7XG4gICAgICAgICAgICAgICAgdGhyb3cgZXJyO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIHJlc3VsdHM7XG4gICAgICAgIH0pO1xuICAgIH1cbiAgICBsaW50RmlsZShmaWxlKSB7XG4gICAgICAgIGxldCBlcnJvcnMgPSBbXTtcbiAgICAgICAgbGV0IG9wdGlvbnMgPSB7XG4gICAgICAgICAgICBmb3JtYXR0ZXI6ICdqc29uJyxcbiAgICAgICAgICAgIGNvbmZpZ3VyYXRpb246IHRoaXMuY29uZmlnLFxuICAgICAgICB9O1xuICAgICAgICBsZXQgcmVzdWx0O1xuICAgICAgICBsZXQgY29udGVudHMgPSBmcy5yZWFkRmlsZVN5bmMoZmlsZSwgJ3V0ZjgnKTtcbiAgICAgICAgbGV0IGxsID0gbmV3IHRzbGludChmaWxlLCBjb250ZW50cywgb3B0aW9ucyk7XG4gICAgICAgIHJlc3VsdCA9IGxsLmxpbnQoKTtcbiAgICAgICAgZm9yIChsZXQgZXJyIG9mIHJlc3VsdC5mYWlsdXJlcykge1xuICAgICAgICAgICAgbGV0IHBvcyA9IGAke2Vyci5zdGFydFBvc2l0aW9uLmxpbmVBbmRDaGFyYWN0ZXIubGluZX06JHtlcnIuc3RhcnRQb3NpdGlvbi5saW5lQW5kQ2hhcmFjdGVyLmNoYXJhY3Rlcn1gO1xuICAgICAgICAgICAgZXJyb3JzLnB1c2goYCR7ZXJyLmZpbGVOYW1lfSAoJHtwb3N9KTogJHtlcnIuZmFpbHVyZX0gKCR7ZXJyLnJ1bGVOYW1lfSlgKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4geyByZXN1bHQsIGVycm9ycyB9O1xuICAgIH1cbn1cbmV4cG9ydHMuZGVmYXVsdCA9IFRTTGludGVyO1xuIiwiLy9pbXBvcnQgKiBhcyBmc3AgZnJvbSAnZnMtcHJvbWlzZSc7XG5pbXBvcnQgKiBhcyBmcyBmcm9tICdmcyc7XG5cbmltcG9ydCB7bG9nZ2VyfSBmcm9tICcuLi9saWIvTG9nZ2VyJztcbi8vIGltcG9ydCBzaGVsbF9leGVjIGZyb20gJy4uL2xpYi9leGVjJztcbmltcG9ydCAqIGFzIGdsb2IgZnJvbSAnZ2xvYic7XG5pbXBvcnQgKiBhcyB0c2xpbnQgZnJvbSAndHNsaW50JztcblxuaW50ZXJmYWNlIElUU2xpbnRlciB7XG4gIGxpbnQoZ2xvYnM6QXJyYXk8c3RyaW5nPik6UHJvbWlzZTxhbnk+O1xuICBsaW50RmlsZShmaWxlOnN0cmluZywgdmVyYm9zZTpib29sZWFuKTpQcm9taXNlPGFueT47XG59XG5cbi8qKlxuICogTGludGVyIENMSVxuICovXG5jbGFzcyBUU0xpbnRlciBpbXBsZW1lbnRzIElUU2xpbnRlciB7XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBjb25maWc6YW55ID0ge30pIHtcblxuICB9XG5cbiAgLyoqXG4gICAqIFJ1biBUU2xpbnQgb24gYWxsIHRzIGZpbGVzIG1hdGhpbmcgZ2xvYiBwYXR0ZXJuc1xuICAgKi9cbiAgcHVibGljIGFzeW5jIGxpbnQoZ2xvYnM6QXJyYXk8c3RyaW5nPik6UHJvbWlzZTxhbnk+IHtcbiAgICBsZXQgcmVzdWx0czphbnkgPSB7XG4gICAgICBmYWlsdXJlQ291bnQ6IDAsXG4gICAgICBmYWlsdXJlczogW10sXG4gICAgICBlcnJvcnM6IFtdXG4gICAgfTtcbiAgICAvLyBjb25zb2xlLmxvZygnZ2xvYnMnLCBnbG9icyk7XG5cbiAgICB0cnkge1xuICAgICAgZm9yICggbGV0IHBhdHRlcm4gb2YgZ2xvYnMpIHtcbiAgICAgICAgLy8gbG9nZ2VyLmRlYnVnKGBwYXR0ZXJuOiAke3BhdHRlcm59YCk7XG4gICAgICAgIGxldCBmaWxlcyA9IGF3YWl0IGdsb2Iuc3luYyhwYXR0ZXJuKTtcbiAgICAgICAgLy8gbG9nZ2VyLmRlYnVnKGBmaWxlczogJHtmaWxlc31gKTtcbiAgICAgICAgZm9yICggbGV0IGZpbGUgb2YgZmlsZXMpIHtcbiAgICAgICAgICAvLyBsb2dnZXIuZGVidWcoYGZpbGU6ICR7ZmlsZX1gKTtcbiAgICAgICAgICBsZXQgcmVzdWx0ID0gdGhpcy5saW50RmlsZShmaWxlKTtcbiAgICAgICAgICAvLyBjb25zb2xlLmxvZygncmVzdWx0JywgcmVzdWx0KTtcbiAgICAgICAgICBpZiAoIXJlc3VsdC5yZXN1bHQuZmFpbHVyZUNvdW50KSBjb250aW51ZTtcbiAgICAgICAgICByZXN1bHRzLmZhaWx1cmVDb3VudCArPSByZXN1bHQucmVzdWx0LmZhaWx1cmVDb3VudDtcbiAgICAgICAgICByZXN1bHRzLmZhaWx1cmVzLnB1c2goe1xuICAgICAgICAgICAgZmlsZSxcbiAgICAgICAgICAgIGZhaWx1cmVzOiByZXN1bHQucmVzdWx0LmZhaWx1cmVzXG4gICAgICAgICAgfSk7XG4gICAgICAgICAgcmVzdWx0cy5lcnJvcnMgPSByZXN1bHRzLmVycm9ycy5jb25jYXQocmVzdWx0LmVycm9ycyk7XG4gICAgICAgIH1cbiAgICAgICAgLy8gY29uc29sZS5sb2coJy0tLScpO1xuICAgICAgfVxuXG4gICAgICAvL2xvZ2dlci53YXJuKHJlc3VsdHMuZXJyb3JzLmpvaW4oJ1xcbicpKTtcblxuICAgIH1jYXRjaCAoZXJyKSB7XG4gICAgICBsb2dnZXIuZXJyb3IoZXJyKTtcbiAgICAgIHRocm93IGVycjtcbiAgICB9XG5cbiAgICAvL2xvZ2dlci5pbmZvKGBUU0xJTlQgcmVzdWx0cyAke0pTT04uc3RyaW5naWZ5KHJlc3VsdHMpfWApO1xuICAgIHJldHVybiByZXN1bHRzO1xuICB9XG5cbiAgcHVibGljIGxpbnRGaWxlKGZpbGU6c3RyaW5nKTphbnkge1xuXG4gICAgbGV0IGVycm9ycyA9IFtdO1xuXG4gICAgbGV0IG9wdGlvbnM6YW55ID0ge1xuICAgICAgZm9ybWF0dGVyOiAnanNvbicsXG4gICAgICBjb25maWd1cmF0aW9uOiB0aGlzLmNvbmZpZyxcbiAgICAgIC8vIHJ1bGVzRGlyZWN0b3J5OiAnY3VzdG9tUnVsZXMvJywgLy8gY2FuIGJlIGFuIGFycmF5IG9mIGRpcmVjdG9yaWVzXG4gICAgICAvLyBmb3JtYXR0ZXJzRGlyZWN0b3J5OiAnY3VzdG9tRm9ybWF0dGVycy8nXG4gICAgfTtcblxuICAgIC8vIGxvZ2dlci5kZWJ1ZyhgbGludGluZyBUUzogJHtmaWxlfWApO1xuICAgIGxldCByZXN1bHQ7XG5cbiAgICAvL2xldCBjb250ZW50cyA9IGF3YWl0IGZzcC5yZWFkRmlsZShmaWxlLCB7ZW5jb2Rpbmc6ICd1dGY4J30pO1xuICAgIGxldCBjb250ZW50cyA9IGZzLnJlYWRGaWxlU3luYyhmaWxlLCAndXRmOCcpO1xuICAgIC8vY29uc29sZS5sb2coJ2NvbnRlbnRzJywgY29udGVudHMpO1xuICAgIGxldCBsbCA9IG5ldyB0c2xpbnQoZmlsZSwgY29udGVudHMsIG9wdGlvbnMpO1xuICAgIHJlc3VsdCA9IGxsLmxpbnQoKTtcbiAgICBmb3IgKCBsZXQgZXJyIG9mIHJlc3VsdC5mYWlsdXJlcykge1xuICAgICAgLy9jb25zb2xlLmxvZygnZXJyJywgZXJyKTtcbiAgICAgIGxldCBwb3MgPSBgJHtlcnIuc3RhcnRQb3NpdGlvbi5saW5lQW5kQ2hhcmFjdGVyLmxpbmV9OiR7ZXJyLnN0YXJ0UG9zaXRpb24ubGluZUFuZENoYXJhY3Rlci5jaGFyYWN0ZXJ9YDtcbiAgICAgIGVycm9ycy5wdXNoKGAke2Vyci5maWxlTmFtZX0gKCR7cG9zfSk6ICR7ZXJyLmZhaWx1cmV9ICgke2Vyci5ydWxlTmFtZX0pYCk7XG4gICAgfVxuXG4gICAgcmV0dXJuIHtyZXN1bHQsIGVycm9yc307XG4gIH1cblxufVxuXG5leHBvcnQgZGVmYXVsdCBUU0xpbnRlcjtcbiJdLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==
