declare module 'child-process-promise' {
  const mod:any;
  export = mod;
}
declare module 'native-dns' {
  const mod:any;
  export = mod;
}
declare module 'fs-promise' {
  const mod:any;
  export = mod;
}
declare module 'pm2' {
  const mod:any;
  export = mod;
}
declare module 'mkdirp' {
  const mod:any;
  export = mod;
}
declare module 'figlet' {
  const mod:any;
  export = mod;
}
declare module 'promisify-node' {
  const mod:any;
  export = mod;
}
declare module 'glob-promise' {
  const mod:any;
  export = mod;
}
declare module 'glob' {
  const mod:any;
  export = mod;
}
declare module 'liftoff' {
  const mod:any;
  export = mod;
}

/**
 * Define ES201x Object methods
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object
 */
interface ObjectConstructor {
  assign(target: any, ...sources:Array<any>): any;
  entries(target: any): any;
  values(obj: any): Array<any>;
  getOwnPropertySymbols(obj: any): Array<any>;
}
