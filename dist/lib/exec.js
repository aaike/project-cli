"use strict";

var __awaiter = undefined && undefined.__awaiter || function (thisArg, _arguments, Promise, generator) {
    return new Promise(function (resolve, reject) {
        generator = generator.call(thisArg, _arguments);
        function cast(value) {
            return value instanceof Promise && value.constructor === Promise ? value : new Promise(function (resolve) {
                resolve(value);
            });
        }
        function onfulfill(value) {
            try {
                step("next", value);
            } catch (e) {
                reject(e);
            }
        }
        function onreject(value) {
            try {
                step("throw", value);
            } catch (e) {
                reject(e);
            }
        }
        function step(verb, value) {
            var result = generator[verb](value);
            result.done ? resolve(result.value) : cast(result.value).then(onfulfill, onreject);
        }
        step("next", void 0);
    });
};
var childProcessPromise = require('child-process-promise');
var Logger_1 = require('./Logger');
var shellExec = childProcessPromise.exec;
var maxBuffer = 1024 * 5000;
function exec(cmd) {
    var options = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];

    return __awaiter(this, void 0, Promise, regeneratorRuntime.mark(function _callee2() {
        var _this = this;

        return regeneratorRuntime.wrap(function _callee2$(_context2) {
            while (1) {
                switch (_context2.prev = _context2.next) {
                    case 0:
                        return _context2.abrupt("return", new Promise(function (resolve, reject) {
                            return __awaiter(_this, void 0, Promise, regeneratorRuntime.mark(function _callee() {
                                var allOutput;
                                return regeneratorRuntime.wrap(function _callee$(_context) {
                                    while (1) {
                                        switch (_context.prev = _context.next) {
                                            case 0:
                                                allOutput = '';

                                                shellExec(cmd, Object.assign({
                                                    maxBuffer: maxBuffer
                                                }, options)).progress(function (childProcess) {
                                                    childProcess.stdout.on('data', function (data) {
                                                        var str = data;
                                                        if (!options.silent) Logger_1.logger.debug(str);
                                                        allOutput += str;
                                                    });
                                                    if (!options.noErrors) childProcess.stderr.on('data', function (data) {
                                                        Logger_1.logger.error(data);
                                                    });
                                                }).then(function (childProcess) {
                                                    resolve({ output: childProcess.stdout.toString(), childProcess: childProcess });
                                                }).fail(function (err) {
                                                    reject({ error: err.toString('utf8'), output: allOutput });
                                                });

                                            case 2:
                                            case "end":
                                                return _context.stop();
                                        }
                                    }
                                }, _callee, this);
                            }));
                        }));

                    case 1:
                    case "end":
                        return _context2.stop();
                }
            }
        }, _callee2, this);
    }));
}
function exec2(cmd) {
    var args = arguments.length <= 1 || arguments[1] === undefined ? [] : arguments[1];
    var options = arguments.length <= 2 || arguments[2] === undefined ? {} : arguments[2];

    return __awaiter(this, void 0, Promise, regeneratorRuntime.mark(function _callee3() {
        return regeneratorRuntime.wrap(function _callee3$(_context3) {
            while (1) {
                switch (_context3.prev = _context3.next) {
                    case 0:
                        return _context3.abrupt("return", new Promise(function (resolve, reject) {
                            try {
                                var spawn = require('child_process').spawn;
                                spawn(cmd, args, { stdio: 'inherit' }).on('exit', function (code) {
                                    resolve(code);
                                }).on('error', function (err) {
                                    reject(err);
                                });
                            } catch (err) {
                                reject(err);
                            }
                        }));

                    case 1:
                    case "end":
                        return _context3.stop();
                }
            }
        }, _callee3, this);
    }));
}
exports.exec2 = exec2;
exports.default = exec;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImxpYi9leGVjLmpzIiwibGliL2V4ZWMudHMiXSwibmFtZXMiOlsiZXhlYyIsImV4ZWMyIl0sIm1hcHBpbmdzIjoiOztBQUFBLElBQUksWUFBWSxTQUFDLElBQVEsVUFBSyxTQUFMLElBQW1CLFVBQVUsT0FBVixFQUFtQixVQUFuQixFQUErQixPQUEvQixFQUF3QyxTQUF4QyxFQUFtRDtBQUMzRixXQUFPLElBQUksT0FBSixDQUFZLFVBQVUsT0FBVixFQUFtQixNQUFuQixFQUEyQjtBQUMxQyxvQkFBWSxVQUFVLElBQVYsQ0FBZSxPQUFmLEVBQXdCLFVBQXhCLENBQVosQ0FEMEM7QUFFMUMsaUJBQVMsSUFBVCxDQUFjLEtBQWQsRUFBcUI7QUFBRSxtQkFBTyxpQkFBaUIsT0FBakIsSUFBNEIsTUFBTSxXQUFOLEtBQXNCLE9BQXRCLEdBQWdDLEtBQTVELEdBQW9FLElBQUksT0FBSixDQUFZLFVBQVUsT0FBVixFQUFtQjtBQUFFLHdCQUFRLEtBQVIsRUFBRjthQUFuQixDQUFoRixDQUFUO1NBQXJCO0FBQ0EsaUJBQVMsU0FBVCxDQUFtQixLQUFuQixFQUEwQjtBQUFFLGdCQUFJO0FBQUUscUJBQUssTUFBTCxFQUFhLEtBQWIsRUFBRjthQUFKLENBQTZCLE9BQU8sQ0FBUCxFQUFVO0FBQUUsdUJBQU8sQ0FBUCxFQUFGO2FBQVY7U0FBekQ7QUFDQSxpQkFBUyxRQUFULENBQWtCLEtBQWxCLEVBQXlCO0FBQUUsZ0JBQUk7QUFBRSxxQkFBSyxPQUFMLEVBQWMsS0FBZCxFQUFGO2FBQUosQ0FBOEIsT0FBTyxDQUFQLEVBQVU7QUFBRSx1QkFBTyxDQUFQLEVBQUY7YUFBVjtTQUF6RDtBQUNBLGlCQUFTLElBQVQsQ0FBYyxJQUFkLEVBQW9CLEtBQXBCLEVBQTJCO0FBQ3ZCLGdCQUFJLFNBQVMsVUFBVSxJQUFWLEVBQWdCLEtBQWhCLENBQVQsQ0FEbUI7QUFFdkIsbUJBQU8sSUFBUCxHQUFjLFFBQVEsT0FBTyxLQUFQLENBQXRCLEdBQXNDLEtBQUssT0FBTyxLQUFQLENBQUwsQ0FBbUIsSUFBbkIsQ0FBd0IsU0FBeEIsRUFBbUMsUUFBbkMsQ0FBdEMsQ0FGdUI7U0FBM0I7QUFJQSxhQUFLLE1BQUwsRUFBYSxLQUFLLENBQUwsQ0FBYixDQVQwQztLQUEzQixDQUFuQixDQUQyRjtDQUFuRDtBQ0E1QyxJQUFZLHNCQUFtQixRQUFNLHVCQUFOLENBQW5CO0FBQ1osSUFBQSxXQUFBLFFBQXFCLFVBQXJCLENBQUE7QUFFQSxJQUFNLFlBQWdCLG9CQUFvQixJQUFwQjtBQUd0QixJQUFNLFlBQVksT0FBTyxJQUFQO0FBU2xCLFNBQUEsSUFBQSxDQUFvQixHQUFwQixFQUFnRDtRQUFoQixnRUFBYyxrQkFBRTs7QURHNUMsV0FBTyxVQUFVLElBQVYsRUFBZ0IsS0FBSyxDQUFMLEVBQVEsT0FBeEIsMEJBQWlDOzs7Ozs7OzBEQ0ZuQ0EsSUFBSUEsT0FBSkEsQ0FBWUEsVUFBT0EsT0FBUEEsRUFBZ0JBLE1BQWhCQTttQ0FBc0JBLGlCQUFBQSxLQUFBQSxDQUFBQSxFQUFBQSxPQUFBQSwwQkFBQUE7b0NBQ25DQTs7Ozs7NERBQWdCQTs7QUFFcEJBLDBEQUFVQSxHQUFWQSxFQUFlQSxPQUFPQSxNQUFQQSxDQUFjQTtBQUMzQkEsK0RBQVdBLFNBQVhBO2lEQURhQSxFQUVaQSxPQUZZQSxDQUFmQSxFQUdHQSxRQUhIQSxDQUdZQSxVQUFDQSxZQUFEQSxFQUFhQTtBQUNyQkEsaUVBQWFBLE1BQWJBLENBQW9CQSxFQUFwQkEsQ0FBdUJBLE1BQXZCQSxFQUErQkEsVUFBQ0EsSUFBREEsRUFBS0E7QUFDbENBLDREQUFJQSxNQUFNQSxJQUFOQSxDQUQ4QkE7QUFFbENBLDREQUFJQSxDQUFDQSxRQUFRQSxNQUFSQSxFQUFnQkEsU0FBQUEsTUFBQUEsQ0FBT0EsS0FBUEEsQ0FBYUEsR0FBYkEsRUFBckJBO0FBQ0FBLHFFQUFhQSxHQUFiQSxDQUhrQ0E7cURBQUxBLENBQS9CQSxDQURxQkE7QUFNckJBLHdEQUFJQSxDQUFDQSxRQUFRQSxRQUFSQSxFQUNMQSxhQUFhQSxNQUFiQSxDQUFvQkEsRUFBcEJBLENBQXVCQSxNQUF2QkEsRUFBK0JBLFVBQUNBLElBQURBLEVBQUtBO0FBQ2xDQSxpRUFBQUEsTUFBQUEsQ0FBT0EsS0FBUEEsQ0FBYUEsSUFBYkEsRUFEa0NBO3FEQUFMQSxDQUEvQkEsQ0FEQUE7aURBTlFBLENBSFpBLENBY0dBLElBZEhBLENBY1FBLFVBQUNBLFlBQURBLEVBQWFBO0FBQ2pCQSw0REFBUUEsRUFBQ0EsUUFBUUEsYUFBYUEsTUFBYkEsQ0FBb0JBLFFBQXBCQSxFQUFSQSxFQUF3Q0EsMEJBQXpDQSxFQUFSQSxFQURpQkE7aURBQWJBLENBZFJBLENBa0JHQSxJQWxCSEEsQ0FrQlFBLFVBQUNBLEdBQURBLEVBQUlBO0FBRVJBLDJEQUFPQSxFQUFDQSxPQUFNQSxJQUFJQSxRQUFKQSxDQUFhQSxNQUFiQSxDQUFOQSxFQUE0QkEsUUFBT0EsU0FBUEEsRUFBcENBLEVBRlFBO2lEQUFKQSxDQWxCUkE7Ozs7Ozs7OzZCQUh1Q0EsQ0FBQUE7eUJBQXRCQTs7Ozs7Ozs7S0RFdUIsQ0FBakMsQ0FBUCxDQ0g0QztDQUFoRDtBQTZCQSxTQUFBLEtBQUEsQ0FBNEIsR0FBNUIsRUFBK0U7UUFBdkMsNkRBQWtCLGtCQUFxQjtRQUFoQixnRUFBYyxrQkFBRTs7QURFM0UsV0FBTyxVQUFVLElBQVYsRUFBZ0IsS0FBSyxDQUFMLEVBQVEsT0FBeEIsMEJBQWlDOzs7OzswRENEbkNDLElBQUlBLE9BQUpBLENBQVlBLFVBQUNBLE9BQURBLEVBQVVBLE1BQVZBLEVBQWdCQTtBQUNqQ0EsZ0NBQUlBO0FBQ0ZBLG9DQUFJQSxRQUFRQSxRQUFRQSxlQUFSQSxFQUF5QkEsS0FBekJBLENBRFZBO0FBRUZBLHNDQUFNQSxHQUFOQSxFQUFXQSxJQUFYQSxFQUFpQkEsRUFBRUEsT0FBT0EsU0FBUEEsRUFBbkJBLEVBQ0NBLEVBRERBLENBQ0lBLE1BREpBLEVBQ1lBLFVBQVNBLElBQVRBLEVBQWFBO0FBQ3ZCLDRDQUFRLElBQVIsRUFEdUJBO2lDQUFiQSxDQURaQSxDQUlDQSxFQUpEQSxDQUlJQSxPQUpKQSxFQUlhQSxVQUFTQSxHQUFUQSxFQUFZQTtBQUN2QiwyQ0FBTyxHQUFQLEVBRHVCQTtpQ0FBWkEsQ0FKYkEsQ0FGRUE7NkJBQUpBLENBU0NBLE9BQU9BLEdBQVBBLEVBQVlBO0FBQ1hBLHVDQUFPQSxHQUFQQSxFQURXQTs2QkFBWkE7eUJBVmdCQTs7Ozs7Ozs7S0RDdUIsQ0FBakMsQ0FBUCxDQ0YyRTtDQUEvRTtBQUFzQixRQUFBLEtBQUEsR0FBSyxLQUFMO0FBbUJ0QixRQUFBLE9BQUEsR0FBZSxJQUFmIiwiZmlsZSI6ImxpYi9leGVjLmpzIiwic291cmNlc0NvbnRlbnQiOlsidmFyIF9fYXdhaXRlciA9ICh0aGlzICYmIHRoaXMuX19hd2FpdGVyKSB8fCBmdW5jdGlvbiAodGhpc0FyZywgX2FyZ3VtZW50cywgUHJvbWlzZSwgZ2VuZXJhdG9yKSB7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKGZ1bmN0aW9uIChyZXNvbHZlLCByZWplY3QpIHtcbiAgICAgICAgZ2VuZXJhdG9yID0gZ2VuZXJhdG9yLmNhbGwodGhpc0FyZywgX2FyZ3VtZW50cyk7XG4gICAgICAgIGZ1bmN0aW9uIGNhc3QodmFsdWUpIHsgcmV0dXJuIHZhbHVlIGluc3RhbmNlb2YgUHJvbWlzZSAmJiB2YWx1ZS5jb25zdHJ1Y3RvciA9PT0gUHJvbWlzZSA/IHZhbHVlIDogbmV3IFByb21pc2UoZnVuY3Rpb24gKHJlc29sdmUpIHsgcmVzb2x2ZSh2YWx1ZSk7IH0pOyB9XG4gICAgICAgIGZ1bmN0aW9uIG9uZnVsZmlsbCh2YWx1ZSkgeyB0cnkgeyBzdGVwKFwibmV4dFwiLCB2YWx1ZSk7IH0gY2F0Y2ggKGUpIHsgcmVqZWN0KGUpOyB9IH1cbiAgICAgICAgZnVuY3Rpb24gb25yZWplY3QodmFsdWUpIHsgdHJ5IHsgc3RlcChcInRocm93XCIsIHZhbHVlKTsgfSBjYXRjaCAoZSkgeyByZWplY3QoZSk7IH0gfVxuICAgICAgICBmdW5jdGlvbiBzdGVwKHZlcmIsIHZhbHVlKSB7XG4gICAgICAgICAgICB2YXIgcmVzdWx0ID0gZ2VuZXJhdG9yW3ZlcmJdKHZhbHVlKTtcbiAgICAgICAgICAgIHJlc3VsdC5kb25lID8gcmVzb2x2ZShyZXN1bHQudmFsdWUpIDogY2FzdChyZXN1bHQudmFsdWUpLnRoZW4ob25mdWxmaWxsLCBvbnJlamVjdCk7XG4gICAgICAgIH1cbiAgICAgICAgc3RlcChcIm5leHRcIiwgdm9pZCAwKTtcbiAgICB9KTtcbn07XG52YXIgY2hpbGRQcm9jZXNzUHJvbWlzZSA9IHJlcXVpcmUoJ2NoaWxkLXByb2Nlc3MtcHJvbWlzZScpO1xudmFyIExvZ2dlcl8xID0gcmVxdWlyZSgnLi9Mb2dnZXInKTtcbmNvbnN0IHNoZWxsRXhlYyA9IGNoaWxkUHJvY2Vzc1Byb21pc2UuZXhlYztcbmNvbnN0IG1heEJ1ZmZlciA9IDEwMjQgKiA1MDAwO1xuZnVuY3Rpb24gZXhlYyhjbWQsIG9wdGlvbnMgPSB7fSkge1xuICAgIHJldHVybiBfX2F3YWl0ZXIodGhpcywgdm9pZCAwLCBQcm9taXNlLCBmdW5jdGlvbiogKCkge1xuICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4gX19hd2FpdGVyKHRoaXMsIHZvaWQgMCwgUHJvbWlzZSwgZnVuY3Rpb24qICgpIHtcbiAgICAgICAgICAgIGxldCBhbGxPdXRwdXQgPSAnJztcbiAgICAgICAgICAgIHNoZWxsRXhlYyhjbWQsIE9iamVjdC5hc3NpZ24oe1xuICAgICAgICAgICAgICAgIG1heEJ1ZmZlcjogbWF4QnVmZmVyXG4gICAgICAgICAgICB9LCBvcHRpb25zKSlcbiAgICAgICAgICAgICAgICAucHJvZ3Jlc3MoKGNoaWxkUHJvY2VzcykgPT4ge1xuICAgICAgICAgICAgICAgIGNoaWxkUHJvY2Vzcy5zdGRvdXQub24oJ2RhdGEnLCAoZGF0YSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICBsZXQgc3RyID0gZGF0YTtcbiAgICAgICAgICAgICAgICAgICAgaWYgKCFvcHRpb25zLnNpbGVudClcbiAgICAgICAgICAgICAgICAgICAgICAgIExvZ2dlcl8xLmxvZ2dlci5kZWJ1ZyhzdHIpO1xuICAgICAgICAgICAgICAgICAgICBhbGxPdXRwdXQgKz0gc3RyO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIGlmICghb3B0aW9ucy5ub0Vycm9ycylcbiAgICAgICAgICAgICAgICAgICAgY2hpbGRQcm9jZXNzLnN0ZGVyci5vbignZGF0YScsIChkYXRhKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBMb2dnZXJfMS5sb2dnZXIuZXJyb3IoZGF0YSk7XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAudGhlbigoY2hpbGRQcm9jZXNzKSA9PiB7XG4gICAgICAgICAgICAgICAgcmVzb2x2ZSh7IG91dHB1dDogY2hpbGRQcm9jZXNzLnN0ZG91dC50b1N0cmluZygpLCBjaGlsZFByb2Nlc3MgfSk7XG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIC5mYWlsKChlcnIpID0+IHtcbiAgICAgICAgICAgICAgICByZWplY3QoeyBlcnJvcjogZXJyLnRvU3RyaW5nKCd1dGY4JyksIG91dHB1dDogYWxsT3V0cHV0IH0pO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pKTtcbiAgICB9KTtcbn1cbmZ1bmN0aW9uIGV4ZWMyKGNtZCwgYXJncyA9IFtdLCBvcHRpb25zID0ge30pIHtcbiAgICByZXR1cm4gX19hd2FpdGVyKHRoaXMsIHZvaWQgMCwgUHJvbWlzZSwgZnVuY3Rpb24qICgpIHtcbiAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAgICAgbGV0IHNwYXduID0gcmVxdWlyZSgnY2hpbGRfcHJvY2VzcycpLnNwYXduO1xuICAgICAgICAgICAgICAgIHNwYXduKGNtZCwgYXJncywgeyBzdGRpbzogJ2luaGVyaXQnIH0pXG4gICAgICAgICAgICAgICAgICAgIC5vbignZXhpdCcsIGZ1bmN0aW9uIChjb2RlKSB7XG4gICAgICAgICAgICAgICAgICAgIHJlc29sdmUoY29kZSk7XG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgLm9uKCdlcnJvcicsIGZ1bmN0aW9uIChlcnIpIHtcbiAgICAgICAgICAgICAgICAgICAgcmVqZWN0KGVycik7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBjYXRjaCAoZXJyKSB7XG4gICAgICAgICAgICAgICAgcmVqZWN0KGVycik7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH0pO1xufVxuZXhwb3J0cy5leGVjMiA9IGV4ZWMyO1xuZXhwb3J0cy5kZWZhdWx0ID0gZXhlYztcbiIsImltcG9ydCAqIGFzIGNoaWxkUHJvY2Vzc1Byb21pc2UgZnJvbSAnY2hpbGQtcHJvY2Vzcy1wcm9taXNlJztcbmltcG9ydCB7bG9nZ2VyfSBmcm9tICcuL0xvZ2dlcic7XG5cbmNvbnN0IHNoZWxsRXhlYzphbnkgPSBjaGlsZFByb2Nlc3NQcm9taXNlLmV4ZWM7XG5cbi8vIDEgbWIgYnVmZmVyIGZvciBwaXBpbmcgdG8gc3RyZWFtcyB0byBzdGRvdXQgYW5kIHN0ZGVyclxuY29uc3QgbWF4QnVmZmVyID0gMTAyNCAqIDUwMDA7XG5cbi8qKlxuICogRXhlY3V0ZSBhIHNoZWxsIGNvbW1hbmQgYW5kIHBpcGUgYWxsIHN0ZG91dCBhbmQgc3RlcnIgb3V0cHV0cy5cbiAqXG4gKiBAcGFyYW0gIHtzdHJpbmd9ICAgICAgIGNtZCAgICAgY29tbWFuZCB0byBleGVjdXRlXG4gKiBAcGFyYW0gIHthbnk9e319ICAgICAgIG9wdGlvbnMgY2hpbGQgcHJvY2VzcyBvcHRpb25zXG4gKiBAcmV0dXJuIHtQcm9taXNlPGFueT59ICAgICAgICAgcmVzb2x2ZXMgd2hlbiB0aGUgcHJvY2VzcyBleGlzdHNcbiAqL1xuYXN5bmMgZnVuY3Rpb24gZXhlYyhjbWQ6c3RyaW5nLCBvcHRpb25zOmFueSA9IHt9KTpQcm9taXNlPGFueT4ge1xuICByZXR1cm4gbmV3IFByb21pc2UoYXN5bmMgKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgIGxldCBhbGxPdXRwdXQ6YW55ID0gJyc7XG5cbiAgICBzaGVsbEV4ZWMoY21kLCBPYmplY3QuYXNzaWduKHtcbiAgICAgIG1heEJ1ZmZlcjogbWF4QnVmZmVyXG4gICAgfSwgb3B0aW9ucykpXG4gICAgICAucHJvZ3Jlc3MoKGNoaWxkUHJvY2VzcykgPT4ge1xuICAgICAgICBjaGlsZFByb2Nlc3Muc3Rkb3V0Lm9uKCdkYXRhJywgKGRhdGEpID0+IHtcbiAgICAgICAgICBsZXQgc3RyID0gZGF0YTtcbiAgICAgICAgICBpZiAoIW9wdGlvbnMuc2lsZW50KSBsb2dnZXIuZGVidWcoc3RyKTtcbiAgICAgICAgICBhbGxPdXRwdXQgKz0gc3RyO1xuICAgICAgICB9KTtcbiAgICAgICAgaWYgKCFvcHRpb25zLm5vRXJyb3JzKVxuICAgICAgICBjaGlsZFByb2Nlc3Muc3RkZXJyLm9uKCdkYXRhJywgKGRhdGEpID0+IHtcbiAgICAgICAgICBsb2dnZXIuZXJyb3IoZGF0YSk7XG4gICAgICAgIH0pO1xuICAgICAgfSlcbiAgICAgIC50aGVuKChjaGlsZFByb2Nlc3MpID0+IHtcbiAgICAgICAgcmVzb2x2ZSh7b3V0cHV0OiBjaGlsZFByb2Nlc3Muc3Rkb3V0LnRvU3RyaW5nKCksIGNoaWxkUHJvY2Vzc30pO1xuICAgICAgICAvL2NvbnNvbGUubG9nKCdbc3Bhd25dIHN0ZG91dDogJywgY2hpbGRQcm9jZXNzLnN0ZG91dC50b1N0cmluZygpKTtcbiAgICAgIH0pXG4gICAgICAuZmFpbCgoZXJyKSA9PiB7XG4gICAgICAgIC8vbG9nZ2VyLmVycm9yKCdGQUlMIDonICsgZXJyLnRvU3RyaW5nKCd1dGY4JykpO1xuICAgICAgICByZWplY3Qoe2Vycm9yOmVyci50b1N0cmluZygndXRmOCcpLCBvdXRwdXQ6YWxsT3V0cHV0fSk7XG4gICAgICB9KTtcbiAgfSk7XG59XG5cbmV4cG9ydCBhc3luYyBmdW5jdGlvbiBleGVjMihjbWQ6c3RyaW5nLCBhcmdzOkFycmF5PGFueT4gPSBbXSwgIG9wdGlvbnM6YW55ID0ge30pOlByb21pc2U8YW55PiB7XG4gIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgdHJ5IHtcbiAgICAgIGxldCBzcGF3biA9IHJlcXVpcmUoJ2NoaWxkX3Byb2Nlc3MnKS5zcGF3bjtcbiAgICAgIHNwYXduKGNtZCwgYXJncywgeyBzdGRpbzogJ2luaGVyaXQnIH0pXG4gICAgICAub24oJ2V4aXQnLCBmdW5jdGlvbihjb2RlKXtcbiAgICAgICAgcmVzb2x2ZShjb2RlKTtcbiAgICAgIH0pXG4gICAgICAub24oJ2Vycm9yJywgZnVuY3Rpb24oZXJyKXtcbiAgICAgICAgcmVqZWN0KGVycik7XG4gICAgICB9KTtcbiAgICB9Y2F0Y2ggKGVycikge1xuICAgICAgcmVqZWN0KGVycik7XG4gICAgfVxuICB9KTtcbn1cblxuXG5cbmV4cG9ydCBkZWZhdWx0IGV4ZWM7XG4iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
